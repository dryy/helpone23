import {NgModule} from "@angular/core";

import { CommonModule } from '@angular/common';

import { MatButtonModule, MatCardModule, MatDialogModule, MatInputModule, MatTableModule, MatToolbarModule, MatMenuModule,MatIconModule, MatProgressSpinnerModule,MatFormFieldModule,MatDatepickerModule,MatNativeDateModule,MatRadioModule,MatSelectModule,MatOptionModule,MatSlideToggleModule  } from '@angular/material';

@NgModule({

  imports: [

		  CommonModule, 
		  MatToolbarModule,
		  MatButtonModule, 
		  MatCardModule,
		  MatInputModule,
		  MatDialogModule,
		  MatTableModule,
		  MatMenuModule,
		  MatIconModule,
		  MatProgressSpinnerModule ,
		  MatFormFieldModule,
		  MatDatepickerModule,
		  MatNativeDateModule,
		  MatRadioModule,
		  MatSelectModule,
		  MatOptionModule,
		  MatSlideToggleModule

  ],

  exports: [

	      CommonModule, 
		  MatToolbarModule,
		  MatButtonModule, 
		  MatCardModule,
		  MatInputModule,
		  MatDialogModule,
		  MatTableModule,
		  MatMenuModule,
		  MatIconModule,
		  MatProgressSpinnerModule ,
		  MatFormFieldModule,
		  MatDatepickerModule,
		  MatNativeDateModule,
		  MatRadioModule,
		  MatSelectModule,
		  MatOptionModule,
		  MatSlideToggleModule

   ],

})

export class CustomMaterialModule { }