export const STEPS = {
    firstStep: 'register',
    secondStep: 'register-step2',
    thirdStep: 'register-step3',
    finalStep: 'register-finalstep'
}

export const STEPS_FOR_FORGOTPWD = {
    firstStep: 'forgotPassword',
    secondStep: 'pwdcode/:id',
    thirdStep: 'newpass/:userid'
}

export const STEPS_FOR_ASKHELP = {
    firstStep: 'access/askHelp-step1',
    secondStep: 'access/askHelp-step2'
}