export class AskData {

    user_id: string = '';
    categoryService_id: string = '';
    sousCategory_id: string = '';
    description: string = '';
    min_price: string = '';
    max_price: string = '';
    choixdeliverandbillhelpforaservice:string[] = [];
    image_service:string[] = [];
    preferred_date_and_time_service:string[] = [];

    clear() {
        this.user_id = '';
        this.categoryService_id = '';
        this.sousCategory_id = '';
        this.description = '';
        this.min_price = '';
        this.max_price = 'user';
        this.choixdeliverandbillhelpforaservice = [];
        this.image_service = [];
        this.preferred_date_and_time_service = [];
    }
}


export class ASKStep1 {
    user_id: string = '';
    categoryService_id: string = '';
    sousCategory_id: string = '';
    description: string = '';
    image_service:string[] = [];
    preferred_date_and_time_service:string[] = [];
}

export class ASKStep2 {
    min_price: string = '';
    max_price: string = '';
}
     

