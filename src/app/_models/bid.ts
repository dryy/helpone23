export class Bid {

    date_livraison: string = '';
    time_livraison: string = '';
    serviceAskForHelp_id: string = '';
    user_id_celui_qui_bid: string = '';
    user_id_celui_qui_offre_service: string = '';
    price_bid: string = '';
    description: string = '';
}