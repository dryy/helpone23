export class Notifications {
    id: number;
    _id:string;
    user_id: String;
    username: String;
    service_id: String;
    content: Number;
    read: Boolean;
    data?:any;
}