export class ProfileData {

    username: string = '';
    firstname: string = '';
    surname: string = '';
    slug: string = '';
    pseudo: string = '';
    email: string = '';
    personal_description: string = '';
    password: string = '';
    experience: string = '';
    gender: string = '';
    phone: string = '';
    CV: string = '';
    criminal_record: string = '';
    longitude_location: string = '';
    latitude_location: string = '';
    location_name: string = '';
    country: string = '';
    picture: string = '';
    picture_cni: string = '';
    picture_with_cni: string = '';
    fonctionName: string = '';
    souscategories:any[] = [];
    langues:any[] = [];

}

     

