export class RegData {

    email: string = '';
    password: string = '';
    username: string = '';
    firstname: string = '';
    surname: string = '';
    slug: string = 'user';
    experience: string = '';
    fonctionName: string = '';
    souscategories:string[] = [];
    langues:string[] = [];
    picture:string = '';
    picture_cni: string = '';
    picture_with_cni: string = '';

    clear() {
        this.email = '';
        this.password = '';
        this.username = '';
        this.firstname = '';
        this.surname = '';
        this.slug = 'user';
        this.experience = '';
        this.fonctionName = '';
        this.souscategories = [];
        this.langues = [];
        this.picture = '';
        this.picture_cni = '';
        this.picture_with_cni = '';

    }
}


export class Step1 {
    pwd : string = '';
    pwd2 : string = '';
    email: string = '';
}

export class Step2 {
    username : string = '';
}

export class FinalStep {
    firstname: string = '';
    surname: string = '';
    fonctionName: string = '';
    experience: string = '';
}

