import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms'; 

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { CustomMaterialModule} from "./../../material.module";
import { AlertModule } from '../../_directives/alert.module';

@NgModule({
  imports: [
    CommonModule,
    LoginRoutingModule,AlertModule,
    CustomMaterialModule,FormsModule,ReactiveFormsModule
  ],
  declarations: [LoginComponent]
})
export class LoginModule { }
