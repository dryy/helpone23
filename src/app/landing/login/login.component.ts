import { Component, OnInit, Injectable } from '@angular/core';

// Must import to use Forms functionality  
import { FormBuilder, FormGroup, Validators} from '@angular/forms'; 
import { first } from 'rxjs/operators'; 
import { Router,ActivatedRoute } from '@angular/router';
import { AlertService, AuthenticationService} from '../../_services';
import { VariablesGlobales } from 'src/app/_models/variablesGlobales';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
	
    loginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    infoMessage: string='';
    conectionError: string='';
 
  constructor(  private formLog: FormBuilder,private route: ActivatedRoute,
		        private router: Router,private authenticationService: AuthenticationService,
                private alertService: AlertService,
                private param: VariablesGlobales ) { }

    ngOnInit() {
        this.route.queryParams
        .subscribe(params => {
            if(params.state !== undefined && params.state === 'true') {
                this.infoMessage = 'Congratulation! Your Registration Was Successful! You have receive a confirmation mail. You should confirm your account by clicking the link in the mail before being able to Login.';
            }
            if (this.param.debutPass=="oui")
                this.infoMessage = 'Please login again to continue !!!'; 
        });

        // To initialize FormGroup  
        this.loginForm = this.formLog.group({    
        'pwd':['', Validators.required],  
        'email':['', Validators.compose([Validators.required])]  
        });  

        // reset login status
        //this.authenticationService.logout();
        
        localStorage.removeItem('currentUser');
        localStorage.removeItem('isLoggedin');

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/access';

    }

  	// convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }
	  
	  // Executed When Form Is Submitted  
	  onSubmit() {
       
       this.submitted = true;

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }

        this.loading = true;
        this.authenticationService.login(this.f.email.value, this.f.pwd.value)
            .subscribe(
                (data : any) => { 
                   this.alertService.success(data.message, true);
                   if(data.user.confirmed==true){
                        console.log(data);
                        localStorage.setItem('currentUser', JSON.stringify(data));
                        localStorage.setItem('isLoggedin', 'true');
                        this.param.parametre = data.user.fonctionName;
                        console.log('Function name : '+data.user.fonctionName);
                        this.router.navigate([this.returnUrl]); 
                   }
                    else{
                        this.conectionError = "You must activate your account using the link that have been sent to you by mail";
                    }
                    
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }

}
