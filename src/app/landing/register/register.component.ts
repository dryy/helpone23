import { Component, OnInit, Input } from '@angular/core';
// Must import to use Forms functionality  
import { FormBuilder, FormGroup, Validators } from '@angular/forms'; 
import { first } from 'rxjs/operators'; 
import { Router,ActivatedRoute } from '@angular/router';
import { AlertService, AuthenticationService,RegisterDataService, UserService} from '../../_services';
import { Step1, RegData }   from '../../_models/registerData';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

    regForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    fstep: Step1;
    aa: String;
    bb: String;
    mdp: String;
    pwd: string;
    pwd2: string;
    @Input() regData: RegData; 

  constructor(  private formLog: FormBuilder,private route: ActivatedRoute,
            private router: Router,private authenticationService: AuthenticationService,
            private userService: UserService,
		        private alertService: AlertService,private regDataService: RegisterDataService) { }

  ngOnInit() {

  	// To initialize FormGroup  
    this.regForm = this.formLog.group({    
      'pwd':['', Validators.required],  
      'pwd2':['', Validators.required],
      'firstname':['', Validators.required],
      'surname':['', Validators.required],
      'username':['', Validators.required],
      'email':['', Validators.compose([Validators.required,Validators.email])]  
    });  
    this.regData = this.regDataService.getRegData(); 

  }

  		  // convenience getter for easy access to form fields
      get f() { return this.regForm.controls; }
	  
	  // Executed When Form Is Submitted  
	  onSubmit() {       
       this.submitted = true;

        // stop here if form is invalid
        if (this.regForm.invalid) {
            return;
        }

        // this.mdp = "";
        let userData : any = {
                              firstname:this.f.firstname.value,
                              surname:this.f.surname.value,
                              username:this.f.username.value,
                              email:this.f.email.value,
                              pwd:this.f.pwd.value 
                            }; 

        this.regDataService.saveData(userData);
        console.log(this.f.pwd.value);
        console.log(this.f.pwd2.value);
        if(this.f.pwd.value == this.f.pwd2.value){
          this.mdp = "";
          this.loading = true;
          this.authenticationService.CheckEmail(this.f.email.value)
              .subscribe(
                  (data : any) => {
                    //  this.userService.registerForm(this.regData); 
                     this.alertService.success(data.status, true);
                     console.log(data.message);

                     console.log('a aaaaaaaaa aaaaaaaa : '+this.regData);
                      this.userService.registerForm(this.regData)
                      .pipe(first())
                      .subscribe(
                        //on success  
                        (dataGet : any) => { 
                            this.alertService.success(dataGet.message, true);
                            // this.regForm.reset();
                            this.regData = this.regDataService.resetRegFormData();                            
                            localStorage.removeItem('suggestionsTerms');
                            localStorage.removeItem('optChooseText');
                        },
                        //on error
                        error => {
                          this.alertService.error(error);
                            this.loading = false;
                        },
                        //on complete
                        () => { 
                            this.router.navigate(['/logon'], { queryParams: { state: 'true' } });
                        });

                     localStorage.removeItem('suggestionsTerms');  
                     localStorage.setItem('suggestionsTerms',JSON.stringify(data.suggestions));
  
                    //  this.router.navigate(['/register-step2']);
                      
                  },
                  error => {
                      this.alertService.error(error);
                  });
        }
        else{
          this.mdp = "Both passwords do not match. Please check them, then try again";
          this.router.navigate(['/register']);
        }

        
    }

}
