import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms'; 

import { ForgotpwdRoutingModule } from './forgotpwd-routing.module';
import { ForgotpwdComponent } from './forgotpwd.component';

import { AlertModule } from '../../_directives/alert.module';

@NgModule({
  imports: [
    CommonModule,
    ForgotpwdRoutingModule,AlertModule,
    FormsModule,ReactiveFormsModule
  ],
  declarations: [ForgotpwdComponent]
})
export class ForgotpwdModule { }
