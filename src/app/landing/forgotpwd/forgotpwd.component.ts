import { Component, OnInit } from '@angular/core';

// Must import to use Forms functionality  
import { FormBuilder, FormGroup, Validators ,FormsModule,NgForm } from '@angular/forms'; 
import { first } from 'rxjs/operators'; 
import { Router,ActivatedRoute } from '@angular/router';
import { AlertService, ForgotpasswordService} from '../../_services';

@Component({
  selector: 'app-forgotpwd',
  templateUrl: './forgotpwd.component.html',
  styleUrls: ['./forgotpwd.component.scss']
})
export class ForgotpwdComponent implements OnInit {
    
    forgotForm: FormGroup;
    submitted = false;

  constructor(  private formLog: FormBuilder,
                private route: ActivatedRoute,
    		        private router: Router,
    		        private forgotPwdService: ForgotpasswordService,
                private alertService: AlertService) { }

  ngOnInit() {

  	// To initialize FormGroup  
    
    this.forgotForm = this.formLog.group({
            email: ['', [Validators.required,Validators.email]],
        });

    // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        localStorage.removeItem('isLoggedin');
        localStorage.removeItem('UserID');
        localStorage.removeItem('emailValue');
        this.router.navigate(["/forgotPassword"]);

  }

  	 // convenience getter for easy access to form fields
    get f() { return this.forgotForm.controls; }


    onSubmit() {

        this.submitted = true;

        // stop here if form is invalid
        if (this.forgotForm.invalid) {
            return;
        }


        this.forgotPwdService.sendPwdEmail(this.f.email.value)
            .pipe(first())
            .subscribe(
                (dataGet : any) => { 
                   this.alertService.success(dataGet.message, true);
                   this.forgotPwdService.setFirstStep(); 
                   this.router.navigate(['pwdcode/',dataGet.data._id]);
                    
                },
                error => {
                    this.alertService.error(error);
                });
        
    }

}

