import { ForgotpwdModule } from './forgotpwd.module';

describe('ForgotpwdModule', () => {
  let forgotpwdModule: ForgotpwdModule;

  beforeEach(() => {
    forgotpwdModule = new ForgotpwdModule();
  });

  it('should create an instance', () => {
    expect(forgotpwdModule).toBeTruthy();
  });
});
