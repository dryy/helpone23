import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms'; 
import { RegisterStep2RoutingModule } from './register-step2-routing.module';
import { RegisterStep2Component } from './register-step2.component';
import { AlertModule } from '../../_directives/alert.module';


@NgModule({
  imports: [
    CommonModule,
    RegisterStep2RoutingModule,AlertModule,
    FormsModule,ReactiveFormsModule
  ],
  declarations: [RegisterStep2Component]
})
export class RegisterStep2Module { }
