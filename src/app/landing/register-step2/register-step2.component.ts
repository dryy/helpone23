import { Component, OnInit } from '@angular/core';

// Must import to use Forms functionality  
import { FormBuilder, FormGroup, Validators ,FormsModule,NgForm } from '@angular/forms'; 
import { first } from 'rxjs/operators'; 
import { Router,ActivatedRoute } from '@angular/router';
import { AlertService, AuthenticationService,RegisterDataService} from '../../_services';

import { Step2 }   from '../../_models/registerData';

@Component({
  selector: 'app-register-step2',
  templateUrl: './register-step2.component.html',
  styleUrls: ['./register-step2.component.scss']
})
export class RegisterStep2Component implements OnInit {

  regForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    fstep2: Step2;
    suggestion : any;
    infoMessage: string = '';

  constructor(  private formLog: FormBuilder,private route: ActivatedRoute,
		        private router: Router,private authenticationService: AuthenticationService,
		        private alertService: AlertService,private regDataService: RegisterDataService) { }

  ngOnInit() {


  	// To initialize FormGroup  
    this.regForm = this.formLog.group({    
      'username':['', Validators.required]
    });  

    var sug = localStorage.getItem("suggestionsTerms");
    this.suggestion = JSON.parse(sug); 

    this.fstep2 = this.regDataService.getSecStep();
    this.regForm.patchValue({username: this.fstep2.username});
  }

    FillInput(value: string): void {
      this.regForm.patchValue({username: value}); 
    }

  		  // convenience getter for easy access to form fields
      get f() { return this.regForm.controls; }
	  
	  // Executed When Form Is Submitted  
	  onSubmit() {
       
       this.submitted = true;

        // stop here if form is invalid
        if (this.regForm.invalid) {
            return;
        }

        this.loading = true;
        this.authenticationService.CheckUsername(this.f.username.value)
            .pipe(first())
            .subscribe(
                (data : any) => { 
                   this.regDataService.setSecStep(this.regForm.value); 
                   this.alertService.success(data.message, true);
                   this.router.navigate(['/register-step3']);
                    
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }

    onPrevious() {

       localStorage.removeItem('suggestionsTerms');
       this.router.navigate(['/register']);
    }


}    