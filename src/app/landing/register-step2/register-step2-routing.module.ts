import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterStep2Component } from './register-step2.component';

const routes: Routes = [

	{
        path: '', component: RegisterStep2Component
    }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegisterStep2RoutingModule { }
