import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LandingRoutingModule } from './landing-routing.module';
import { LandingComponent } from './landing.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderlogComponent } from './headerlog/headerlog.component';
import { CategoryListComponent } from './category-list/category-list.component';
import { CategoryServicesModule } from '../components/category-services/category-services.module';
import { ModalModule } from '../components/modal/modal.module';
// import { LivechatWidgetModule } from '@livechat/angular-widget';

@NgModule({
  imports: [
    CommonModule,
    // LivechatWidgetModule,
    CategoryServicesModule,
    LandingRoutingModule,
    ModalModule
  ],
  declarations: [LandingComponent, FooterComponent, HeaderlogComponent, CategoryListComponent]
})
export class LandingModule { }