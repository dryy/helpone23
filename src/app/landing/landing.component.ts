	import { Component, OnInit,Input, ViewChild  } from '@angular/core';
	import { Router,ActivatedRoute } from '@angular/router';

	import { RegisterDataService }  from '../_services';
	import { LiveChatWidgetModel, LiveChatWidgetApiModel } from '@livechat/angular-widget';
	// import 'waypoints/lib/noframework.waypoints.min.js';

	declare var jQuery:any;
	declare var $ :any;
	declare const Waypoint: any;


	@Component({
		selector: 'app-landing',
		templateUrl: './landing.component.html',
		styleUrls: ['./landing.component.scss']
	})
export class LandingComponent implements OnInit {

		// public liveChatApi: LiveChatWidgetApiModel;
		// public isLiveChatWidgetLoaded: boolean = false;
		// public visitor: { name: string; email: string};
		// public params: { name: string; value: string}[];
	
	@Input() formData;
	
	//    @ViewChild('liveChatWidget', {static: false}) public liveChatWidget: LiveChatWidgetModel;
	constructor(public route: ActivatedRoute,private router: Router,private RegisterService: RegisterDataService) {
		
		// this.visitor = {
		// 	name: 'John Doe',
		// 	email: 'john@doe.com',
		// };
		// this.params = [
		// 	{ name: 'Login', value: 'joe_public' },
		// 	{ name: 'Account ID', value: 'ABCD1234' },
		// 	{ name: 'Total order value', value: '$123' }
		//   ];
		// override the route reuse strategy
			this.router.routeReuseStrategy.shouldReuseRoute = function() {
				return false;
			};
			

	}

	ngOnInit() {

	this.formData = this.RegisterService.getRegData();


	let owl,owlAnimateFilter,TxtType:any;

	/* =======================================
			Slider  Carousel
		======================================*/	
		
		jQuery(".slider_home").owlCarousel({
			items: 1,
			autoplay: false,
			loop: true,
			autoplayTimeout: 8000, // Default is 5000
			smartSpeed: 1000, // Default is 250
			navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
			dots: true,
			nav: false,
			mouseDrag: true,
			touchDrag: true
		});

		jQuery(".slider_home").on("translate.owl.carousel", function() {
			jQuery(".slide-text-grid,.single_slider h2, .single_slider p").removeClass("animated fadeInUp").css("opacity", "0");
		});

		jQuery(".slider_home").on("translated.owl.carousel", function() {
			jQuery(".slide-text-grid,.single_slider h2, .single_slider p").addClass("animated fadeInUp").css("opacity", "1");
		});

		//________COUNTERUP FUNCTION BY= counterup-min.js________//

		//jQuery('.counter').counterUp({
			//delay: 10,
		//	time: 1000
		//});


		//________BOOSTRAP 4 TABS ACTIVE________//

		$(".nav-tabs li a").click(function(){
				$(".nav-tabs li").removeClass("active");
				$(this).parent().addClass("active");
			});

	//________TOOLTIP FUNCTION BY=bootstrap.js________//

		jQuery('[data-toggle="tooltip"]').tooltip();
		

	//________POPOVER FUNCTION BY =bootstrap.js________//

		jQuery('[data-toggle="popover"]').popover();

	//________STICKY MENU WHEN SCROLL DOWN________//	

		// if(jQuery('.sticky-header').length){
		// 	var sticky = new Waypoint.Sticky({
		// 	  element: jQuery('.sticky-header')
		// 	})
		// }

	//________SCROLL TOP BUTTON________//	

		jQuery("button.scroltop").on('click', function() {
			jQuery("html, body").animate({
				scrollTop: 0
			}, 1000);
			return false;
		});

		jQuery(window).on("scroll", function() {
			var scroll = jQuery(window).scrollTop();
			if (scroll > 900) {
				jQuery("button.scroltop").fadeIn(1000);
			} else {
				jQuery("button.scroltop").fadeOut(1000);
			}
		});


	//________FOOTER FIXED ON BOTTOM PART________//	

		jQuery('.site-footer').css('display', 'block');
		jQuery('.site-footer').css('height', 'auto');
		var footerHeight = jQuery('.site-footer').outerHeight();
		jQuery('.footer-fixed > .page-wraper').css('padding-bottom', footerHeight);
		jQuery('.site-footer').css('height', footerHeight);

	//________NAVIGATION SUBMENU SHOW & HIDE ON MOBILE________//	

		jQuery(".sub-menu").parent('li').addClass('has-child');
		jQuery(".mega-menu").parent('li').addClass('has-child');
		jQuery("<div class=' glyphicon glyphicon-plus submenu-toogle'></div>").insertAfter(".has-child > a");
		jQuery('.has-child a+.submenu-toogle').on('click', function(ev) {
			jQuery(this).next(jQuery('.sub-menu')).slideToggle('fast', function(){
				jQuery(this).parent().toggleClass('nav-active');
			});
			ev.stopPropagation();
		});

	//________INDEX PAGE CAROUSEL 1 FUNCTION BY = owl.carousel.js________//

		jQuery('.home-carousel-1').owlCarousel({
			loop:true,
			margin:30,
			nav:false,
			dots: true,
			autoplay:true,
			autoplayTimeout:5000,
			autoplayHoverPause:false,
			navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
			responsive:{
				0:{
					items:1
				},
				480:{
					items:1
				},			
				
				767:{
					items:1,
					margin:50
				},
				1000:{
					items:2
				}
			}
			
		});	
		
		//________CLIENT LOGO ON HOME PAGE FUNCTION BY= owl.carousel.js________//

		jQuery('.home-client-logo').owlCarousel({
			loop:true,
			autoplay:true,
			autoplayTimeout:4000,
			margin:20,
			nav:false,
			dots: false,
			navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
			responsive:{
				0:{
					items:2
				},
				480:{
					items:2
				},			
				
				767:{
					items:3
				},
				1000:{
					items:5
				}
			}
			
		});

	/* Filter Nav */

		jQuery('.btn-filter-wrap2').on('click', '.btn-filter', function(e) {
			var filter_data = jQuery(this).data('filter');

			/* return if current */
			if(jQuery(this).hasClass('btn-active')) return;

			/* active current */
			jQuery(this).addClass('btn-active').siblings().removeClass('btn-active');

			/* Filter */
			owl.owlFilter(filter_data, function(_owl) { 
				jQuery(_owl).find('.item').each(owlAnimateFilter); 
			});
		})


	window.onload = function() {
		var elements = document.getElementsByClassName('typewrite');
		for (var i=0; i<elements.length; i++) {
			var toRotate = elements[i].getAttribute('data-type');
			var period = elements[i].getAttribute('data-period');
			if (toRotate) {
			new TxtType(elements[i], JSON.parse(toRotate), period);
			}
		}
		// INJECT CSS
		var css = document.createElement("style");
		css.type = "text/css";
		css.innerHTML = ".typewrite > .wrap {}";
		document.body.appendChild(css);
	};

	//________ON SCROLL CONTENT ANIMATED FUNCTION BY= Viewportchecker.js________//

		jQuery('.animate').scroll({
			mobile: false,
			once: true
		});

	//________PARALLAX EFFECT ON BACKGROUND IMAGE FUNCTION BY = stellar.js ________// 

		// jQuery('load', function(){
		// 		jQuery.stellar({
		// 			horizontalScrolling: false,
		// 			verticalOffset:100
		// 		});
		// 	});

		//________PAGE LOADER________// 	
		
		jQuery(".slides__preload_wrapper").fadeOut(1500);

			

		this.showHeaderDynamic();
	}
	//End of ngOnInit

		public showHeaderDynamic() {
				// if (this.router.url.startsWith('/home') || this.router.url.startsWith('/help') || this.router.url.startsWith('/askForHelp-step2')) {
				// return true;
				// } else {
				// return false;
				// }
		}

		//   onChatWindowMinimized() {
		// 	console.log('minimized')
		//   }
		
		//   onChatWindowOpened() {
		// 	console.log('opened')
		//   }

		//   onChatLoaded(api: LiveChatWidgetApiModel): void {
		// 	this.liveChatApi = api;
		// 	this.isLiveChatWidgetLoaded = true;
		// 	console.log('zeeeeeeeeeeeeeeee    :  '+this.visitor[name]);
		
		//   }

}
