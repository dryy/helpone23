import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms'; 

import { NewpassRoutingModule } from './newpass-routing.module';
import { NewpassComponent } from './newpass.component';

import { AlertModule } from '../../_directives/alert.module';

@NgModule({
  imports: [
    CommonModule,
    NewpassRoutingModule,AlertModule,
    FormsModule,ReactiveFormsModule
  ],
  declarations: [NewpassComponent]
})
export class NewpassModule { }
