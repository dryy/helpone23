import { Component, OnInit,OnDestroy  } from '@angular/core';
// Must import to use Forms functionality  
import { FormBuilder, FormGroup, Validators ,FormsModule,NgForm } from '@angular/forms'; 
import { first,takeUntil } from 'rxjs/operators'; 
import {componentDestroyed} from "@w11k/ngx-componentdestroyed";
import { Router,ActivatedRoute } from '@angular/router';
import { AlertService, ForgotpasswordService,UserService} from '../../_services';
import { PasswordValidation } from '../../shared';

@Component({
  selector: 'app-newpass',
  templateUrl: './newpass.component.html',
  styleUrls: ['./newpass.component.scss']
})
export class NewpassComponent implements OnInit,OnDestroy  {

    NewPasswordForm: FormGroup;
    submitted = false;
    emailVal:any; 

  constructor( private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private forgotPwdService: ForgotpasswordService,
        private alertService: AlertService,
        private userService: UserService) { }

  ngOnInit() {

  			let useridValue = localStorage.getItem('UserID');

  			if (useridValue) {  
		       this.userService.getById(useridValue).pipe(first(),takeUntil(componentDestroyed(this))).subscribe( (dataget : any)  => { 
		       		this.emailVal= dataget;
		          }); 		            
            }  

  			this.NewPasswordForm = this.formBuilder.group({
	  			   ValUser: [useridValue],
	               password: ['', [Validators.required, Validators.minLength(4)]],
	               Confpassword: ['', Validators.required]
               }, 
               { 
               	   validator: PasswordValidation.MatchPassword // your validation method	
            });
  }

  ngOnDestroy() {
    // 
    console.log('Component Destroyed !');
  }


  // convenience getter for easy access to form fields
    get f() { return this.NewPasswordForm.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.NewPasswordForm.invalid) {
            return;
        }

        this.forgotPwdService.setThirdStep();

        this.forgotPwdService.changePwd(this.f.ValUser.value,this.f.password.value)
            .pipe(first())
            .subscribe(
                (dataGet : any) => { 
                   this.alertService.success(dataGet.message, true);
                   this.forgotPwdService.resetStepsFlow();
                   this.router.navigate(['/logon']);
                   localStorage.removeItem('UserID'); 
                    
                },
                error => {
                    this.alertService.error(error);
                });
        
    }


}