import { Component, OnInit } from '@angular/core';
import { UserInfo } from 'src/app/_models';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss']
})
export class HelpComponent implements OnInit {

  currentUser: UserInfo;
  dataArray: any;
  categorie: string='';
  sous_cat : any;

  constructor() { 
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    // localStorage.setItem('suggestionsTerms',JSON.stringify(data.suggestions));
  }

  ngOnInit() {
    this.categorie = localStorage.getItem('categorie');
    this.sous_cat = JSON.parse(localStorage.getItem('sous_categorie'));
    this.sous_cat = this.sous_cat.dataGroupedBy;
    console.log(this.categorie);
    console.log(this.sous_cat);
  }

  saveSousCategorie(sousCategorie: any, iden:any){
    localStorage.setItem('laSousCategorie', sousCategorie); 
    localStorage.setItem('idlaSousCategorie', iden);
  }

  
}
