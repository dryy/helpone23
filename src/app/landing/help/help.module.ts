import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HelpRoutingModule } from './help-routing.module';
import { AlertModule } from 'src/app/_directives/alert.module';
import { HelpComponent } from './help.component';
import { CustomMaterialModule } from 'src/app/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [HelpComponent],
  imports: [
    CommonModule,
    HelpRoutingModule,AlertModule,
    CustomMaterialModule,FormsModule,ReactiveFormsModule
  ]
})
export class HelpModule { }
