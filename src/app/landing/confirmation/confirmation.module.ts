import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms'; 

import { ConfirmationRoutingModule } from './confirmation-routing.module';
import { CustomMaterialModule} from "./../../material.module";
import { AlertModule } from '../../_directives/alert.module';
import { ConfirmationComponent } from './confirmation.component';

@NgModule({
  imports: [
    CommonModule,
    ConfirmationRoutingModule, AlertModule,
    CustomMaterialModule,FormsModule,ReactiveFormsModule
  ],
  declarations: [ConfirmationComponent]
})
export class confirmationModule { }
