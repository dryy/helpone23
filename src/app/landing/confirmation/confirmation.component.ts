import { Component, OnInit } from '@angular/core';
import { AuthenticationService, ProfileService, AlertService } from '../../_services';
import { first } from 'rxjs/operators';
import { UserInfo } from '../../_models';
import { Router, ActivatedRoute } from '@angular/router';
import { VariablesGlobales } from 'src/app/_models/variablesGlobales';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss']
})
export class ConfirmationComponent implements OnInit {

  currentUser: UserInfo;
  ValUser: string;
  MyInfo: any;
  activation_response: string;
  mauvais: string;
  val = [];
  fname: string;
  sname: string;
  emailAddress: string;
  activationCode: string;
  returnUrl: string;
  email: string;
  password: string;
  
  constructor(private profileServ: ProfileService, private authenticationService: AuthenticationService, 
    private router: Router,
    private alertService: AlertService,
    private route: ActivatedRoute,
    private param: VariablesGlobales) { 
    // this.authenticationService.login(this.f.email.value, this.f.pwd.value);
    
      this.extractUrlParams();
      // this.onSubmit(this.email, this.password)
          
   }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

      if (this.currentUser) {
            this.ValUser=this.currentUser.user._id;
      } else {
            //console.log('not get on init!');
      }
      // get return url from route parameters or default to '/'
      this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/access';
  }


  extractUrlParams(){	
          // var t = location.search.substring(1).split('&');
          // var f = [];
          // for (var i=0; i<t.length; i++){
          //   var x = t[ i ].split('=');
          //   f[x[0]]=x[1];
          //   this.val[i] = f[x[0]];
          // }
          // console.log(this.val[0]);
          // console.log(this.val[1]);

          this.route.queryParams.subscribe(params => {
            this.emailAddress = params['uemail'];
            this.activationCode = params['code'];
            console.log('parammmmmmmm : '+this.emailAddress);
            console.log('parammmmmmmm : '+this.activationCode);
          });
 
          this.authenticationService.activation(this.emailAddress, this.activationCode)
          .subscribe(
              (data : any) => { 
                this.alertService.success(data.message, true);
                    this.activation_response = "Your account has been well activated. You can now log in";
                    console.log(data);
                    localStorage.setItem('currentUser', JSON.stringify(data));
                    localStorage.setItem('isLoggedin', 'true');
                    this.email= data.user.email;
                    this.password = data.user.password;
                    this.router.navigate([this.returnUrl], { queryParams: { activate: 'true' } });                                            
              },
              error => {
                  this.alertService.error(error);
              });
              console.log(this.activation_response);
          
          return this.emailAddress;
  }

  // onSubmit(email, pass){
  //   this.authenticationService.login(email, pass)
  //   .pipe(first())
  //   .subscribe(
  //       (data : any) => { 
  //         this.alertService.success(data.message, true);
  //         if(data.user.confirmed==true){
  //             console.log(data);
  //             this.param.parametre = data.user.fonctionName;
  //             console.log('eeeeeeeeeeeeeeeeee : '+data.user.fonctionName);
  //             this.router.navigate([this.returnUrl]);
  //         }
  //           else{
  //             this.router.navigate(["logon"]);
  //               // this.conectionError = "You must activate your account using the link that have been sent to you by mail";
  //           }
            
  //       },
  //       error => {
  //           this.alertService.error(error);
  //           // this.loading = false;
  //       });  
  // }

}
