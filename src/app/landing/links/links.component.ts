import { Component, OnInit } from '@angular/core';
import { UserInfo } from 'src/app/_models';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-links',
  templateUrl: './links.component.html',
  styleUrls: ['./links.component.scss']
})
export class LinksComponent implements OnInit {

  lien: any;

  constructor(private route: ActivatedRoute) { 
    // localStorage.setItem('suggestionsTerms',JSON.stringify(data.suggestions));
  }

  ngOnInit() {
    this.getParam();
  }

  getParam(){
    this.lien = this.route.snapshot.paramMap.get("thelink");    
    console.log('leinssss  : '+this.lien);
  }
  
}
