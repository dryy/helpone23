import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlertModule } from 'src/app/_directives/alert.module';
import { LinksComponent } from './links.component';
import { CustomMaterialModule } from 'src/app/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LinksRoutingModule } from './links-routing.module';



@NgModule({
  declarations: [LinksComponent],
  imports: [
    CommonModule,
    LinksRoutingModule,AlertModule,
    CustomMaterialModule,FormsModule,ReactiveFormsModule
  ]
})
export class LinksModule { }
