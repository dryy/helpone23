import { AboutModule } from './../../components/about/about.module';
import { CategoryServicesModule } from './../../components/category-services/category-services.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomepageRoutingModule } from './homepage-routing.module';
import { HomepageComponent } from './homepage.component';

import { SharedPipesModule } from './../../shared';
import { ButtonModule } from 'src/app/components/button/button.module';
import { GridModule } from 'src/app/components/grid/grid.module';

@NgModule({
  imports: [
    CommonModule,
    SharedPipesModule,
    HomepageRoutingModule,
    CategoryServicesModule,
    AboutModule,
    GridModule,
    ButtonModule
  ],
  declarations: [HomepageComponent]
})
export class HomepageModule { }
