    import { Component, OnInit } from '@angular/core';
    import { first } from 'rxjs/operators';
    import { routerTransition } from '../../router.animations';
    import { Router,ActivatedRoute } from '@angular/router';

    import { ServiceCat,UserInfo } from './../../_models';

    import { ServiceCatService,ServiceListingService, ProfileService } from './../../_services';

    import * as _ from 'lodash';
    import { VariablesGlobales } from 'src/app/_models/variablesGlobales';

    @Component({
    selector: 'app-homepage',
    templateUrl: './homepage.component.html',
    styleUrls: ['./homepage.component.scss'],
    animations: [routerTransition()]
    })
    export class HomepageComponent implements OnInit {

    services_data: any;
    subcats_data: any;
    services_categ: any;
    dataProduct: any;
    dataArray: any;
    currentUser: UserInfo;
    ValUser: string;
    MyInfo: any;
    helpConnect: string;
    services_category: any[] = [];
    categ: any;
    name: string = "le nom";
    title: string = "le titre";
    textButton = "Apply Now"
    content : string = "Help 123 is a one-stop platform for all of your handyman needs near you anywhere in South Africa. Based in Johannesburg, we are here to connect you immediately with the help you need to make your life easier. <br>We’ve spent many years figuring out what works and what doesn’t, so that you don’t have to. <br>The wisdom and invaluable experience we’ve gained allows us to get you the best of the best when you need them. <br>Our drop-down list provides a wide range of services from electricians, plumbers, mechanics, roof repair specialists etc... visit Website www.help123.co.za";
    constructor(private SetServiceCat: ServiceCatService,
                private serviceList: ServiceListingService, 
                public route: ActivatedRoute,
                private router: Router,
                private profileServ: ProfileService,
                public param: VariablesGlobales) {
        
        // override the route reuse strategy
            this.router.routeReuseStrategy.shouldReuseRoute = function() {
                return false;
            };

    }

    ngOnInit() {
        this.loadAllCategories();
        this.loadAllCategoriesAndSubCategories();

        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (this.currentUser) {
                this.ValUser=this.currentUser.user._id;
                this.getProfileInfoData();
        } else {
                console.log('not get on init!');
        }
    }

    private getProfileInfoData() {
        this.profileServ.getProfileInfo(this.ValUser).pipe(first()).subscribe(UserInfoValues => {          
            this.MyInfo = UserInfoValues; 
        });
    }

    private loadAllCategoriesAndSubCategories() {
            this.SetServiceCat.getAll().pipe(first()).subscribe(cats => { 
                this.services_categ = cats; 
            });
            this.SetServiceCat.getAllSubCateg().pipe(first()).subscribe(subcats => {          
                
                this.subcats_data = subcats; 

                let datas = this.subcats_data.data;

                this.dataArray = _(datas)
                .groupBy(data => data.categoryServiceId)
                .map((dataGroupedBy, category) => ({ category, dataGroupedBy }))
                .value();
            });
        }

        private loadAllServicesCategoriesForUsers() {

            this.SetServiceCat.getAll().pipe(first()).subscribe(serv_cats => { 
                this.services_categ = serv_cats; 
            });

            this.serviceList.getAllRecentHelpsRequest().pipe(first()).subscribe(RecentsHelpReqData => {          
                this.services_data = RecentsHelpReqData; 
                
                let datas = this.services_data.data;
                this.dataProduct = _(datas)
                .groupBy(data => data.categoryService_id)
                .map((dataGroupedBy, category) => ({ category, dataGroupedBy }))
                .value();
            });

        }

        getCategoryName(idCategory:string,TabCat:any): string {

                let verified:boolean = false, i=0;
                let VarName="";
                if (idCategory && TabCat) {
                    while (!verified && i < TabCat.length ) {

                    if ( String(TabCat[i]._id) == idCategory){
                        VarName=TabCat[i].categoryName;
                        verified=true;
                    }

                    i++;
                    }

                } 

            return (VarName.length > 20)? (VarName.slice(0, 20))+'..':(VarName) 
        }

        getCategoryLogo(idCategory:string,TabCat:any): string {

                let verified:boolean = false, i=0;
                let VarName="";
                if (idCategory && TabCat) {
                    while (!verified && i < TabCat.length ) {

                    if ( String(TabCat[i]._id) == idCategory){
                        VarName=TabCat[i].logo;
                        verified=true;
                    }

                    i++;
                    }
                }
            return VarName 
        } 

    categorie(cat: any, iden: any){
        console.log(cat.dataGroupedBy);
        console.log(cat.dataGroupedBy[iden].categoryServiceId);
        localStorage.setItem('categorie', cat.dataGroupedBy[0].categoryName);
        localStorage.setItem('sous_categorie', JSON.stringify(cat));
        localStorage.setItem('idlaCategorie', cat.dataGroupedBy[iden].categoryServiceId);
        if(this.ValUser){
        console.log(this.ValUser);
        this.router.navigate(['help']);
        } else{
        this.helpConnect = "You must be connected before being able to ask some help !"
        this.router.navigate(['/logon']);
        }  
    }

    goToApply(){ 
        if(this.ValUser){
        if(this.currentUser.user.fonctionName == "Offer Helps" || this.currentUser.user.experience=='Beginner' || this.currentUser.user.experience=='Intermediate' || this.currentUser.user.experience=='Advanced'){
            this.router.navigate(['/access/helpBanks']);
        } else{
            this.router.navigate(['/access/switchAccount']);
        }
        } else{
        this.router.navigate(['/logon']);
        }  
    }

    goToServices(){
        this.router.navigate(['/category-list']);
    }

    saveSousCategorie(cat: any, sousCategorie: any, iden:any, iden2: any){
        localStorage.setItem('categorie', cat.dataGroupedBy[0].categoryName);
        localStorage.setItem('laSousCategorie', sousCategorie);
        localStorage.setItem('idlaCategorie', cat.dataGroupedBy[iden].categoryServiceId);
        localStorage.setItem('idlaSousCategorie', cat.dataGroupedBy[iden2]._id);
        console.log(cat.dataGroupedBy[0].categoryName) ;
        console.log(sousCategorie) ;
        console.log(cat.dataGroupedBy[iden].categoryServiceId) ;
        console.log(cat.dataGroupedBy[iden2]._id) ;
        if(this.ValUser){
        this.router.navigate(['/askForHelp-step2/',cat.dataGroupedBy[iden2]._id]); 
        console.log(this.ValUser);
        } else{
        this.router.navigate(['/logon']);
        }    
    }
    private loadAllCategories() {
        this.SetServiceCat.getAll().subscribe(categories => {
        this.categ =  categories;
        for(var i=0; i<3; i++){
            this.services_category[i] = this.categ.data[i];
        }    
        console.log(this.services_category);
        });
        
        this.SetServiceCat.getAllSubCateg().pipe(first()).subscribe(subcats => {
            this.subcats_data = subcats;
            let datas = this.subcats_data.data;
            console.log(datas);
        });
    }

}
