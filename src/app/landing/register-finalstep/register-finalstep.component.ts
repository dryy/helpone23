import { Component, OnInit , Input} from '@angular/core';

// Must import to use Forms functionality  
import { FormBuilder, FormGroup, Validators ,FormsModule,NgForm } from '@angular/forms'; 
import { first } from 'rxjs/operators'; 
import { Router,ActivatedRoute } from '@angular/router';
import { AlertService, UserService ,AuthenticationService,RegisterDataService,LanguageService} from '../../_services';

import { RegData }   from '../../_models/registerData';
import { Langue } from './../../_models';

@Component({
  selector: 'app-register-finalstep',
  templateUrl: './register-finalstep.component.html',
  styleUrls: ['./register-finalstep.component.scss']
})
export class RegisterFinalstepComponent implements OnInit {

  
    regForm: FormGroup;
    loading = false;
    submitted = false;
    valueOptChose:string;

    urls: any;
    urls1: any;
    urls2: any;
    DataImg = [] ;
    DataImg1 = [] ;
    DataImg2 = [] ;

    LanguageData: Langue[] = [];
    dropdownList = [];
    selectedItems = [];
    dropdownSettings = {};

    @Input() regData: RegData; 
    isFormValid: boolean = false;

    ExperienceCol: boolean = false;
    textNoUplaod: boolean = false;
    textNoUplaod1: boolean = false;
    textNoUplaod2: boolean = false;
    colStyle: string;
    ChosenOptValue: string;
    HideFinishElement:boolean= false;
    state1:boolean= true;
    state2:boolean= true;
    state3:boolean= true;

  constructor(  private formLog: FormBuilder,private route: ActivatedRoute,
		        private router: Router,private authenticationService: AuthenticationService,
		        private userService: UserService,private SetLangRole: LanguageService,
		        private alertService: AlertService,private regDataService: RegisterDataService) { }

  ngOnInit() {

  	this.valueOptChose = localStorage.getItem("optChooseText");

    this. colStyle = 'experiencecol';

  	// To initialize FormGroup  
    this.regForm = this.formLog.group({    
      'firstname':['', Validators.required],
      'surname':['', Validators.required],
      'langues':['',Validators.required],
      'experience':[''],
      'fonctionName':[this.valueOptChose, Validators.required]
    });  

     this.regData = this.regDataService.getRegData();
     this.isFormValid = this.regDataService.isFormRegValid();

     this.SetLangRole.getAll().pipe(first()).subscribe((lang: any) => { 
            this.LanguageData = lang.data; 
        });

     this.dropdownSettings = {
      singleSelection: false,
      idField: 'nom_langue',
      textField: 'nom_langue',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      maxHeight:400,
      itemsShowLimit: 4,
      allowSearchFilter: true
    };


    if(this.valueOptChose=='Offer Helps'){
      this.ExperienceCol=true;
      this.HideFinishElement = false; 
    }
    else{
      this.ExperienceCol=false;
      this.HideFinishElement = true; 
    }

  }

    onItemSelect(item: any) {
    //console.log(item);
    }

    onSelectAll(items: any) {
      //console.log(items);
    }

    logTextFunction1(value: string): void {
      this.regForm.patchValue({experience: value});
      this.HideFinishElement = true; 
      this.state2=true; 
      this.state3=true;
      this.state1 = !this.state1; 
    }

    logTextFunction2(value: string): void {
      this.regForm.patchValue({experience: value});
      this.HideFinishElement = true; 
      this.state1=true; 
      this.state3=true;
      this.state2 = !this.state2; 
    }

    logTextFunction3(value: string): void {
      this.regForm.patchValue({experience: value});
      this.HideFinishElement = true; 
      this.state1=true; 
      this.state2=true;
      this.state3 = !this.state3; 
    }

    uploadProfilePic(event) {
    this.urls = [];
    this.DataImg = [] ;
    let files = event.target.files;
    if (files) {
      for (let file of files) {
        let reader = new FileReader();
        reader.onload = (e: any) => {
          this.urls.push(e.target.result);
          this.DataImg.push({picture:e.target.result});
        }       
        reader.readAsDataURL(file);
      }
    }
  }

  //update the is card picture
  uploadProfilePic1(event) {
    this.urls1 = [];
    this.DataImg1 = [] ;
    let files = event.target.files;
    if (files) {
      for (let file of files) {
        let reader = new FileReader();
        reader.onload = (e: any) => {
          this.urls1.push(e.target.result);
          this.DataImg1.push({picture:e.target.result});
        }       
        reader.readAsDataURL(file);
      }
    }
  }

  //uploade the picture of the user and its id card
  uploadProfilePic2(event) {
    this.urls2 = [];
    this.DataImg2 = [] ;
    let files = event.target.files;
    if (files) {
      for (let file of files) {
        let reader = new FileReader();
        reader.onload = (e: any) => {
          this.urls2.push(e.target.result);
          this.DataImg2.push({picture:e.target.result});
        }       
        reader.readAsDataURL(file);
      }
    }
  }


  		  // convenience getter for easy access to form fields
      get f() { return this.regForm.controls; }
	  
	  // Executed When Form Is Submitted  
	  onSubmit() {
       
       this.submitted = true;

       // stop if upload no images
        if(this.DataImg.length < 1 ) {
         this.textNoUplaod = true;
         return;
       }
       if(this.DataImg1.length < 1 ) {
        this.textNoUplaod1 = true;
        return;
      }
      if(this.DataImg2.length < 1 ) {
        this.textNoUplaod2 = true;
        return;
      }

        // stop here if form is invalid
        if (this.regForm.invalid) {
            return;
        }

        this.loading = true; 

        this.regDataService.setFinalStep(this.regForm.value);

        this.regDataService.SetPic(this.DataImg[0].picture); 
        this.regDataService.SetPic1(this.DataImg1[0].picture);
        this.regDataService.SetPic2(this.DataImg2[0].picture);

        let stringToSplit = this.f.langues.value.toString().split(",");

        var data = [];
        for(var i=0; i< this.f.langues.value.length; i++)  {
            data.push({langue:stringToSplit[i]});
        }
       // console.log("Form Values :"+JSON.stringify(this.regForm.value));
       // console.log("Form Values :"+JSON.stringify(data)); 
       
        this.regDataService.SetLang(data); 
console.log(this.regData);
        this.userService.registerForm(this.regData)
               .pipe(first())
               .subscribe(

                 //on success  
                (dataGet : any) => { 
                     this.alertService.success(dataGet.message, true);
                     this.regData = this.regDataService.resetRegFormData();
                     this.isFormValid = false; 

                     localStorage.removeItem('suggestionsTerms');
                     localStorage.removeItem('optChooseText');
                },

                //on error
                error => {
                  this.alertService.error(error);
                     this.loading = false;
                },

                 //on complete
                () => { 
                     this.router.navigate(['/logon'], { queryParams: { state: 'true' } });
                });        
       
    }

    onPrevious() {

       localStorage.removeItem('optChooseText');
       this.regDataService.ResetSousCateg(); 
       this.router.navigate(['/register-step3']);
    }


}    