import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms'; 
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { RegisterFinalstepRoutingModule } from './register-finalstep-routing.module';
import { RegisterFinalstepComponent } from './register-finalstep.component';
import { AlertModule } from '../../_directives/alert.module';

@NgModule({
  imports: [
    CommonModule,
    NgMultiSelectDropDownModule.forRoot(),
    RegisterFinalstepRoutingModule,AlertModule,
    FormsModule,ReactiveFormsModule
  ],
  declarations: [RegisterFinalstepComponent]
})
export class RegisterFinalstepModule { }
