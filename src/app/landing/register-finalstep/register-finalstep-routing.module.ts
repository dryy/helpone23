import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegisterFinalstepComponent } from './register-finalstep.component';


const routes: Routes = [

	{
        path: '', component: RegisterFinalstepComponent
    }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegisterFinalstepRoutingModule { }
