import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterFinalstepComponent } from './register-finalstep.component';

describe('RegisterFinalstepComponent', () => {
  let component: RegisterFinalstepComponent;
  let fixture: ComponentFixture<RegisterFinalstepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterFinalstepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterFinalstepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
