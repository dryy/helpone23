import { ErrorNotFoundModule } from './error-not-found.module';

describe('ErrorNotFoundModule', () => {
  let errorNotFoundModule: ErrorNotFoundModule;

  beforeEach(() => {
    errorNotFoundModule = new ErrorNotFoundModule();
  });

  it('should create an instance', () => {
    expect(errorNotFoundModule).toBeTruthy();
  });
});
