import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms'; 

import { RegisterStep4RoutingModule } from './register-step4-routing.module';
import { RegisterStep4Component } from './register-step4.component';

@NgModule({
  imports: [
    CommonModule,
    RegisterStep4RoutingModule,
    FormsModule,ReactiveFormsModule
  ],
  declarations: [RegisterStep4Component]
})
export class RegisterStep4Module { }
