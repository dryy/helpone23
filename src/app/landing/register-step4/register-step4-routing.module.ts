import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterStep4Component } from './register-step4.component';

const routes: Routes = [

	{
        path: '', component: RegisterStep4Component
    }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegisterStep4RoutingModule { }
