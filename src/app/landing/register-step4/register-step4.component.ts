import { Component, OnInit } from '@angular/core';

// Must import to use Forms functionality  
import { FormBuilder, FormGroup, Validators ,FormsModule,NgForm,FormControl,FormArray } from '@angular/forms'; 
import { first } from 'rxjs/operators'; 
import { Router,ActivatedRoute } from '@angular/router';
import { AlertService, RegisterDataService,ServiceCatService} from '../../_services';

@Component({
  selector: 'app-register-step4',
  templateUrl: './register-step4.component.html',
  styleUrls: ['./register-step4.component.scss']
})
export class RegisterStep4Component implements OnInit {
  
  cats:any;
  subcats_data:any;
  chosenOption:any;
  chosenOptionTitle:any;
  listChosen:any;

  bntText: string ='Skip';

  sec_col:boolean= false;
  third_col:boolean= false;
  
  stateChecked:string='';
  
  TableData: any[] = [];

  valueOptChose:string;

constructor(  private formLog: FormBuilder,private route: ActivatedRoute,
          private router: Router,private alertService: AlertService,
          private SetServiceCat: ServiceCatService,
          private regDataService: RegisterDataService) { 
          
              // override the route reuse strategy
            this.router.routeReuseStrategy.shouldReuseRoute = function() {
              return false;
            };
            
          }

ngOnInit() {

  this.valueOptChose = localStorage.getItem("optChooseText");

  this.SetServiceCat.getAll().pipe(first()).subscribe(catsData => { 
    this.cats = catsData; 
  });


}


loadSubCat(value:string,name:string){
  
  this.SetServiceCat.getSubCatbyCatID(value).pipe(first()).subscribe(subcats => { 
    this.subcats_data = subcats;
   }); 
   this.chosenOptionTitle = name;
   this.sec_col = true;
}


 ListSelectedItems(event,list:number) {
    if (event.target.checked) {
        if (this.TableData.indexOf((event.target.name)) < 0 && this.TableData.length < list ) { 
                this.TableData.push(({'sousCategorie_id':event.target.value,'sousCategoryName':event.target.name}));

        }

    } else {
        
       //let index = this.TableData.findIndex(d => d.sousCategorie_id === event.target.value); //find index in your array
       //this.TableData.splice(index, 1);//remove element from array

	        for(let i = 0; i < this.TableData.length; i++){
		        if(this.TableData[i].sousCategorie_id == event.target.value) {
		            this.TableData.splice(i, 1);
		            i--;
		        }
	       }
    }
    this.third_col = true;

    let SelectedItems = this.TableData.length;
    this.chosenOption = SelectedItems+' / '+list+' selected';

	  if (SelectedItems > 0 ){
	  	this.bntText='Next';
	  }
	  else {
	  	this.bntText='Skip';
	  }

}


  onNext() {

     this.regDataService.SetSousCateg(this.TableData); 
     this.router.navigate(['/register-finalstep']);
     
  }

  onPrevious() {
     localStorage.removeItem('optChooseText');
     this.regDataService.ResetSousCateg();  
     this.router.navigate(['/register-step3']);
  }


}    