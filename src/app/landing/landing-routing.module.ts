import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingComponent } from './landing.component';

import { WorkflowGuard }        from '../_services/workflow-guard.service';
import { CategoryListComponent } from './category-list/category-list.component';

const routes: Routes = [

   {

        path: '',
        component: LandingComponent,
        children: [
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', loadChildren: './homepage/homepage.module#HomepageModule' },
            { path: 'help', loadChildren: './help/help.module#HelpModule' },
            { path: 'UsefulLinks/:thelink', loadChildren: './links/links.module#LinksModule' },
            { path: 'terms/:theterm', loadChildren: './terms/terms.module#TermsModule' },
            { path: 'askForHelp-step2/:subCatId', loadChildren: './help-step2/help-step2.module#HelpStep2Module' },

            { path: 'logon', loadChildren: './login/login.module#LoginModule' },
            { path: 'confirm', loadChildren: './confirmation/confirmation.module#confirmationModule' },

            { path: 'forgotPassword', loadChildren: './forgotpwd/forgotpwd.module#ForgotpwdModule' },
            { path: 'pwdcode/:id', canActivate: [WorkflowGuard], loadChildren: './pwdcode/pwdcode.module#PwdcodeModule' },
            { path: 'newpass/:userid', canActivate: [WorkflowGuard], loadChildren: './newpass/newpass.module#NewpassModule' },
            { path: 'blocpass', loadChildren: './blocpass/blocpass.module#BlocpassModule' },
            { path: 'category-list', component: CategoryListComponent },
            { path: 'register', loadChildren: './register/register.module#RegisterModule' },
            { path: 'register-step2', canActivate: [WorkflowGuard], loadChildren: './register-step2/register-step2.module#RegisterStep2Module' },
            { path: 'register-step3', loadChildren: './register-step3/register-step3.module#RegisterStep3Module' },
            { path: 'register-step4', loadChildren: './register-step4/register-step4.module#RegisterStep4Module' },
            { path: 'register-finalstep', canActivate: [WorkflowGuard], loadChildren: './register-finalstep/register-finalstep.module#RegisterFinalstepModule' },

            { path: 'error404', loadChildren: './error-not-found/error-not-found.module#ErrorNotFoundModule' }
        ]
    }



];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [WorkflowGuard]
})
export class LandingRoutingModule { }
