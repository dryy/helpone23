import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms'; 
import { PwdcodeRoutingModule } from './pwdcode-routing.module';
import { PwdcodeComponent } from './pwdcode.component';

import { AlertModule } from '../../_directives/alert.module';


@NgModule({
  imports: [
    CommonModule,
    PwdcodeRoutingModule,AlertModule,
    FormsModule,ReactiveFormsModule
  ],
  declarations: [PwdcodeComponent]
})
export class PwdcodeModule { }
