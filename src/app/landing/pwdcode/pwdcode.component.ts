import { Component, OnInit } from '@angular/core';

// Must import to use Forms functionality  
import { FormBuilder, FormGroup, Validators ,FormsModule,NgForm } from '@angular/forms'; 
import { first } from 'rxjs/operators'; 
import { Router,ActivatedRoute } from '@angular/router';
import { AlertService, ForgotpasswordService} from '../../_services';

@Component({
  selector: 'app-pwdcode',
  templateUrl: './pwdcode.component.html',
  styleUrls: ['./pwdcode.component.scss']
})
export class PwdcodeComponent implements OnInit {

    PasswordCodeForm: FormGroup;
    submitted = false;

  constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private forgotPwdService: ForgotpasswordService,
        private alertService: AlertService) { }

  ngOnInit() {

  		this.PasswordCodeForm = this.formBuilder.group({
            code: ['', [Validators.required]]
        });
  }

  // convenience getter for easy access to form fields
    get f() { return this.PasswordCodeForm.controls; }


       onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.PasswordCodeForm.invalid) {
            return;
        }

        this.forgotPwdService.checkCodeEmail(this.f.code.value)
            .pipe(first())
            .subscribe(
                (dataGet : any) => {
                   localStorage.removeItem('UserID'); 
                   localStorage.setItem('UserID',dataGet.data.user_id.toString()); 
                   this.alertService.success(dataGet.message, true);
                   this.forgotPwdService.setSecStep();
                   this.router.navigate(['newpass',dataGet.data._id]);
                    
                },
                error => {
                    this.alertService.error(error);
                });
        
    }

}