import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PwdcodeComponent } from './pwdcode.component';

const routes: Routes = [

	{
        path: '', component: PwdcodeComponent
    }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PwdcodeRoutingModule { }
