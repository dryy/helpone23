import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PwdcodeComponent } from './pwdcode.component';

describe('PwdcodeComponent', () => {
  let component: PwdcodeComponent;
  let fixture: ComponentFixture<PwdcodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PwdcodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PwdcodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
