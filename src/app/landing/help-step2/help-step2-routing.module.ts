import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HelpStep2Component } from './help-step2.component';

const routes: Routes = [

	{
        path: '', component: HelpStep2Component
    }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HelpStep2RoutingModule { }
