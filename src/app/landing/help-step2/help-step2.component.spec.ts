import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HelpStep2Component } from './help-step2.component';

describe('HelpComponent', () => {
  let component: HelpStep2Component;
  let fixture: ComponentFixture<HelpStep2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HelpStep2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpStep2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
