import { Component, OnInit } from '@angular/core';
import { UserInfo, AskData } from 'src/app/_models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AskhelpService, ProfileService, AddressService, AlertService, ServicetransactionService } from 'src/app/_services';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-help-step2',
  templateUrl: './help-step2.component.html',
  styleUrls: ['./help-step2.component.scss']
})
export class HelpStep2Component implements OnInit {

  ValUser: string;
  currentUser: UserInfo;
  dataArray: any;
  categorie: string='';
  sous_cat : any;
  etape: number=1;
  laSousCategorie: any;
  askForm: FormGroup;
  askData: AskData;
  urls = new Array<string>();
  DataImg =[];
  submitted = false;
  textUplaod :boolean = false;
  textNoUplaod :boolean = false;
  dateBorder :boolean = false;
  myDate = new Date();
  helpInfo: any;

  askForm2: FormGroup;
  saveAdressFirstForm: FormGroup;
  saveAdressSecForm: FormGroup;
  AdressData: any;
  ShowContentAddress : boolean[]=[];
  MyInfo: any;
  submittedFirstForm = false;
  submittedSecForm = false;
  ShowUpdateForm = [];
  priceBorder :boolean = false;
  TableData: any[] = [];
  checkField :boolean = false;
  TableDataModified: any[] = [];
  isFormValid: boolean = false;

  adress_title_value:string;
  adress_value:string;
  zip_code_value:string;
  IdAddress:string;
  amountChecked:number=0;
  show_delete_content=false;
  show_address_list=true;

  checkIfDeliversAreSelected = [];
  checkIfBillsAreSelected = [];
  minDate: string;

  constructor(private profileServ: ProfileService,
              private router: Router, 
              private alertService: AlertService,
              private askDataService: AskhelpService, 
              private addressService: AddressService,
              private ServiceTransService: ServicetransactionService,
              private formLog: FormBuilder) { 
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.laSousCategorie = localStorage.getItem('laSousCategorie');
    this.ValUser=this.currentUser.user._id;

  }

  ngOnInit() {
    this.getDate();

    	// To initialize FormGroup  
      this.askForm2 = this.formLog.group({    
        'IdUser':[this.ValUser, Validators.required],
        'min_price':['', Validators.required],
        'max_price':['', Validators.required],
      });

        // To initialize FormGroup  
    this.saveAdressFirstForm = this.formLog.group({    
      'user_id':[this.ValUser, Validators.required],
      'address_title':['', Validators.required],
      'address':['', Validators.required],
      'zip_code_postal':['', Validators.required],
      'telephone':['', Validators.required],
      'city':['', Validators.required],
      'email':['', Validators.compose([Validators.required,Validators.email])]
    }); 

    this.saveAdressSecForm = this.formLog.group({    
      'user_id':[this.ValUser, Validators.required],
      '_id':['', Validators.required],
      'address_title':['', Validators.required],
      'address':['', Validators.required],
      'zip_code_postal':['', Validators.required],
      'telephone':['', Validators.required],
      'city':['', Validators.required],
      'email':['', Validators.compose([Validators.required,Validators.email])]
    });

    this.loadAddresses();
    this.getProfileInfoData();
    this.askData = this.askDataService.getAskData(); 

    this.categorie = localStorage.getItem('categorie');
    this.sous_cat = JSON.parse(localStorage.getItem('sous_categorie'));
    this.sous_cat = this.sous_cat.dataGroupedBy;
    console.log(this.categorie);
    console.log(this.sous_cat);

    	// To initialize FormGroup  
      this.askForm = this.formLog.group({
        'description':['', Validators.required],
        'date1':['', Validators.required],
        'time1':['', Validators.required],
        'date2':['', Validators.required],
        'time2':['', Validators.required],
        'date3':['', Validators.required],
        'time3':['', Validators.required]
      }); 
      
      this.isFormValid = this.askDataService.isFormAskValid();
  }

  // Cette fonction permet de récupérer la date courante afin d'indiquer dans le template le fait que l'utilisateur ne peut pas choisir une date antérieure
  getDate(){
    let date: Date = new Date();
    console.log("Date = " + date);
    console.log(date.getMonth());
    let mois = date.getMonth()+1+'';
    let dat = date.getDate()+'';
    if(mois.length==1){
      mois=0+mois;
    }
    if(dat.length==1){
      dat=0+dat;
    }
    this.minDate = date.getFullYear()+'-'+mois+'-'+dat;
    console.log("La Date = " + this.minDate);
  }

    // convenience getter for easy access to form fields
    get f() { return this.askForm.controls; }
    get f2() { return this.askForm2.controls; }
    get g() { return this.saveAdressFirstForm.controls; }
    get h() { return this.saveAdressSecForm.controls; }

    private loadAddresses() {
      this.addressService.getByUserID(this.ValUser).pipe(first()).subscribe(dataAdress => { 
          this.AdressData = dataAdress; 
           
           for(let t = 0; t < this.AdressData.data.length; t++) {
             this.ShowContentAddress[t] = true;
           } 
      });

    }

    private getProfileInfoData() {
      this.profileServ.getProfileInfo(this.ValUser).pipe(first()).subscribe(UserInfoValues => {          
            this.MyInfo = UserInfoValues; 
      });
    }
  
  
    // Executed When Form Is Submitted  
    SubmitForm() {
       
      this.submittedFirstForm = true;

      // stop here if form is invalid
      if (this.saveAdressFirstForm.invalid) {
          return;
      } 

     this.addressService.save(this.saveAdressFirstForm.value)  
    .subscribe(
     //on success 
     (data : any) => { 
          this.alertService.success(data.message, true);
          this.submittedFirstForm = false;
          this.saveAdressFirstForm.patchValue({
          "address_title": '',"city": '',"telephone": '',
          "email": '',"address": '',"zip_code_postal": '' 
           });
          
          
     },
     //on error
     error => {
         this.alertService.error(error);
     },
      //on complete
     () => { 
          this.loadAddresses(); 
     });


    }

  // when user clicks the 'update' link
SubmitUpdate(index: number) {
       
  this.submittedSecForm = true;

  // stop here if form is invalid
  if (this.saveAdressSecForm.invalid) {
      return;
  }

 this.addressService.update(this.saveAdressSecForm.value).pipe(first()).subscribe(

     //on success 
     (data : any) => { 
          this.alertService.success(data.message, true);
          this.submittedSecForm = false;
          this.cancelUpdate(index);
          
     },

     //on error
     error => {
          this.alertService.error(error);
     },

      //on complete
     () => { 
          
          this.loadAddresses(); 
     }); 


}

  	detectFiles(event) {
      this.urls = [];
      this.DataImg = [];
      let files = event.target.files;
      if (files) {
        for (let file of files) {
          let reader = new FileReader();
          reader.onload = (e: any) => {
            this.urls.push(e.target.result);
            this.DataImg.push({picture:e.target.result});
          }
          
          reader.readAsDataURL(file);
        }   
  
        
      }
    }


  // when user clicks the 'delete' button
  deleteAddress(id:string,address:string,address_title:string,zip_code_postal:string) {

    this.adress_title_value=address_title;
    this.adress_value=address;
    this.zip_code_value=zip_code_postal;
    this.IdAddress=id;

    this.show_delete_content=true;
    this.show_address_list=false;
  }


    // when user clicks the 'edit' link
    editAddress(id:string,address:string,telephone:string,address_title:string,zip_code_postal:string,city:string,email:string,index: number){

      this.ShowUpdateForm[index] = true;
      for(let i = 0; i < this.ShowUpdateForm.length; i++) {
        if (i !== index) {
          this.ShowUpdateForm[i] = false;
        }
      }  
  
      this.ShowContentAddress[index] = false;
      for(let i = 0; i < this.ShowContentAddress.length; i++) {
        if (i !== index) {
          this.ShowContentAddress[i] = true;
        }
      } 
      
      this.saveAdressSecForm.patchValue({
      "_id": id,"address_title": address_title,"city": city,"telephone": telephone,
      "email": email,"address": address,"zip_code_postal": zip_code_postal 
        });    
      
    }
          
    // when user clicks the 'cancel' link
  cancelUpdate(index: number){
    this.ShowUpdateForm[index] = false;
    this.ShowContentAddress[index] = true;
  }
          
  onSubmi(){

  }
    onSubmit() {
       
      this.submitted = true;

   // stop if upload more than 6 images
     if(this.DataImg.length > 6 ) {
      this.textUplaod = true;
      return;
    }

    // stop if upload no images
     if(this.DataImg.length < 1 ) {
      this.textNoUplaod = true;
      return;
    }

     // stop here if form is invalid
     if (this.askForm.invalid) {
         return;
     }
     let helpData : any = {
      sousCategory_id:localStorage.getItem('idlaSousCategorie'),
      categoryService_id:localStorage.getItem('idlaCategorie'),
      description:this.f.description.value };
    this.askDataService.setHelpData(helpData);  
    
    this.askDataService.SetImageService(this.DataImg); 

      let dateVal1 = moment(this.f.date1.value).format('YYYY-MM-DD');
      let timeVal1 = moment(this.f.time1.value).format('hh:mm A');
      let dateVal2 = moment(this.f.date2.value).format('YYYY-MM-DD');
      let timeVal2 = moment(this.f.time2.value).format('hh:mm A');
      let dateVal3 = moment(this.f.date3.value).format('YYYY-MM-DD');
      let timeVal3 = moment(this.f.time3.value).format('hh:mm A');
      let myDates = moment(this.myDate).format('YYYY-MM-DD');
      console.log(myDates);
      if( (dateVal1 < myDates) || (dateVal2 < myDates) || (dateVal3 < myDates) ) {
       console.log("daaaaate 1 : "+ dateVal1+timeVal1);
       console.log("daaaaate 2 : "+ dateVal2+timeVal2);
       console.log("daaaaate 3 : "+ dateVal3+timeVal3);
       this.dateBorder = true;
       return;
     }
     else{
         var dataDT = [];
         dataDT.push({time_service:timeVal1,date_service:dateVal1});
         dataDT.push({time_service:timeVal2,date_service:dateVal2});
         dataDT.push({time_service:timeVal3,date_service:dateVal3});
     
      this.askDataService.SetTimeDate(dataDT);     
     }
     this.helpInfo = this.askDataService.getFirstStep();
     console.log(this.helpInfo);
     this.etapeSuiv();

   }

    
  etapeSuiv(){
    if(this.etape<4)
    this.etape++;
  }

  etapePrev(){
    if(this.etape>1)
    this.etape--;
  }

  // Executed When Form Is Submitted  
  onSubmit2() {
       
    this.submitted = true;

   // stop here if form is invalid
   if (this.askForm2.invalid) {
       return;
   }

   // stop if price mix is greather than price max
   if( (+this.f2.min_price.value) >= (+this.f2.max_price.value ) ) {
     this.priceBorder = true;
    return;
   }

   else {
     this.priceBorder = false;
   }

   // Control if Deliver and Bill Adresses were checked
   if( this.TableData.length !== 2 ) {
     this.checkField = true;
    return;
   }

   else {
     this.checkField = false;
   }

   this.TableDataModified=this.TableData;

   //remove property name on this array
   for (var i = 0; i < this.TableDataModified.length; i++) {
       delete this.TableDataModified[i].name;
   }

   this.askDataService.setSecStep(this.askForm2.value); 

   this.askDataService.SetAdressesInfo(this.TableDataModified); 

   this.askDataService.SetUserID(this.f2.IdUser.value); 
  
   console.log(this.askData);
   this.askData = this.askDataService.getHelpData(); 
     console.log(this.askData);
    this.ServiceTransService.save(this.askData)
    .pipe(first())
    .subscribe(

      //on success 
     (dataGet : any) => { 
          this.askData = this.askDataService.resetAskFormData();
          this.isFormValid = false;
          console.log('aaaaaa : '+ dataGet.message)
         this.alertService.success(dataGet.message, true);
          
     },

     //on error
     error => {
          this.alertService.error(error);
     },

      //on complete
     () => { 
      this.etapeSuiv();
      //  this.router.navigate(['/access/askHelp-step1'], { queryParams: { state: 'true' } });
     });


 }


 checkSelected(e, id,ArrayLength,address,telephone,address_title,zip_code_postal,city,email) {

  this.checkIfDeliversAreSelected[id] = false;
  //this.checkIfBillsAreSelected[id] = true;
  
  if(e.target.checked) {

  if (this.TableData.indexOf((e.target.name)) < 0 ) { 
      this.TableData.push(({ 'name':e.target.name,
                             'addressUser_id':e.target.value,
                             'address_title':address_title,
                             'address':address,
                             'zip_code_postal':zip_code_postal,
                             'telephone':telephone,
                             'city':city,
                             'email':email,
                             'deliver_help_to_this_address':1,
                             'bill_to_this_address':0}));
  }

    for(let i = 0; i < ArrayLength; i++) {
      if (i !== id) {
        this.checkIfDeliversAreSelected[i] = true;
      }
      else {
        this.checkIfDeliversAreSelected[i] = false;
      }
      
    }  
  }
  else{              
       
       for(let i = 0; i < ArrayLength; i++) {
          this.checkIfDeliversAreSelected[i] = false;
      } 

      for(let j = 0; j < this.TableData.length; j++){
        if(this.TableData[j].address_title == e.target.name) {
            this.TableData.splice(j, 1);
            j--;
        }
     }
  }

//  console.log('TABCHOSEN :'+JSON.stringify(this.TableData));
  
}

checkBillSelected(e, id,ArrayLength,address,telephone,address_title,zip_code_postal,city,email) {

  this.checkIfBillsAreSelected[id] = false;

  if(e.target.checked) {

  if (this.TableData.indexOf((e.target.name)) < 0 ) { 
      this.TableData.push(({ 'name':e.target.name,
                             'addressUser_id':e.target.value,
                             'address_title':address_title,
                             'address':address,
                             'zip_code_postal':zip_code_postal,
                             'telephone':telephone,
                             'city':city,
                             'email':email,
                             'deliver_help_to_this_address':0,
                             'bill_to_this_address':1}));
  }

    for(let i = 0; i < ArrayLength; i++) {
      if (i !== id) {
        this.checkIfBillsAreSelected[i] = true;
      }
      else {
        this.checkIfBillsAreSelected[i] = false;
      }
      
    }  
  }
  else{              
       
       for(let i = 0; i < ArrayLength; i++) {
          this.checkIfBillsAreSelected[i] = false;
      } 

      for(let j = 0; j < this.TableData.length; j++){
        if(this.TableData[j].address_title == e.target.name) {
            this.TableData.splice(j, 1);
            j--;
        }
     }
  }

 // console.log('TABCHOSEN :'+JSON.stringify(this.TableData));
  
}


  // show addresses list
  showListAddress($event){
    this.loadAddresses();
    this.show_delete_content=false;
    this.show_address_list=true;
}


  
}
