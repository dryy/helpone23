import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HelpStep2RoutingModule } from './help-step2-routing.module';
import { AlertModule } from 'src/app/_directives/alert.module';
import { HelpStep2Component } from './help-step2.component';
import { CustomMaterialModule } from 'src/app/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OwlDateTimeModule, OwlNativeDateTimeModule, OWL_DATE_TIME_FORMATS } from 'ng-pick-datetime';
import { DeleteAdressComponent } from './delete-adress/delete-adress.component';


export const MY_MOMENT_FORMATS = {
  parseInput: 'l LT',
  fullPickerInput: 'L LT',
  datePickerInput: 'L',
  timePickerInput: 'LT',
  monthYearLabel: 'MMM YYYY',
  dateA11yLabel: 'LL',
  monthYearA11yLabel: 'MMMM YYYY',

};

@NgModule({
  declarations: [HelpStep2Component, DeleteAdressComponent],
  imports: [
    CommonModule,
    HelpStep2RoutingModule,AlertModule,
    OwlDateTimeModule, OwlNativeDateTimeModule ,
    CustomMaterialModule,FormsModule,ReactiveFormsModule
  ],
  providers: [
    // use french locale. This provider is causing trouble when displaying date, so that is why I comment it
    //  {provide: OWL_DATE_TIME_FORMATS, useValue: MY_MOMENT_FORMATS},
  ]
})
export class HelpStep2Module { }
