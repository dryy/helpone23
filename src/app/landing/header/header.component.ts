import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators'; 
import { Router,ActivatedRoute } from '@angular/router';
import { UserInfo } from '../../_models';

import { AuthenticationService,ProfileService } from '../../_services';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { FileNameDialogComponent } from '../../access/file-name-dialog/file-name-dialog.component';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    currentUser: UserInfo;
    ValUser: string;
    MyInfo: any;
    fileNameDialogRef: MatDialogRef<FileNameDialogComponent>;

    constructor ( private authenticationService: AuthenticationService,
                    private route: ActivatedRoute,
                    private router: Router,
                    private profileServ: ProfileService,
                    private dialog: MatDialog ) {   }



    ngOnInit() {

            this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

            if (this.currentUser) {
                this.ValUser=this.currentUser.user._id;
                this.getProfileInfoData();
                console.log(this.currentUser);
            } else {
                //console.log('not get on init!');
            }

        
    }

    private getProfileInfoData() {
        this.profileServ.getProfileInfo(this.ValUser).pipe(first()).subscribe(UserInfoValues => {          
                this.MyInfo = UserInfoValues; 
        });
    }

    logoutApp(): void {
            console.log("Logout");
            this.authenticationService.logout();
    }

    GotoAskUrl(): void {
            this.router.navigate(['/access/askHelp-step1']);
    }

    GotoRecentsHelps(): void {
            this.router.navigate(['/access/helpBanks']);
    }

    openAddFileDialog() {
        this.fileNameDialogRef = this.dialog.open(FileNameDialogComponent, {
                hasBackdrop: false
        });
    }
    
    askHelp(){
        localStorage.removeItem('idlaCategorie');
        localStorage.removeItem('idlaSousCategorie');
        this.router.navigate(["/access/askHelp-step1"]);
    }

}
