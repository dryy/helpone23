import { Component, OnInit } from '@angular/core';
import { ServiceCatService } from 'src/app/_services';
import { first } from 'rxjs/operators';
import { ModalComponent } from 'src/app/components/modal/modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

@Component({
    selector: 'app-category-list',
    templateUrl: './category-list.component.html',
    styleUrls: ['./category-list.component.scss']
})
export class CategoryListComponent implements OnInit {

    services_category: any[] = [];
    categ: any;
    subcats_data: any;
    categName: any;
    constructor(private SetServiceCat: ServiceCatService,
                private router: Router,
                private modalService: NgbModal) { }

    ngOnInit() {
        this.loadAllCategories();
    }

    private loadAllCategories() {
        this.SetServiceCat.getAll().subscribe(categories => {
        this.categ =  categories;
            this.services_category = this.categ.data;
            console.log(this.services_category);
        });
        // this.SetServiceCat.getAllSubCateg().pipe(first()).subscribe(subcats => {
        //     this.subcats_data = subcats;
        //     this.subcats_data = this.subcats_data.data;
        //     console.log("data : ",this.subcats_data);
        // });
    }

    loadName(event){
        console.log(event);
        document.getElementById("myCheck").click();
        this.SetServiceCat.getAllSubCateg().pipe(first()).subscribe(subcats => {
            this.subcats_data = subcats;
            this.subcats_data = this.subcats_data.data;
            let a: any[]=[];
            for(let sc of this.subcats_data){
                if(event._id == sc.categoryServiceId){
                    a.push(sc);
                }
            }
            this.subcats_data = a;
            console.log("data : ",this.subcats_data);
        });
    }
    goToAsk(event){
        console.log(event);
        localStorage.setItem('categorie', event.categoryName);
        localStorage.setItem('idlaCategorie', event.categoryServiceId);
        localStorage.setItem('sous_categorie', JSON.stringify(event));
        localStorage.setItem('laSousCategorie', event.souscategoryName); 
        localStorage.setItem('idlaSousCategorie', event._id);
        console.log(event._id);
        this.router.navigate(['/access/askHelp-step1/']);
    }
    nothing(){
        return
    }
    openModal() {
        const modalRef = this.modalService.open(ModalComponent);
        modalRef.componentInstance.subcats_data = this.subcats_data;
    }

}
