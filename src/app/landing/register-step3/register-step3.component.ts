import { Component, OnInit } from '@angular/core';

// Must import to use Forms functionality  
import { FormBuilder, FormGroup, Validators ,FormsModule,NgForm } from '@angular/forms'; 
import { Router,ActivatedRoute } from '@angular/router';
import { AlertService, RegisterDataService} from '../../_services';
import { Step2 }   from '../../_models/registerData';

@Component({
  selector: 'app-register-step3',
  templateUrl: './register-step3.component.html',
  styleUrls: ['./register-step3.component.scss']
})
export class RegisterStep3Component implements OnInit {

    regForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    fstep2: Step2;
    suggestion : any;
    chosenOption : any ; 
    ChosenOptNum: number;
    bntStyle: string;
    HideElement:boolean= false;
    state1:boolean= true;
    state2:boolean= true;

  constructor(  private formLog: FormBuilder,private route: ActivatedRoute,
            private router: Router,private alertService: AlertService,
            private regDataService: RegisterDataService) { }

  ngOnInit() {

  	this. bntStyle = 'log-bg-white';

  	// To initialize FormGroup  
    this.regForm = this.formLog.group({    
    });  

  }

    logTextFunction1(value: string): void {
      this.chosenOption = `You Choose '${value}'\n`;
      this.ChosenOptNum = 1;
      this.HideElement = true; this.state2=true; this.state1 = !this.state1; 
    }

    logTextFunction2(value: string): void {
      this.chosenOption = `You Choose '${value}'\n`;
      this.ChosenOptNum = 2;
      this.HideElement = true; this.state1=true; this.state2 = !this.state2; 
    }

  	onNext(value:number) {
       if (value==2) {
        localStorage.removeItem('optChooseText');  
        localStorage.setItem('optChooseText','Ask for Helps');
        this.router.navigate(['/register-finalstep']);
       }

       if (value==1) {
        localStorage.removeItem('optChooseText');  
        localStorage.setItem('optChooseText','Offer Helps');
        this.router.navigate(['/register-step4']);
       }
       
    }

    onPrevious() {
       localStorage.removeItem('optChooseText'); 
       this.router.navigate(['/register-step2']);
    }


}    