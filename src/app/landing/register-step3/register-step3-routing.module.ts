import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterStep3Component } from './register-step3.component';

const routes: Routes = [

	{
        path: '', component: RegisterStep3Component
    }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegisterStep3RoutingModule { }
