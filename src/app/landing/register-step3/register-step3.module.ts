import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms'; 

import { RegisterStep3RoutingModule } from './register-step3-routing.module';
import { RegisterStep3Component } from './register-step3.component';

@NgModule({
  imports: [
    CommonModule,
    RegisterStep3RoutingModule,
    FormsModule,ReactiveFormsModule
  ],
  declarations: [RegisterStep3Component]
})
export class RegisterStep3Module { }
