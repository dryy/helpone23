import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlertModule } from 'src/app/_directives/alert.module';
import { TermsComponent } from './terms.component';
import { CustomMaterialModule } from 'src/app/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TermsRoutingModule } from './terms-routing.module';



@NgModule({
  declarations: [TermsComponent],
  imports: [
    CommonModule,
    TermsRoutingModule,AlertModule,
    CustomMaterialModule,FormsModule,ReactiveFormsModule
  ]
})
export class TermsModule { }
