import { Component, OnInit } from '@angular/core';
import { UserInfo } from 'src/app/_models';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.scss']
})
export class TermsComponent implements OnInit {

  lien: any;

  constructor(private route: ActivatedRoute) { 
    // localStorage.setItem('suggestionsTerms',JSON.stringify(data.suggestions));
  }

  ngOnInit() {
    this.getParam();
  }

  getParam(){
    this.lien = this.route.snapshot.paramMap.get("theterm");    
    console.log('leinssss  : '+this.lien);
  }
  
}
