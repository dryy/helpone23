import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlocpassComponent } from './blocpass.component';

describe('BlocpassComponent', () => {
  let component: BlocpassComponent;
  let fixture: ComponentFixture<BlocpassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlocpassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlocpassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
