import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms'; 

import { BlocpassRoutingModule } from './blocpass-routing.module';
import { BlocpassComponent } from './blocpass.component';

import { AlertModule } from '../../_directives/alert.module';

@NgModule({
  imports: [
    CommonModule,
    BlocpassRoutingModule,AlertModule,
    FormsModule,ReactiveFormsModule
  ],
  declarations: [BlocpassComponent]
})
export class BlocpassModule { }
