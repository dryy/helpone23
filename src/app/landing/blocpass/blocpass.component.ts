import { Component, OnInit,OnDestroy  } from '@angular/core';
// Must import to use Forms functionality  
import { FormBuilder, FormGroup, Validators ,FormsModule,NgForm } from '@angular/forms'; 
import { first,takeUntil } from 'rxjs/operators'; 
import {componentDestroyed} from "@w11k/ngx-componentdestroyed";
import { Router,ActivatedRoute } from '@angular/router';
import { AlertService, ForgotpasswordService,UserService} from '../../_services';
import { PasswordValidation } from '../../shared';

@Component({
  selector: 'app-blocpass',
  templateUrl: './blocpass.component.html',
  styleUrls: ['./blocpass.component.scss']
})
export class BlocpassComponent implements OnInit  {

    submitted = false;
    emailVal:any; 
  BlocPasswordForm: FormGroup;
  userId: string;

  constructor( private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private forgotPwdService: ForgotpasswordService,
        private alertService: AlertService,
        private userService: UserService) { }

  ngOnInit() {

  			// let useridValue = localStorage.getItem('UserID');

  			// if (useridValue) {  
		    //    this.userService.getById(useridValue).pipe(first(),takeUntil(componentDestroyed(this))).subscribe( (dataget : any)  => { 
		    //    		this.emailVal= dataget;
		    //       }); 		            
        //     }  

  			this.BlocPasswordForm = this.formBuilder.group({
	  			  //  ValUser: [useridValue],
	               password: ['', [Validators.required, Validators.minLength(4)]],
	               Confpassword: ['', Validators.required]
               }, 
               { 
               	   validator: PasswordValidation.MatchPassword // your validation method	
            });
  }

  // convenience getter for easy access to form fields
    get f() { return this.BlocPasswordForm.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.BlocPasswordForm.invalid) {
            return;
        }

        // this.forgotPwdService.setThirdStep();
        this.route.queryParams.subscribe(params => {
          this.userId = params['userId'];
          console.log('parammmmmmmm : '+this.userId);
        });

        this.forgotPwdService.changePwd(this.userId, this.f.password.value)
            .pipe(first())
            .subscribe(
                (dataGet : any) => { 
                   this.alertService.success(dataGet.message, true);
                   this.forgotPwdService.resetStepsFlow();
                   this.router.navigate(['/logon']);
                   localStorage.removeItem('UserID'); 
                    
                },
                error => {
                    this.alertService.error(error);
                });
        
    }


}