import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BlocpassComponent } from './blocpass.component';

const routes: Routes = [

	{
        path: '', component: BlocpassComponent
    }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BlocpassRoutingModule { }
