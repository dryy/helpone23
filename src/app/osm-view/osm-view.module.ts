import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OsmViewComponent } from './osm-view.component';



@NgModule({
  declarations: [OsmViewComponent],
  imports: [
    CommonModule
  ],
  exports: [ OsmViewComponent ]
})
export class OsmViewModule { }
