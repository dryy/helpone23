import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileDataComponent } from './profile-data.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';



@NgModule({
  declarations: [ProfileDataComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule
  ],
  exports: [ProfileDataComponent]
})
export class ProfileDataModule { }
