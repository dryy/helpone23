import { FormControl, FormGroup } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-profile-data',
  templateUrl: './profile-data.component.html',
  styleUrls: ['./profile-data.component.scss']
})
export class ProfileDataComponent implements OnInit {

  @Input() name: string;
  @Input() label: string;
  @Input() myGroup: FormGroup;
  // input: FormControl = new FormControl('');
  constructor() { }

  ngOnInit() {
  }

}
