import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

// function myFunction(modal: any) {
//   $("#exampleModalScrollable").modal('show');
// }
@Component({
    selector: 'app-grid',
    templateUrl: './grid.component.html',
    styleUrls: ['./grid.component.scss']
})

export class GridComponent implements OnInit {
    @Input() rowData: any;
    @Input() columnDefs: any;
    @Output() onClick = new EventEmitter();

    constructor() { }

    ngOnInit() { }

    onCellClicked(event: any){
        console.log("event : ", event.data._id);
        if (event.column.colId =="FirstName" ) // only first column clicked
        {
        // execute the action as you want here in on click of hyperlink
        }
    }

    onGridReady(event: any){
        console.log(event);
    }
}
