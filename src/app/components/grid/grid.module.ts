import { AboutModule } from './../about/about.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GridComponent } from './grid.component';
import { AgGridModule } from 'ag-grid-angular';

@NgModule({
  declarations: [GridComponent],
  imports: [
    CommonModule,
    AboutModule,
    AgGridModule.withComponents(null)
  ],
  exports: [GridComponent]
})
export class GridModule { }
