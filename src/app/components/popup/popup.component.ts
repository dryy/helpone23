import { HelpClassComponent } from './../../access/classes/help-class.component';
import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatDialogRef } from '@angular/material';
import { ServiceListingService, AlertService, BidService } from 'src/app/_services';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { UserInfo } from 'src/app/_models';
import * as moment from 'moment';
import { NotificationsService } from 'src/app/_services/notifications.service';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss']
})

export class PopupComponent extends HelpClassComponent implements OnInit {

  saveBidForm: FormGroup;
  submitted = false;
  ValUser: string;
  currentUser: UserInfo;
  textPriceBid:boolean=false;
  constructor(private serviceList: ServiceListingService,
                private notificationsService: NotificationsService,
                private formBuilder: FormBuilder,
                private alertService: AlertService,
                private SetBid: BidService,
                private dialogRef: MatDialogRef<PopupComponent>, 
                @Inject(MAT_DIALOG_DATA) public data: any) {
    super(serviceList);
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.ValUser=this.currentUser.user._id;
    console.log("vvvv  : " , this.data.service._id);
  }

  ngOnInit() {
    console.log("picture : ", this.data.picture);
    this.dialogRef.updateSize('90%','95%');
    
    this.saveBidForm = this.formBuilder.group({
      'serviceAskForHelp_id':[this.data.service._id, Validators.required],
      'user_id_celui_qui_offre_service':['', Validators.required],
      'user_id_celui_qui_bid':[this.ValUser, Validators.required],
      'price_bid':['', Validators.required],
      'date_livraison':['', Validators.required],
      'time_livraison':['', Validators.required],
      'description':['', Validators.required]
    }); 
  }

  get f() { return this.saveBidForm.controls; }

  onSubmit(minVal: any ,MaxVal: any): void {
    this.saveBidForm.patchValue({"user_id_celui_qui_offre_service": this.data.service.user_id });
    this.submitted = true;
    // stop if price bid is not between price min and price max of service
    if( (this.f.price_bid.value < minVal) || (this.f.price_bid.value > MaxVal) ) {
        this.textPriceBid = true;
        return;
    }
    else{
        this.textPriceBid = false;
    }

    // stop here if form is invalid
    if (this.saveBidForm.invalid) {
      console.log("eeee ee");
        return;
    }

    let dateVal1 = moment(this.f.date_livraison.value).format('YYYY-MM-DD');
    let timeVal1 = moment(this.f.time_livraison.value).format('hh:mm A');
    let serviceAskForHelp_id = this.f.serviceAskForHelp_id.value;
    let user_id_celui_qui_bid = this.f.user_id_celui_qui_bid.value;
    let user_id_celui_qui_offre_service = this.f.user_id_celui_qui_offre_service.value;
    let price_bid = this.f.price_bid.value;
    let description = this.f.description.value;
    let dataDT : any = { date_livraison:dateVal1,
                time_livraison:timeVal1,
                serviceAskForHelp_id:serviceAskForHelp_id,
                user_id_celui_qui_bid:user_id_celui_qui_bid,
                user_id_celui_qui_offre_service:user_id_celui_qui_offre_service,
                price_bid:price_bid,
                description:description };
    this.saveNotif(serviceAskForHelp_id);

    this.SetBid.saveBid(dataDT).subscribe(

            //on success 
            (dataGet : any) => {
                this.saveBidForm.reset();
                this.submitted = false;
                this.alertService.success(dataGet.message, true);
                this.closeModal();
                alert(dataGet.message);                
            },
            //on error
            error => {
                this.alertService.error(error);
            },	      
        //on complete
        () => {            
            // this.getAllBiddersInfo();
            // this.getProfileInfoData();
            // this.getServiceBidInfo();
        });
        // alert('SUCCESS!! :-)\n\n' + JSON.stringify(dataDT, null, 4));
    }

    saveNotif(serviceAskForHelp_id: any){
        let dataNotif : any = { serviceAskForHelp_id:serviceAskForHelp_id,
            content: this.currentUser.user.username +' has bidded on your asked help. click to check' };
            console.log(dataNotif);
          this.notificationsService.save(dataNotif).subscribe( 
            //on success 
            (notification : any) => {
                let notif = notification.data; 
                console.log(notif); 
                console.log(notif.message);
                this.alertService.success(notif.message, true);              
            },
            //on error
            error => {
                this.alertService.error(error);
            },
        
          //on complete
          () => {
          });
      }

    closeModal(): void {
        this.dialogRef.close();
    }

    cancelBid(){
        this.saveBidForm.reset();
    }

}
