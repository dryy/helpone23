import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoryServicesComponent } from './category-services.component';



@NgModule({
  declarations: [CategoryServicesComponent],
  imports: [
    CommonModule
  ],
  exports: [ CategoryServicesComponent ]
})
export class CategoryServicesModule { }
