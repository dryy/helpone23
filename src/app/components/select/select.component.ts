import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class SelectComponent implements OnInit {

  @Input() categories: any;
  @Input() subCategories: any;
  @Input() submitted: any;
  @Input() observer: any;
  @Output() onChange = new EventEmitter();
  constructor() { }

  ngOnInit() {
    console.log("eee : ", this.categories);
    console.log("eee : ", this.subCategories);
  }

}
