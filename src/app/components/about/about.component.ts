import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  @Input() name: string;
  @Input() title: string;
  @Input() content: string;
  constructor() { }

  ngOnInit() {
  }

}
