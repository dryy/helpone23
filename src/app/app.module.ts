import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HashLocationStrategy, LocationStrategy  } from '@angular/common';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import { MatDialogModule } from '@angular/material';

import { AgmCoreModule } from '@agm/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { NgbActiveModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { NgKnifeModule } from 'ng-knife';

import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import {DataTableModule} from 'angular-6-datatable';

import {NgxPaginationModule} from 'ngx-pagination';

import { NgxPayPalModule } from 'ngx-paypal';

import {ProgressBarModule} from "angular-progress-bar";

import { OwlDateTimeModule, OwlNativeDateTimeModule, OWL_DATE_TIME_FORMATS } from 'ng-pick-datetime';

import {CarouselModule } from 'ngx-owl-carousel-o';

import { StarRatingModule } from 'angular-star-rating';

import { AngularOpenlayersModule } from 'ngx-openlayers';

import { JwtInterceptor, ErrorInterceptor } from './_helpers';
import { AlertService, AuthenticationService, UserService, ForgotpasswordService, ParametersService,
ServiceCatService, RoleService, RegisterDataService, WorkflowService, AddressService, LanguageService, ForgotpwdWorkflowService, AskhelpService, AskHelpWorkflowService, ServicetransactionService, WalletService, MethodPaymentService, ProfileService, ServiceListingService, PagerService, BidService, HelpnoteService, ScheduleDateService } from './_services';
import { AppRoutingModule} from './app-routing.module';
import { AppComponent } from './app.component';
import { CustomMaterialModule} from './material.module';
import { AuthGuard } from './shared';

import { VariablesGlobales } from './_models/variablesGlobales';
import { LiveChatComponent } from './live-chat/live-chat.component';
import { FileNameDialogComponent } from './access/file-name-dialog/file-name-dialog.component';
import { AgGridModule } from 'ag-grid-angular';
import { PopupComponent } from './components/popup/popup.component';
import { ModalComponent } from './components/modal/modal.component';
import { HeaderComponent } from './landing/header/header.component';
// import { LivechatWidgetModule } from '@livechat/angular-widget'


// AoT requires an exported function for factories
export const createTranslateLoader = (http: HttpClient) => {
    /* for development
    return new TranslateHttpLoader(
        http,
        '/start-angular/SB-Admin-BS4-Angular-6/master/dist/assets/i18n/',
        '.json'
    ); */
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
};

    // See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
export const MY_MOMENT_FORMATS = {
    parseInput: 'l LT',
    fullPickerInput: 'L LT',
    datePickerInput: 'L',
    timePickerInput: 'LT',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
};


@NgModule({
    imports: [
        CommonModule,
        NgbModule,
        AngularOpenlayersModule,
        BrowserModule,
        NgKnifeModule,
        BrowserAnimationsModule,
        ProgressBarModule, 
        MatProgressBarModule,
        DataTableModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule, NgxPayPalModule,
        OwlDateTimeModule, OwlNativeDateTimeModule,
        NgMultiSelectDropDownModule.forRoot(),
        HttpClientModule,
        AgGridModule.withComponents(null),
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyCn0XiiBhH8XTJIGmtAL7wZsIwZRb97E10'
        }),
        // LivechatWidgetModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        }),
        AppRoutingModule,
        CarouselModule ,
        CustomMaterialModule,
        MatDialogModule,
        StarRatingModule.forRoot()
    ],
    declarations: [AppComponent, FileNameDialogComponent, LiveChatComponent, PopupComponent, HeaderComponent],

    providers: [
        AuthGuard,
        AlertService,
        AuthenticationService,
        UserService,
        AskhelpService, AskHelpWorkflowService,
        ParametersService,
        RegisterDataService, VariablesGlobales ,
        ForgotpasswordService,
        NgbActiveModal,
        WorkflowService, ForgotpwdWorkflowService,
        ServiceCatService, RoleService, LanguageService, AddressService,
        ServicetransactionService, WalletService, MethodPaymentService, ProfileService,
        ServiceListingService, PagerService, BidService, HelpnoteService, ScheduleDateService,
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
        // {provide: OWL_DATE_TIME_FORMATS, useValue: MY_MOMENT_FORMATS},
        {provide : LocationStrategy , useClass: HashLocationStrategy},
        ],

    bootstrap: [AppComponent],
    entryComponents: [FileNameDialogComponent, PopupComponent]
})
export class AppModule {}
