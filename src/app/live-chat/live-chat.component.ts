import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { first } from 'rxjs/operators';

import { HelpnoteService } from '../_services';
type classeVoiture = Array<{message: string, nom: string}>;
@Component({
  selector: 'app-live-chat',
  templateUrl: './live-chat.component.html',
  styleUrls: ['./live-chat.component.scss']
})
export class LiveChatComponent implements OnInit {

  displayForm: boolean = false;
  regForm: FormGroup;
  monDiv: boolean;
  fillForm: boolean = false;
  submitted = false;
  offreur: string = "If you want to offer a service, please just register on the platform, then go to the dashboard and click th button ''Offer help''. Then follow the instructions by specifying your competences until you submit your answers. At that moment, you'll be able to see helps that have been asked and bid on them if they match your competences.";
  demandeur: string = "If you want to ask for a service, please just register on the platform, then go to the dashboard and click th button ''ask help''. Then you will be able to see the button ''Ask Help''. Click on it and follow the instructions by all the information about your service until you submit your answers. At that moment, people offering services will be able to see your request and bid on it if it matches their competences.";
  reponses: classeVoiture = [];
  constructor(private formLog: FormBuilder, private helpnoteService: HelpnoteService,) { }

  ngOnInit() {
    this.regForm = this.formLog.group({  
      'name':['', Validators.required],
      'message':['', Validators.required],
      'email':['', Validators.compose([Validators.required,Validators.email])]  
    });  
  }

  get f() { return this.regForm.controls; }

  openForm(){
    this.displayForm = !this.displayForm;
  }

  messageRobot(message: string, nom: string){
    this.reponses.push({message: message, nom: nom});
  }

  ajouter(message: string, nom: string, mess: string, name: string){
    this.reponses.push({message: message, nom: nom});
    setTimeout(() => {
      this.reponses.push({message: mess, nom: name});
    }, 3000);    
    // this.messageRobot(mess, name)
    this.fillForm = false;
    console.log('ùmes données : ', this.reponses[0]);
    console.log('ùmes données : ', this.reponses);
    console.log(this.reponses.length);
  }

  sendMessage(){
    this.fillForm = true;
  }

    getColor(back) { 
      if(back == 'robot'){
        console.log(back);
        return 'blue';
      } else{
        console.log(back);
        return 'rgb(236, 228, 228)';
      }
    }

  onSubmit(){
    this.submitted = true;
    if (this.regForm.invalid) {
      return;
    }
    this.helpnoteService.saveMessage(this.f.name.value, this.f.email.value, this.f.message.value)
        .subscribe(
            (data : any) => {
                console.log(data); 
                console.log(data.data);
                console.log(data.data.user_name);
                if(data.status == "success"){
                  this.fillForm = false;
                  this.messageRobot(data.message, "robot");
                  this.regForm.reset();
                }
            });
    }

}
