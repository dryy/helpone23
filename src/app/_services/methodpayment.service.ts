import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { MethodPayment } from '../_models';

@Injectable(
    {
        providedIn: 'root'
      }
)
export class MethodPaymentService {

    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<MethodPayment[]>(`${environment.apiUrl}/methodpayment/get`);
    }

    getById(val: string) {
        return this.http.get(`${environment.apiUrl}/methodpayment/get/amethodpayment/` + val);
    }

    save(formValue: MethodPayment) {
        return this.http.post(`${environment.apiUrl}/methodpayment/save`, formValue);
    }

    update(formValue: MethodPayment) {
        return this.http.patch(`${environment.apiUrl}/methodpayment/update/` + formValue._id, formValue);
    }

    delete(id: number) {
        return this.http.delete(`${environment.apiUrl}/methodpayment/delete/` + id);
    }
}