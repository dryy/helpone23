import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Bid } from '../_models';

@Injectable(
    {
        providedIn: 'root'
      }
)
export class BidService {

    constructor(private http: HttpClient) { }

    saveBid(formValue) {
        return this.http.post(`${environment.apiUrl}/bid/save`, formValue);
    }

    getAllBidders(serviceaskforhelp_id:string) {
        return this.http.get(`${environment.apiUrl}/bid/get/bidders/` +serviceaskforhelp_id);
    }

}