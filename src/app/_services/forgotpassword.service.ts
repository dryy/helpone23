import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { UserInfo } from '../_models';

import { ForgotpwdWorkflowService } from './forgotpwd-workflow.service';
import { STEPS_FOR_FORGOTPWD }   from '../_models/workflow';

@Injectable(
    {
        providedIn: 'root'
      }
)
export class ForgotpasswordService {

    private isFirstStepFormValid: boolean = false;
    private isSecStepFormValid: boolean = false;
    private isThirdStepFormValid: boolean = false;

    constructor(private http: HttpClient,private workflowService: ForgotpwdWorkflowService) { }

    setFirstStep() {
        
        this.isFirstStepFormValid = true;
        // Validate Personal Step in Workflow
        this.workflowService.validateStep(STEPS_FOR_FORGOTPWD.firstStep);
    }

    setSecStep() {
        
        this.isSecStepFormValid = true;
        // Validate Personal Step in Workflow
        this.workflowService.validateStep(STEPS_FOR_FORGOTPWD.secondStep);
    }

    setThirdStep() {
        
        this.isThirdStepFormValid = true;
        // Validate Personal Step in Workflow
        this.workflowService.validateStep(STEPS_FOR_FORGOTPWD.thirdStep);
    }

    resetStepsFlow() {

        // Reset the workflow
        this.workflowService.resetSteps();
                
        this.isFirstStepFormValid = this.isSecStepFormValid = this.isThirdStepFormValid = false;
    }

    sendPwdEmail(valEmail: string) {
        return this.http.post(`${environment.apiUrl}/passwordresets/sendcode`, { email: valEmail });
    }

    checkCodeEmail(valCode: string) {
        return this.http.post(`${environment.apiUrl}/passwordresets/checkcode`, { code: valCode });
    }

    changePwd(userid: string,valPwd: string) {
        return this.http.post(`${environment.apiUrl}/passwordresets/new_password/` + userid, { new_password: valPwd });
    } 


}