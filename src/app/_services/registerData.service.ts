import { Injectable }  from '@angular/core';

import { RegData, Step1, Step2, FinalStep }  from '../_models/registerData';

import { WorkflowService }                   from './workflow.service';
import { STEPS }                             from '../_models/workflow';

@Injectable(
    {
        providedIn: 'root'
      }
)
export class RegisterDataService {

    private regData: RegData = new RegData();

    private isFormValid: boolean = false;
    private isFirstStepFormValid: boolean = false;
    private isSecStepFormValid: boolean = false;
    private isFourtStepFormValid: boolean = false;
    private isFinalStepFormValid: boolean = false;

    constructor(private workflowService: WorkflowService) { 
    }

    getFirstStep(): Step1 {
        
        // Return the Step1 data
        var step1: Step1 = {
            pwd: this.regData.password,
            email: this.regData.email,
            pwd2: ""
        };
        return step1;
    }

    setFirstStep(data: Step1) {
        // Update the  data only when the First Step Form had been validated successfully
        this.isFirstStepFormValid = true;
        this.regData.password = data.pwd;
        this.regData.email = data.email;

        // Validate Personal Step in Workflow
        this.workflowService.validateStep(STEPS.firstStep); 
    }

    
    getSecStep(): Step2 {
        
        // Return the Step2 data
        var step2: Step2 = {
            username: this.regData.username
        };
        return step2;
    }

    setSecStep(data: Step2) {
        // Update the data only when the Second Step Form had been validated successfully
        this.isSecStepFormValid = true;
        this.regData.username = data.username;

        // Validate Personal Step in Workflow
        this.workflowService.validateStep(STEPS.secondStep);
    }

    setFinalStep(data: FinalStep) {
        // Update the data only when the Second Step Form had been validated successfully
        this.isFinalStepFormValid = true;
        this.regData.firstname = data.firstname;
        this.regData.surname = data.surname;
        this.regData.fonctionName = data.fonctionName;
        this.regData.experience = data.experience;

        // Validate Personal Step in Workflow
        this.workflowService.validateStep(STEPS.finalStep);
    }

    SetLang(valueArray: any) {

        // Update the data only when the Second Step Form had been validated successfully
        this.isFinalStepFormValid = true;
        this.regData.langues = valueArray;

        // Validate Personal Step in Workflow
        this.workflowService.validateStep(STEPS.finalStep);
    }

     SetPic(valueArray: any) {

        // Update the data only when the Second Step Form had been validated successfully
        this.isFinalStepFormValid = true;
        this.regData.picture = valueArray;

        // Validate Personal Step in Workflow
        this.workflowService.validateStep(STEPS.finalStep);
    }

    SetPic1(valueArray: any) {

        // Update the data only when the Second Step Form had been validated successfully
        this.isFinalStepFormValid = true;
        this.regData.picture_cni = valueArray;

        // Validate Personal Step in Workflow
        this.workflowService.validateStep(STEPS.finalStep);
    }

    SetPic2(valueArray: any) {

        // Update the data only when the Second Step Form had been validated successfully
        this.isFinalStepFormValid = true;
        this.regData.picture_with_cni = valueArray;

        // Validate Personal Step in Workflow
        this.workflowService.validateStep(STEPS.finalStep);
    }

    SetSousCateg(valueArray: any) {

        // Update the data only when the Second Step Form had been validated successfully
        this.isFourtStepFormValid = true;
        this.regData.souscategories = valueArray;
    }
    
    ResetSousCateg() {
        this.regData.souscategories = [];
    }


     getRegData(): RegData {
        // Return the entire Form Data
        return this.regData;
    }

    isFormRegValid() {
        // Return true if all forms had been validated successfully; otherwise, return false
        return this.isFirstStepFormValid ;
        // &&
        //        this.isSecStepFormValid &&
        //        this.isFourtStepFormValid &&
        //        this.isFinalStepFormValid;
    }

    resetRegFormData(): RegData {

        // Reset the workflow
        this.workflowService.resetSteps();
                
        // Return the form data after all this.* members had been reset
        this.regData.clear();
        this.isFormValid = this.isFirstStepFormValid = this.isSecStepFormValid = this.isFourtStepFormValid = this.isFinalStepFormValid = false;
        return this.regData;
    }

    saveData(data: any) {
        // Update the  data only when the First Step Form had been validated successfully
        this.isFormValid = true;
        this.regData.firstname = data.firstname;
        this.regData.surname = data.surname;
        this.regData.username = data.username;
        this.regData.password = data.pwd;
        this.regData.email = data.email;
        // Validate Personal Step in Workflow
        this.workflowService.validateStep(STEPS.firstStep); 
    }

}