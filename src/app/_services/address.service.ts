import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Address } from '../_models';

@Injectable(
    {
        providedIn: 'root'
      }
)
export class AddressService {

    constructor(private http: HttpClient) { }

    getByUserID(user_id: string) {
        return this.http.get(`${environment.apiUrl}/addressuser/view/` + user_id);
    }

    save(formValue: Address) {
        return this.http.post(`${environment.apiUrl}/addressuser/save`, formValue);
    }

    update(formValue: Address) {
        return this.http.patch(`${environment.apiUrl}/addressuser/update/` + formValue._id, formValue);
    }

    delete(user_id: string) {
        return this.http.delete(`${environment.apiUrl}/addressuser/delete/` + user_id);
    }
}