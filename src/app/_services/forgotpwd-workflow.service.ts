import { Injectable }         from '@angular/core';
 
import { STEPS_FOR_FORGOTPWD }              from '../_models/workflow';
 
@Injectable(
    {
        providedIn: 'root'
      }
)
export class ForgotpwdWorkflowService {
    private Forgotpwd_workflow = [
        { step: STEPS_FOR_FORGOTPWD.firstStep, valid: false },
        { step: STEPS_FOR_FORGOTPWD.secondStep, valid: false },
        { step: STEPS_FOR_FORGOTPWD.thirdStep, valid: false }
    ];


    validateStep(step: string) {
        // If the state is found, set the valid field to true 
        var found = false;
        for (var i = 0; i < this.Forgotpwd_workflow.length && !found; i++) {
            if (this.Forgotpwd_workflow[i].step === step) {
                found = this.Forgotpwd_workflow[i].valid = true;
            }
        }
    }
 
    resetSteps() {
        // Reset all the steps in the Workflow to be invalid
        this.Forgotpwd_workflow.forEach(element => {
            element.valid = false;
        });
    }
 
    getFirstInvalidStep(step: string) : string {
        // If all the previous steps are validated, return blank
        // Otherwise, return the first invalid step
        var found = false;
        var valid = true;
        var redirectToStep = '';
        for (var i = 0; i < this.Forgotpwd_workflow.length && !found && valid; i++) {
            let item = this.Forgotpwd_workflow[i];
            if (item.step === step) {
                found = true;
                redirectToStep = '';
            }
            else {
                valid = item.valid;
                redirectToStep = item.step
            }
        }
        return redirectToStep;
    }
}