﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { UserInfo } from '../_models';
import { RegData }  from '../_models/registerData';
import { map } from 'rxjs/operators';

@Injectable(
    {
        providedIn: 'root'
      }
)
export class UserService {

    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<UserInfo[]>(`${environment.apiUrl}/users/get`);
    }

    getById(val: string) {
        return this.http.get(`${environment.apiUrl}/users/getprofile/` + val);
    }

    register(user: UserInfo) {
        return this.http.post(`${environment.apiUrl}/users/signup`, user);
    }

    registerForm(user: RegData) {
        return this.http.post(`${environment.apiUrl}/users/signup`, user);
    }

    update(user: UserInfo) {
        return this.http.patch(`${environment.apiUrl}/users/editprofile/` + user._id, user);
    }

    delete(id: number) {
        return this.http.delete(`${environment.apiUrl}/users/delete/` + id);
    }

    suspension(id: number) {
        return this.http.post(`${environment.apiUrl}/users/suspension/`, {id: id});
    }

    unSuspension(id: number) {
        return this.http.post(`${environment.apiUrl}/users/unSuspension/`, {id: id});
    }

    switchForm(user_id: string, experience: string, souscategories: any[]){
        // return this.http.post(`${environment.apiUrl}/users/switchUser/`, {user_id: user_id, experience: experience, souscategories: souscategories});
        return this.http.post<any>(`${environment.apiUrl}/users/switchUser`, { user_id: user_id, experience: experience, souscategories: souscategories })
            .pipe(map(user => {
                // login successful if there's a jwt token in the response
               if  (user) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    JSON.stringify(user);
                    // localStorage.setItem('isLoggedin', 'true');
                }

                return user;
            }));
    }
}