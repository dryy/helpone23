import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';

@Injectable(
    {
        providedIn: 'root'
      }
)
export class ScheduleDateService {

    constructor(private http: HttpClient) { }

    saveScheduleDate(formValue) {
        return this.http.post(`${environment.apiUrl}/scheduledates/save`, formValue);
    }

    getAllScheduleDates(serviceaskforhelp_id:string) {
        return this.http.get(`${environment.apiUrl}/scheduledates/get/allscheduledates/` +serviceaskforhelp_id);
    }

    ApproveOrRevoke(scheduledate_id:string,formValue) {
        return this.http.patch(`${environment.apiUrl}/scheduledates/update/schedule/status/` +scheduledate_id,formValue);
    }

}