﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { RegData }  from '../_models/registerData';
import { map } from 'rxjs/operators';
import { Notifications } from '../_models/notifications';

@Injectable(
    {
        providedIn: 'root'
      }
)
export class NotificationsService {

    constructor(private http: HttpClient) { }

    getAll(user_id: string) {
        return this.http.get<Notifications[]>(`${environment.apiUrl}/notifications/get/`+ user_id);
    }

    getById(service_id: string) {
        return this.http.get(`${environment.apiUrl}/notifications/getForService/` + service_id);
    }

    save(notif: any) {
        return this.http.post(`${environment.apiUrl}/notifications/save`, notif);
    }

    // registerForm(user: RegData) {
    //     return this.http.post(`${environment.apiUrl}/notifications/signup`, user);
    // }
    
    deleteNotif(id: string) {
        return this.http.delete(`${environment.apiUrl}/notifications/supprimer/` + id);
    }

    readNotif(notif_id: string) {
        return this.http.get(`${environment.apiUrl}/notifications/lire/` + notif_id);
    }

}