import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject, Subject } from 'rxjs';


import { environment } from '../../environments/environment';
import { Wallet } from '../_models';
import { CurrentWallet }  from '../_models/wallet';

@Injectable(
    {
        providedIn: 'root'
      }
)
export class WalletService {
	
	private empDetailSubject = new BehaviorSubject(null);

    private currentBalData: CurrentWallet = new CurrentWallet();
    
    constructor(private http: HttpClient) { }

    getMyWallet(user_id: string) {
        return this.http.get(`${environment.apiUrl}/walletuser/view/` + user_id);
    } 

    setMyWalletUpdated(dataValue) {
        this.empDetailSubject.next(dataValue);
    }

    getActualMyWalletUpdated() {
        return this.empDetailSubject;
    }

    getCurrentBalData(): CurrentWallet {
        // Return the entire User Current Balance
        return this.currentBalData;
    }

     setBalance(value: string) {
        this.currentBalData.balance = value ;
     }

    ChargeWallet(formValue) {
        return this.http.post(`${environment.apiUrl}/walletuser/save`, formValue);
    }
    
    HistoryTransact(user_id: string) {
        return this.http.get(`${environment.apiUrl}/transactionwalletuser/view/` + user_id);
    }

}