import { Injectable } from '@angular/core';
import {
    CanActivate, Router,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    CanLoad, Route
} from '@angular/router';
 
import { WorkflowService } from './workflow.service';
import { ForgotpwdWorkflowService } from './forgotpwd-workflow.service';
import { AskHelpWorkflowService } from './askhelp-workflow.service';
 
@Injectable(
    {
        providedIn: 'root'
      }
)
export class WorkflowGuard implements CanActivate {
    constructor(private router: Router, private workflowService: WorkflowService, private fpwd_workflowService: ForgotpwdWorkflowService, private askhelp_workflowService: AskHelpWorkflowService) { }
 
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        let path: string = route.routeConfig.path;
        
        if ( path.startsWith("register") ) {
            //console.log('le lien: '+path);
            return this.verifyWorkFlow(path) ;
            
        }
        
        if ( path.startsWith("askHelp") ) {
            //console.log('le lien 1: '+path);
            return this.verifyAskhelp_WorkFlow(path) ;
            
        }

             //console.log('le lien 2: '+path);
             return this.verifyFpwd_WorkFlow(path);

       
        
        
    }
 
    verifyWorkFlow(path) : boolean {
        console.log("Entered '" + path + "' path.");
 
        // If any of the previous steps is invalid, go back to the first invalid step
        let firstPath = this.workflowService.getFirstInvalidStep(path);
        if (firstPath.length > 0) {
            console.log("Redirected to '" + firstPath + "' path which it is the first invalid step.");
            let url = `/${firstPath}`;
            this.router.navigate([url]);
            return false;
        };
 
        return true;
    }

    verifyFpwd_WorkFlow(path) : boolean {
        console.log("Entered '" + path + "' path.");
 
        // If any of the previous steps is invalid, go back to the first invalid step
        let firstPath = this.fpwd_workflowService.getFirstInvalidStep(path);
        if (firstPath.length > 0) {
            console.log("Redirected to '" + firstPath + "' path which it is the first invalid step.");
            let url = `/${firstPath}`;
            this.router.navigate([url]);
            return false;
        };
 
        return true;
    } 

    verifyAskhelp_WorkFlow(path) : boolean {
        console.log("Entered '" + path + "' path.");
 
        // If any of the previous steps is invalid, go back to the first invalid step

        let Newpath = "access/"+path;
        
        let firstPath = this.askhelp_workflowService.getFirstInvalidStep(Newpath);
        if (firstPath.length > 0) {
            console.log("Redirected to '" + firstPath + "' path which it is the first invalid step.");
            let url = `/${firstPath}`;
            this.router.navigate([url]);
            return false;
        };
 
        return true;
    }
}