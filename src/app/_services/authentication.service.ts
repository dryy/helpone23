﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Router } from "@angular/router";

import { environment } from '../../environments/environment';
import { VariablesGlobales } from '../_models/variablesGlobales';
import { UserInfo } from '../_models';

@Injectable(
    {
        providedIn: 'root'
      }
)

export class AuthenticationService {
   
    // Fields 
    private http  : HttpClient; // pointer
    private router: Router;
    
    constructor(http  : HttpClient, router: Router) {
        this.http   = http;
        this.router = router;
        console.log("http : ", this.http);
    }
    login: (email_or_username: string, password: string) => any; 
    sendmail: (uemail: string, Confirmation_code: string) => any;
    activation: (uemail: string, Confirmation_code: string) => any;
    logout: () => void;
    CheckEmail: (email: string) => any;
    CheckUsername: (username: string) => any;
}

AuthenticationService.prototype.login = function(email_or_username: string, password: string) {   
     return this.http.post(`${environment.apiUrl}/users/signin`, { email_or_username: email_or_username, password: password });    
}

AuthenticationService.prototype.sendmail = function(uemail: string, Confirmation_code: string){
    return this.http.post(`${environment.apiUrl}/users/sendmail`, { uemail: String, Confirmation_code: String });
}
AuthenticationService.prototype.activation = function(uemail: string, Confirmation_code: string){
    return this.http.post(`${environment.apiUrl}/users/activate`, { uemail: uemail, Confirmation_code: Confirmation_code });    
}
AuthenticationService.prototype.CheckUsername = function(username: string) {
    return this.http.post(`${environment.apiUrl}/users/checkUsername`, {username:username});
}

AuthenticationService.prototype.CheckEmail = function(email: string) {
    return this.http.post(`${environment.apiUrl}/users/checkEmail`, {email:email});
}
AuthenticationService.prototype.logout = function() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    localStorage.removeItem('isLoggedin');
    this.router.navigate(["/logon"]);
}
//authentication : The process or action of proving or showing something to be true, genuine, or valid

