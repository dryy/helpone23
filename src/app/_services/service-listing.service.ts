import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { ServiceData } from '../_models';
import { ServiceInfoData } from '../_models/services';
import { ServiceBidInfos } from '../_models/service-bid-infos';

@Injectable(
    {
        providedIn: 'root'
      }
)
export class ServiceListingService {

    private servInfoData: ServiceInfoData = new ServiceInfoData();
    donnee : string = "serviceaskforhelp/recent_helps_requests";

    constructor(private http: HttpClient) { }

    //permet de get tous les services différents de celui qui est connecté (user) avec pour statut 'in_progress' (My Recent Helps Requests)

    getRecentHelpsRequest(user_id: string, url:any) {
        if(user_id != null){
            return this.http.get<ServiceData[]>(`${environment.apiUrl}/`+url+'/'  + user_id);
        } else{
            return this.http.get<ServiceData[]>(`${environment.apiUrl}/`+url+'/');
        }
        
    } 

    //permet de get tous les services avec pour status = in_progress (ceci c'est pour les users non connectés)

    getAllRecentHelpsRequest() {
        return this.http.get<ServiceData[]>(`${environment.apiUrl}/serviceaskforhelp/recent_helps_requests`);
    } 

    //permet de get tous les services avec pour status = completed

    getAllCompletedHelpsRequest() {
        return this.http.get<ServiceData[]>(`${environment.apiUrl}/serviceaskforhelp/completed_helps_requests`);
    } 

    //permet de GET tous les services du User qui sont en status = in_progress (My Open Helps)

    getMyOpenHelps(user_id: string) {
        return this.http.get<ServiceData[]>(`${environment.apiUrl}/serviceaskforhelp/my_open_helps/` + user_id);
    } 

    //permet de GET tous les services du User dependant du statut du service

    getMyHelps(user_id: string) {
        return this.http.get<ServiceData[]>(`${environment.apiUrl}/serviceaskforhelp/view/` + user_id);
    } 
   
   //permet de GET tous les services du User

    getMyAllHelps(user_id: string) { 
        return this.http.get<ServiceData[]>(`${environment.apiUrl}/serviceaskforhelp/other_recent_helps_requests/` + user_id);
    } 

    // permet de renvoyer les services biddés par un user (My Bidding Helps)
    
    getMyBiddingHelps(user_id: string) {
        return this.http.get<ServiceData[]>(`${environment.apiUrl}/serviceaskforhelp/my_bidding_helps/` + user_id);
    } 

    //permet de get les infos principales d'un service à savoir user_id, categoryService_id, souscategoryService_id, description,min_price, max_price et status

    getServiceInfos(serviceaskforhelp_id: string) {
        return this.http.get<ServiceBidInfos[]>(`${environment.apiUrl}/serviceaskforhelp/view/service/` + serviceaskforhelp_id);
    } 

     //permet de get toutes les images d'un service (serviceAskForHelp_id)

    getServicePictures(serviceaskforhelp_id: string) {
        return this.http.get<any>(`${environment.apiUrl}/image/service/view/` + serviceaskforhelp_id);
    } 

     //permet de GET toutes les dates et heures préférées d'un service (serviceAskForHelp_id)

    getServiceTime(serviceaskforhelp_id: string) {
        return this.http.get<any>(`${environment.apiUrl}/preferred_date_and_time/service/view/` + serviceaskforhelp_id);
    }  

    //permet de GET toutes adresses choisies par le user pour un service (serviceAskForHelp_id)

    getServiceAddress(serviceaskforhelp_id: string) {
        return this.http.get<any>(`${environment.apiUrl}/choix_deliver_and_bill/service/view/` + serviceaskforhelp_id);
    } 

    getServiceData(): ServiceInfoData { 
        // Return the entire Service Data
        return this.servInfoData;
    }

    setServiceInfoData(data: ServiceInfoData) {        
        this.servInfoData.user_id = data.user_id;
        this.servInfoData.categoryService_id = data.categoryService_id;
        this.servInfoData.sousCategory_id = data.sousCategory_id;
        this.servInfoData.description = data.description;
        this.servInfoData.min_price = data.min_price;
        this.servInfoData.max_price = data.max_price;
        this.servInfoData.choixdeliverandbillhelpforaservice = [];
        this.servInfoData.image_service = [];
        this.servInfoData.preferred_date_and_time_service = [];
    }

     SetServiceInfoPicture(valueArray: any) {
        this.servInfoData.image_service = valueArray;
     }

     SetServiceInfoTime(valueArray: any) {
        this.servInfoData.preferred_date_and_time_service = valueArray;
     }

     SetServiceAddressTime(valueArray: any) {
        this.servInfoData.choixdeliverandbillhelpforaservice = valueArray;
     }

     setF1Value(value: string) {
        this.servInfoData.description = value;
     }
     setF2Value(valueArray: any) {
        this.servInfoData.preferred_date_and_time_service = valueArray;
     }

     updateServiceInfo(formValue: ServiceInfoData, valueID:string) {
        return this.http.post(`${environment.apiUrl}/serviceaskforhelp/update/` + valueID, formValue);
    }

    //Premet de noter le travail effectué par un helper
    setServiceEtoile(valueID:string, notation: any) {
        return this.http.post(`${environment.apiUrl}/serviceaskforhelp/setEtoiles/` + valueID, notation);
    }

    //Premet au helper de noter le déroulement du travail effectué
    setOfferEtoile(valueID:string, notation: any) {
        return this.http.post(`${environment.apiUrl}/serviceaskforhelp/setOfferEtoiles/` + valueID, notation);
    }

    //permet d'enregistrer celui qui a été accepté pour faire le service du user 
     acceptServiceBid(formValue, valueID:string) {
        return this.http.post(`${environment.apiUrl}/serviceaskforhelp/accept/bidder/` + valueID, formValue);
    }

    // permet de renseigner qu'un service a bien été complété par l'offreur

    compliteSevice(service_id: string){
        return this.http.post(`${environment.apiUrl}/serviceaskforhelp/completeHelp`, {service_id: service_id});
    }

    // permet de payer le travailleur qui a fini le service  
     PayHelper(formValue) {
        return this.http.post(`${environment.apiUrl}/serviceaskforhelp/pay/helper/`, formValue);
    }

    // permet de confirmer que le service a été fait avec succès puis enregistre le nombre d'étoiles et le commentaire fait par l'offer de service  

     RateHelper(formValue,valueID:string) {
        return this.http.patch(`${environment.apiUrl}/serviceaskforhelp/confirm/completed/service/` + valueID, formValue);
    }

    //permet d'enregistrer un message du chat par rapport à un service donné
    // saveHelpNotes(notesId: string, senderId: string, senderUName: string, ReceiverId: string, username: string) {
    //     return this.http.post(`${environment.apiUrl}/HelpNotes/saveChat`, {receiver:receiver, subject:subject, message:message, senderId:senderId, username: username});
    // }
    
}