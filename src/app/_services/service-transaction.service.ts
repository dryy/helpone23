import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { ServiceTransaction } from '../_models';
import { AskData }  from '../_models/askHelp';

@Injectable(
    {
        providedIn: 'root'
      }
)
export class ServicetransactionService {

    constructor(private http: HttpClient) { }

    save(formValue: AskData) {
        return this.http.post(`${environment.apiUrl}/serviceaskforhelp/save`, formValue);
    }

}