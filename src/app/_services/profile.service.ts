import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { UserInfo,Address } from '../_models';
import { ProfileData }  from '../_models/profile';

@Injectable(
    {
        providedIn: 'root'
      }
)
export class ProfileService {

    private profileUserData: ProfileData = new ProfileData();

    niveau: number = 5;

    constructor(private http: HttpClient) { }

    getProfileInfo(user_id: string) { 
        return this.http.get<UserInfo[]>(`${environment.apiUrl}/users/getprofile/` + user_id);
    } 

    getProfileAddress(user_id: string) {
        return this.http.get<UserInfo[]>(`${environment.apiUrl}/addressuser/view/` + user_id);
    } 

    updateProfileAddress(formValue: Address) {
        return this.http.post(`${environment.apiUrl}/addressuser/update/` + formValue._id, formValue);
    }

    updateProfileInfo(formValue: ProfileData,user_id: string) {
        return this.http.post(`${environment.apiUrl}/users/editprofile/` +user_id, formValue);
    }

    getUserProfileData(): ProfileData {
        // Return the entire User Profile Data
        return this.profileUserData;
    }

    setUserProfileData(data: ProfileData, dataCat: any, dataLang: any) {  
        console.log(dataCat);
        this.niveau = 5;      
        this.profileUserData.username = data.username;
        this.profileUserData.firstname = data.firstname;
        this.profileUserData.surname = data.surname;
        this.profileUserData.slug = data.slug;
        this.profileUserData.pseudo = data.pseudo;
        this.profileUserData.email = data.email;
        this.profileUserData.personal_description = data.personal_description;
        this.profileUserData.password = data.password;
        this.profileUserData.experience = data.experience;
        this.profileUserData.gender = data.gender;
        this.profileUserData.phone = data.phone;
        this.profileUserData.CV = data.CV;
        this.profileUserData.criminal_record = data.criminal_record;
        this.profileUserData.longitude_location = data.longitude_location;
        this.profileUserData.latitude_location = data.latitude_location;
        this.profileUserData.location_name = data.location_name;
        this.profileUserData.country = data.country;
        this.profileUserData.picture = data.picture;
        this.profileUserData.picture_cni = data.picture_cni;
        this.profileUserData.picture_with_cni = data.picture_with_cni;
        this.profileUserData.fonctionName = data.fonctionName;
        this.profileUserData.souscategories = dataCat;
        this.profileUserData.langues = dataLang;
    }

    getNiveau(){
        if(this.profileUserData.slug!='')
        this.niveau++;
        if(this.profileUserData.pseudo!='')
        this.niveau++; 
        if(this.profileUserData.personal_description!='')
        this.niveau++;
        if(this.profileUserData.experience!='')
        this.niveau++;
        if(this.profileUserData.gender!='')
        this.niveau++;
        if(this.profileUserData.phone!='')
        this.niveau++;
        if(this.profileUserData.CV!='')
        this.niveau++;
        if(this.profileUserData.criminal_record!='')
        this.niveau++;
        if(this.profileUserData.longitude_location!='')
        this.niveau++;
        if(this.profileUserData.latitude_location!='')
        this.niveau++;
        if(this.profileUserData.location_name!='')
        this.niveau++;
        if(this.profileUserData.country!='')
        this.niveau++;
        if(this.profileUserData.picture!='')
        this.niveau++;
        if(this.profileUserData.fonctionName!='')
        this.niveau++;
        if(this.profileUserData.souscategories!=[])
        this.niveau++;
        if(this.profileUserData.langues!=[])
        this.niveau++;
        if(this.profileUserData.picture_cni!='')
        this.niveau++;
        if(this.profileUserData.picture_with_cni!='')
        this.niveau++;

        return this.niveau;
    }

    setValue(data: any){
        this.profileUserData.username = data.username;
        this.profileUserData.personal_description = data.description;
        this.profileUserData.phone = data.phone ;
        this.profileUserData.email = data.email ;
        this.profileUserData.firstname = data.firstname;
        this.profileUserData.surname = data.surname;
    }

    setF1Value(value: string) {
    this.profileUserData.username = value ;
    }

    setF2Value(value: string) {
    this.profileUserData.personal_description = value ;
    }

    setF3Value(value: string) {
    this.profileUserData.phone = value ;
    }

    setF4Value(value: string) {
    this.profileUserData.email = value ;
    }

    setImgValue(value: string) {
    this.profileUserData.picture = value ;
    }

    setCniImgValue(value: string) {
    this.profileUserData.picture_cni = value ;
    }

    setImgWithCniValue(value: string) {
        this.profileUserData.picture_with_cni = value ;
    }

    SetLang(valueArray: any) {
        this.profileUserData.langues = valueArray;
    }

    SetSousCateg(valueArray: any) {
        this.profileUserData.souscategories = valueArray;
    }

    SetFonctionName(value: string){
        this.profileUserData.fonctionName = value;
    }

    SetExperience(value: string){
        this.profileUserData.experience = value;
    }

    SetSousCategories(value: string[]){
        this.profileUserData.souscategories = value;
    }

   // get le nbre de My Open Helps, le nbre de My Bidding Helps, le nbre de My Completed Helps et le nbre de My Closed Helps d'un user

    getUserStats(user_id: string) {
        return this.http.get<UserInfo[]>(`${environment.apiUrl}/serviceaskforhelp/nbre_helps_dashboard/` + user_id);
    } 
}