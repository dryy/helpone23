import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AskHelpWorkflowService } from './askhelp-workflow.service';
import { STEPS_FOR_ASKHELP }   from '../_models/workflow';

import { AskData, ASKStep1, ASKStep2 }  from '../_models/askHelp';

@Injectable(
    {
        providedIn: 'root'
      }
)
export class AskhelpService {

    private askData: AskData = new AskData();

    private isFirstStepFormValid: boolean = false;
    private isSecStepFormValid: boolean = false;

    constructor(private http: HttpClient,private workflowService: AskHelpWorkflowService) { }

     getFirstStep(): ASKStep1 {
        
        // Return the Step1 data
        var step1: ASKStep1 = {
            user_id: this.askData.user_id,
            categoryService_id: this.askData.categoryService_id,
            sousCategory_id: this.askData.sousCategory_id,
            description: this.askData.description,
            image_service: this.askData.image_service,
            preferred_date_and_time_service: this.askData.preferred_date_and_time_service
        };
        return step1;
    }

    setFirstStep(data: ASKStep1) {
        
        this.isFirstStepFormValid = true;

        this.askData.user_id = '';
        this.askData.categoryService_id = data.categoryService_id;
        this.askData.sousCategory_id = data.sousCategory_id;
        this.askData.description = data.description;

        // Validate Personal Step in Workflow
        this.workflowService.validateStep(STEPS_FOR_ASKHELP.firstStep);
    }

    setHelpData(data: ASKStep1){
        this.askData.categoryService_id = data.categoryService_id;
        this.askData.sousCategory_id = data.sousCategory_id;
        this.askData.description = data.description;
    }

    getHelpData(){
        return this.askData;
    }

     SetTimeDate(valueArray: any) {
        this.askData.preferred_date_and_time_service = valueArray;
     }

     SetImageService(valueArray: any) {
        this.askData.image_service = valueArray;
     }



    setSecStep(data: ASKStep2) {
        
        this.isSecStepFormValid = true;

        this.askData.min_price = data.min_price;
        this.askData.max_price = data.max_price;

        // Validate Personal Step in Workflow
        this.workflowService.validateStep(STEPS_FOR_ASKHELP.secondStep);
    }

     SetAdressesInfo(valueArray: any) {
        this.askData.choixdeliverandbillhelpforaservice = valueArray;
     }

     SetUserID(IdUser) {
        this.askData.user_id = IdUser;
     }

    getSecStep(): ASKStep2 {
        
        // Return the Step1 data
        var step2: ASKStep2 = {
            min_price: this.askData.min_price,
            max_price: this.askData.max_price
        };
        return step2;
    }

    getAskData(): AskData {
        // Return the entire Form Data
        return this.askData;
    }

    isFormAskValid() {
        // Return true if all forms had been validated successfully; otherwise, return false
        return this.isFirstStepFormValid &&
               this.isSecStepFormValid ;
    }


    resetAskFormData(): AskData {

        // Reset the workflow
        this.workflowService.resetSteps();
                
        // Return the form data after all this.* members had been reset
        this.askData.clear();
         this.isFirstStepFormValid = this.isSecStepFormValid = false;
        return this.askData;
    }

}