import { Injectable }         from '@angular/core';
 
import { STEPS_FOR_ASKHELP }              from '../_models/workflow';
 
@Injectable(
    {
        providedIn: 'root'
      }
)
export class AskHelpWorkflowService {
    private AskHelp_workflow = [
        { step: STEPS_FOR_ASKHELP.firstStep, valid: false },
        { step: STEPS_FOR_ASKHELP.secondStep, valid: false }
    ];


    validateStep(step: string) {
        // If the state is found, set the valid field to true 
        var found = false;
        for (var i = 0; i < this.AskHelp_workflow.length && !found; i++) {
            if (this.AskHelp_workflow[i].step === step) {
                found = this.AskHelp_workflow[i].valid = true;
            }
        }
    }
 
    resetSteps() {
        // Reset all the steps in the Workflow to be invalid
        this.AskHelp_workflow.forEach(element => {
            element.valid = false;
        });
    }
 
    getFirstInvalidStep(step: string) : string {
        // If all the previous steps are validated, return blank
        // Otherwise, return the first invalid step
        var found = false;
        var valid = true;
        var redirectToStep = '';
        for (var i = 0; i < this.AskHelp_workflow.length && !found && valid; i++) {
            let item = this.AskHelp_workflow[i];
            if (item.step === step) {
                found = true;
                redirectToStep = '';
            }
            else {
                valid = item.valid;
                redirectToStep = item.step
            }
        }
        return redirectToStep;
    }
}