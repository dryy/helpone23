import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AuthGuard } from './shared';

const routes: Routes = [

    { path: '', loadChildren: './landing/landing.module#LandingModule' },
    { path: 'portal', loadChildren: './layout/layout.module#LayoutModule', canActivateChild: [AuthGuard] },
    { path: 'access', loadChildren: './access/access.module#AccessModule', canActivate: [AuthGuard]},

    { path: 'error', loadChildren: './server-error/server-error.module#ServerErrorModule' },
    { path: 'access-denied', loadChildren: './access-denied/access-denied.module#AccessDeniedModule' },
    { path: '**', redirectTo: 'error404' }

];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
