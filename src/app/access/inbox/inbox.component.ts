import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators'; 
import { Router,ActivatedRoute } from '@angular/router';
import { UserInfo } from '../../_models';
import { HelpnoteService } from '../../_services';
import { VariablesGlobales } from 'src/app/_models/variablesGlobales';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';  
import { HelpNotes } from 'src/app/_models/helpNotes';
import { AccessComponent } from '../access.component';

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.scss']
})
export class InboxComponent implements OnInit {

  notes: HelpNotes[] = [];
  notesReceived: HelpNotes[] = [];
  messages: any;
  showHide: boolean[] = [];
  currentUser: UserInfo;
  regForm: FormGroup;
  MessageSent: string;
  submitted = false;	
  ValUser: string;
  RecentsHelpReq: any;
  OpenHelpsReq: any;
  BidHelpDataTab: any;
  BidHelpInfoDataTab: any;

  constructor(  private formLog: FormBuilder,
                public param: VariablesGlobales,
                private router: Router,
                public route: ActivatedRoute,
                private helpnoteService: HelpnoteService,
                public acc: AccessComponent) { 

    this.router.routeReuseStrategy.shouldReuseRoute = function() {
        return false;
    };    
    this.acc.menuManager = 'Inbox';
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.ValUser=this.currentUser.user._id;    
    this.getAllNotes();
    this.getAllReceivedNotes();
  }

  ngOnInit() {
    this.regForm = this.formLog.group({    
        'receiver':['', Validators.required],
        'subject':['', Validators.required],
        'notes':['', Validators.required]
    });
  }

  onSubmit(){
    this.submitted = true;
      if (this.regForm.invalid) {
        return;
      }
      this.helpnoteService.saveHelpNotes(this.f.receiver.value, this.f.subject.value, this.f.notes.value, this.currentUser.user._id, this.currentUser.user.username)
          .pipe(first())
          .subscribe(
              (data : any) => {
                  console.log(data.data); 
                  console.log(data.message);
                  if(data.status=='success'){
                      this.aaa();
                  }
                  this.MessageSent = data.message;                   
              });
  }
  get f() { return this.regForm.controls; }

  getAllNotes(){ 
      this.helpnoteService.getAll(this.currentUser.user._id).pipe(first()).subscribe((notes: any) => { 
          this.notes = notes.data; 
          console.log(this.notes); 
          console.log(notes.message);
      });          
      for(var i=0; i<10000; i++){
          this.showHide[i]=false;
      }
  }

  //get all notes sents to the corrent user
  getAllReceivedNotes(){ 
      this.helpnoteService.getAllReceived(this.currentUser.user._id).pipe(first()).subscribe((notes: any) => { 
          this.notesReceived = notes.data; 
          console.log(this.notesReceived); 
          console.log(notes.message); 
      });  
  }

  accordions(i: number){
      if(this.showHide[i]==false){
          this.showHide[i] = true;
      }else{
          this.showHide[i] = false;
      }
  }

  aaa(){
      this.regForm.reset();
      this.MessageSent='';
      this.submitted = false;
  }

  onDelete(notesId: string){
    this.helpnoteService.supNote(notesId).pipe(first()).subscribe((notes: any) => { 
        console.log(notes.message); 
        console.log(notes.status); 
        this.getAllReceivedNotes();
        this.getAllNotes();
    });
  }
}