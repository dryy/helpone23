import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InboxRoutingModule } from './inbox-routing.module';
import { InboxComponent } from './inbox.component';
import {ReactiveFormsModule,FormsModule} from '@angular/forms';

import { SharedPipesModule } from './../../shared';


@NgModule({
  imports: [
    CommonModule,
    SharedPipesModule,
    InboxRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [InboxComponent]
})
export class InboxModule { }
