import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyopenhelpsComponent } from './myopenhelps.component';
const routes: Routes = [{ path: '', component: MyopenhelpsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyopenhelpsRoutingModule { }
