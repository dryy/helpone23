import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { MyopenhelpsRoutingModule } from './myopenhelps-routing.module';
import { MyopenhelpsComponent } from './myopenhelps.component';

import {NgxPaginationModule} from 'ngx-pagination'; 

import { SharedPipesModule } from './../../shared';

@NgModule({
  imports: [
    CommonModule,
    SharedPipesModule,
    FormsModule,NgxPaginationModule,
    MyopenhelpsRoutingModule
  ],
  declarations: [MyopenhelpsComponent]
})
export class MyopenhelpsModule { }
