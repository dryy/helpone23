import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators'; 
import { Router,ActivatedRoute } from '@angular/router';
import { UserInfo } from '../../_models';
import { WalletService,ProfileService,ServiceListingService,ServiceCatService,PagerService } from '../../_services';

@Component({
  selector: 'app-myopenhelps',
  templateUrl: './myopenhelps.component.html',
  styleUrls: ['./myopenhelps.component.scss']
})
export class MyopenhelpsComponent implements OnInit {
 
 currentUser: UserInfo;
  ValUser: string;
  MyFund: any;
  MyInfo: any;
  MyStats: any;
  services_categ: any;
  RecentsHelpReq: any;
  OpenHelpsReq: any;
  queryString:any;
  p: number = 1;

  constructor(  private route: ActivatedRoute,
                private router: Router,
                private profileServ: ProfileService,
                private SetServiceCat: ServiceCatService,
                private pagerService: PagerService,
                private serviceList: ServiceListingService,
                private myWalletService: WalletService) { 

  this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

  this.ValUser=this.currentUser.user._id;

  }

  ngOnInit() {

  	this.getAllCateg();
    this.getUserStatistics();
    this.getProfileInfoData();
    this.getCurrentWalletCharge();
    this.getOpenHelpsListing();

  }

    getCurrentWalletCharge() {
    this.myWalletService.getMyWallet(this.ValUser).pipe(first()).subscribe(funds => { 
                        this.MyFund = funds; 
    });
  }

  getUserStatistics() {
    this.profileServ.getUserStats(this.ValUser).pipe(first()).subscribe(UserStatsValues => {          
            this.MyStats = UserStatsValues; 
      });
  } 

  getOpenHelpsListing() {
    this.serviceList.getMyOpenHelps(this.ValUser).pipe(first()).subscribe(OpenHelpsReqData => {          
            this.OpenHelpsReq = OpenHelpsReqData; 
      });
  }
 

  getAllCateg() {
       this.SetServiceCat.getAll().pipe(first()).subscribe(serv_cats => { 
            this.services_categ = serv_cats; 
        });
  }

  
    getCategoryLogo(idCategory:string,TabCat:any): string {

              let verified:boolean = false, i=0;
              let LogoName="";
              if (idCategory && TabCat) {
                 while (!verified && i < TabCat.length ) {

                if ( String(TabCat[i]._id) == idCategory){
                    LogoName=TabCat[i].logo;
                    verified=true;
                  }

                  i++;
                }

              } 

           return LogoName;
    }    

  getCategoryName(idCategory:string,TabCat:any): string {

              let verified:boolean = false, i=0;
              let VarName="";
              if (idCategory && TabCat) {
                 while (!verified && i < TabCat.length ) {

                if ( String(TabCat[i]._id) == idCategory){
                    VarName=TabCat[i].categoryName;
                    verified=true;
                  }

                  i++;
                }

              } 

           return VarName;
    }

    getProfileInfoData(){
      this.profileServ.getProfileInfo(this.ValUser).pipe(first()).subscribe(UserInfoValues => {          
            this.MyInfo = UserInfoValues; 
      });
  }

}
