import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MyopenhelpsComponent } from './myopenhelps.component';

describe('MyopenhelpsComponent', () => {
  let component: MyopenhelpsComponent;
  let fixture: ComponentFixture<MyopenhelpsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyopenhelpsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyopenhelpsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
