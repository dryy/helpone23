import { ProfileDataModule } from './../components/profile-data/profile-data.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccessRoutingModule } from './access-routing.module';
import { AccessComponent } from './access.component';

import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { ProfileUserComponent } from './profile-user/profile-user.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatTabsModule } from '@angular/material';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

@NgModule({
  imports: [
    CommonModule,
    AccessRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MatTabsModule,
    ProfileDataModule,
    NgMultiSelectDropDownModule
  ],
  declarations: [AccessComponent,HeaderComponent, FooterComponent, ProfileUserComponent]
  
})
export class AccessModule { }

