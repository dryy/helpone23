import { Component, OnInit,Input } from '@angular/core';

// Must import to use Forms functionality
import { FormBuilder, FormGroup, Validators ,FormsModule,NgForm } from '@angular/forms';
import { first,mergeMap  } from 'rxjs/operators';
import { Observable, of, interval } from 'rxjs';
import { IPayPalConfig,ICreateOrderRequest } from 'ngx-paypal';
import { Router,ActivatedRoute } from '@angular/router';
import {ClickEvent,RatingChangeEvent} from 'angular-star-rating';

import { UserInfo } from '../../_models';

import { WalletService,AlertService,ProfileService,ServiceListingService,ServiceCatService,BidService,HelpnoteService,ScheduleDateService,MethodPaymentService } from '../../_services';

import * as moment from 'moment';

declare var jQuery:any;
declare var $ :any;

@Component({
  selector: 'app-completebid',
  templateUrl: './completebid.component.html',
  styleUrls: ['./completebid.component.scss']
})
export class CompletebidComponent implements OnInit {

 onClickResult: ClickEvent;
 onRatingChangeResult: RatingChangeEvent;

  public payPalConfig ? : IPayPalConfig;

  currentUser: UserInfo;
  ValUser: string;
  MyFund: any;
  MyInfo: any;
  MyStats: any;
  p: number = 1;
  t: number = 1;

   paymentNameValue: string='';
   paymentVal: any;

  buttonDeposit:boolean= true;
  buttonPaypal:boolean= false;

  bidCol:boolean=false;
  HelpFormCol:boolean=false;
  HelpFormButton:boolean=true;
  PayButton:boolean=false;

  SDFormCol:boolean=false;
  SDFormButtonLabel:string='Add New Date';

  services_categ: any;
  ServiceBidInfos: any;
  ServiceBidUser: any;
  ServicePicInfos: any;
  ServiceTimeInfos: any;
  ServiceAddressInfos: any;
  BiddersInfo: any;
  dataServPicTab: any;
  dataServTimeTab: any;
  dataServAdressTab: any;
  HelpNotesTab: any;
  ScheduleDateTab: any;

  helpNoteForm: FormGroup;
  submittedForm1 = false;

  scheduleDateForm: FormGroup;
  submittedForm2 = false;

  rechargeWalletForm: FormGroup;
  submitted = false;

  PayHelperForm: FormGroup;
  submittedPay = false;

  RateHelperForm: FormGroup;
  submittedRate = false;

  regForm: FormGroup;
  regForm1: FormGroup;
  note: any;
  note1: any;
  commentaire: any;

  couleur1: string='#aaa';
  couleur2: string='orange';

  constructor(  private route: ActivatedRoute,
                private router: Router,
                private formLog: FormBuilder,
                private alertService: AlertService,
                private SetServiceCat: ServiceCatService,
                private profileServ: ProfileService,
                private SetBid: BidService,
                private SetHelpNote: HelpnoteService,
                private SetScheduleDate: ScheduleDateService,
                private PaymentType: MethodPaymentService,
                private serviceList: ServiceListingService,
                private myWalletService: WalletService) {

  this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

  this.ValUser=this.currentUser.user._id;

  }

  ngOnInit() {

    this.getAllCateg();
    this.initPaypalConfig();
    this.getPaymentType();
    this.getUserStatistics();
    this.getProfileInfoData();
    this.getAllHelpNotesFunc();
    this.getAllScheduleDatesFunc();
    this.getCurrentWalletCharge();
    this.getServiceBidInfo();
    this.getServicesBiddersInfo();

            // To initialize RechargeForm
    this.rechargeWalletForm = this.formLog.group({
      'methodpayment_id':['', Validators.required],
      'methodpayment_name':[this.paymentNameValue, Validators.required],
      'user_id':[this.ValUser, Validators.required],
      'amount':['', Validators.required]
    });

           // To initialize PayHelperForm
    this.PayHelperForm = this.formLog.group({
      'tips':['0', Validators.required],
      'service_price':['', Validators.required],
      'serviceaskforhelp_id':[this.route.snapshot.params.id, Validators.required]
    });

           // To initialize PayHelperForm
    this.RateHelperForm = this.formLog.group({
      'etoiles_donnees_par_offer_service':['0', Validators.required],
      'commentaire_offer_service':['', Validators.required]
    });

    // To initialize helpNoteForm
    this.helpNoteForm = this.formLog.group({
      'serviceAskForHelpId':[this.route.snapshot.params.id, Validators.required],
      'user_id':[this.ValUser, Validators.required],
      'status_help_note':['', Validators.required],
      'note_help':['', Validators.required]
    });

    // To initialize scheduleDateForm
    this.scheduleDateForm = this.formLog.group({
      'serviceAskForHelpId':[this.route.snapshot.params.id, Validators.required],
      'user_id':[this.ValUser, Validators.required],
      'date_schedule':['', Validators.required],
      'time_schedule':['', Validators.required]
    });

    this.regForm = this.formLog.group({    
      'commentaire':['', Validators.required],
      'sousCategory_id':['', Validators.required]
    }); 

    this.regForm1 = this.formLog.group({    
      'commentaire':['', Validators.required],
      'sousCategory_id':['', Validators.required]
    });

  }


  private initPaypalConfig(): void {
        this.payPalConfig =  {
            currency: 'USD',
            clientId: 'Adihiwo7dpJ3GzDqFoP6BK-KsrfXsWLIRsDl51vQ-etwrUGCaO7ZHUNkb_S_iMQFQqbzEX5boNFNudXX',
            createOrderOnClient: (data) => < ICreateOrderRequest > {
                intent: 'CAPTURE',
                purchase_units: [{
                    amount: {
                        currency_code: 'USD',
                        value: this.f.amount.value,
                        breakdown: {
                            item_total: {
                                currency_code: 'USD',
                                value: this.f.amount.value
                            }
                        }
                    },
                    items: [{
                        name: 'HELP123 FUNDS DEPOSIT',
                        quantity: '1',
                        category: 'DIGITAL_GOODS',
                        unit_amount: {
                            currency_code: 'USD',
                            value: this.f.amount.value,
                        },
                    }]
                }]
            },

            style: {
                label: 'paypal',
                layout: 'horizontal'
            },
            onApprove: (data, actions) => {
                console.log('onApprove - transaction was approved, but not authorized', data, actions);
                actions.order.get().then(details => {
                    console.log('onApprove - you can get full order details inside onApprove: ', details);
                });

            },
            onClientAuthorization: (data) => {
                console.log('onClientAuthorization - you should probably inform your server about completed transaction at this point', data);

               this.myWalletService.ChargeWallet(this.rechargeWalletForm.value)
               .pipe(first())
               .subscribe(

                 //on success
                (dataGet : any) => {
                     this.alertService.success(dataGet.message, true);
                     let ValueAdded= ( parseInt(this.f.amount.value) + parseInt(this.f.user_balance.value) );
                     this.myWalletService.setBalance(ValueAdded.toString());
                     this.submitted = false;
                     this.rechargeWalletForm.patchValue({"methodpayment_id": "","amount": "","methodpayment_name": "" });
                     this.buttonDeposit= true;
                     this.buttonPaypal= false;

                },

                //on error
                error => {
                     this.alertService.error(error);
                },

                 //on complete
                () => {
                     this.initPaypalConfig();
                     this.getCurrentWalletCharge();
                });

            },
            onCancel: (data, actions) => {
                console.log('OnCancel', data, actions);

            },
            onError: err => {
                console.log('OnError', err);
            },
            onClick: () => {
                console.log('onClick');
            },

        };
    }



   // convenience getter for easy access to form fields
    get HF() { return this.helpNoteForm.controls; }
    get SD() { return this.scheduleDateForm.controls; }
    get f() { return this.rechargeWalletForm.controls; }
    get pF() { return this.PayHelperForm.controls; }
    get rF() { return this.RateHelperForm.controls; }
    get g() { return this.regForm.controls; }
    get g1() { return this.regForm1.controls; }


    showPaymentName(value:string) {

          this.rechargeWalletForm.patchValue({
             "methodpayment_name": value
           });

           if(value=="PayPal") {
               this.buttonDeposit= false;
               this.buttonPaypal= false;
           }
           else{
               this.buttonDeposit= true;
               this.buttonPaypal= false;
           }
      }

       inputValidator(event: any) {

        if ( (event.target.value.length > 0) && (this.f.methodpayment_name.value=="PayPal") ) {
          this.buttonDeposit= false;
          this.buttonPaypal= true;
        }
        else {
          this.buttonDeposit= true;
          this.buttonPaypal= false;
        }
     }


     StrToInt (x: string) {
        return (parseInt(x, 10));
      }

     getPaymentType(){
       this.PaymentType.getAll().pipe(first()).subscribe(paymentData => {
         this.paymentVal = paymentData;
      });

    }

    getCurrentWalletCharge() {
    this.myWalletService.getMyWallet(this.ValUser).pipe(first()).subscribe(funds => {
                        this.MyFund = funds; 
                        console.log(this.MyFund);
    });
  }

    getAllCateg() {
       this.SetServiceCat.getAll().pipe(first()).subscribe(serv_cats => {
            this.services_categ = serv_cats;
        });
    }

  getUserStatistics() {
    this.profileServ.getUserStats(this.ValUser).pipe(first()).subscribe(UserStatsValues => {
            this.MyStats = UserStatsValues;
      });
  }

  getServiceBidInfo() {
    if(this.ServiceBidInfos==undefined){
      console.log('lllllllllllllaaaaaaaaaaaaaaaaaddddddd : '+ this.ServiceBidInfos);
    }
    
     this.serviceList.getServiceInfos(this.route.snapshot.params.id).pipe(first()).subscribe(ServiceInfo => {
            this.ServiceBidInfos = ServiceInfo;
            console.log('ServiceBidInfos : '+ this.ServiceBidInfos.data.user_id);

            this.PayHelperForm.patchValue({"service_price": this.ServiceBidInfos.data.price_bid_celui_choisit});

      this.profileServ.getProfileInfo(this.ServiceBidInfos.data.user_id).pipe(first()).subscribe(UserSV => {
            this.ServiceBidUser = UserSV;
          });

      });

   this.serviceList.getServicePictures(this.route.snapshot.params.id).pipe(first()).subscribe(ServiceV => {
            this.ServicePicInfos = ServiceV;
      });

   this.serviceList.getServiceTime(this.route.snapshot.params.id).pipe(first()).subscribe(ServiceV2 => {
            this.ServiceTimeInfos = ServiceV2;
      });

     this.serviceList.getServiceAddress(this.route.snapshot.params.id).pipe(first()).subscribe(ServiceV3 => {
            this.ServiceAddressInfos = ServiceV3;
      });

  }

  getServicesBiddersInfo() {

     // this.SetBid.getAllBidders(this.route.snapshot.params.id).pipe(first()).subscribe(ServiceV3 => {
       //     this.BiddersInfo = ServiceV3;
     // });

       interval(6000)
      .pipe( mergeMap ( () => this.SetBid.getAllBidders(this.route.snapshot.params.id) ) )
      .subscribe( ServiceV3 => {
            this.BiddersInfo = ServiceV3;
      });
  }

getCategoryName(idCategory:string,TabCat:any): string {

      let verified:boolean = false, i=0;
      let VarName="";
      if (idCategory && TabCat) {
         while (!verified && i < TabCat.length ) {

        if ( String(TabCat[i]._id) == idCategory){
            VarName=TabCat[i].categoryName;
            verified=true;
          }

          i++;
        }

      }

   return VarName;
    }



    getProfileInfoData() {
      this.profileServ.getProfileInfo(this.ValUser).pipe(first()).subscribe(UserInfoValues => {
            this.MyInfo = UserInfoValues;
      });
    }

    getAllHelpNotesFunc() {

      //this.SetHelpNote.getAllHelpNotes(this.route.snapshot.params.id).pipe(first()).subscribe(HelpNoteData => {
        //      this.HelpNotesTab = HelpNoteData;
      // });

      interval(6000)
      .pipe( mergeMap ( () => this.SetHelpNote.getAllHelpNotes(this.route.snapshot.params.id) ) )
      .subscribe( HelpNoteData => {
            this.HelpNotesTab = HelpNoteData;
      });
    }

    getAllScheduleDatesFunc() {

  // this.SetScheduleDate.getAllScheduleDates(this.route.snapshot.params.id).pipe(first()).subscribe(ScheduleInfo => {
       //     this.ScheduleDateTab = ScheduleInfo;
      // });

      interval(6000)
      .pipe( mergeMap ( () => this.SetScheduleDate.getAllScheduleDates(this.route.snapshot.params.id) ) )
      .subscribe( ScheduleInfo => {
            this.ScheduleDateTab = ScheduleInfo;
      });

    }

      ShowHelpNoteForm(){
        this.HelpFormCol=true;
        this.HelpFormButton=false;
      }

      HideHelpNoteForm(){
        this.HelpFormCol=false;
        this.HelpFormButton=true;
      }

      // Executed When Form Is Submitted
    SaveNote() {

        this.submittedForm1 = true;

        // stop here if form is invalid
        if (this.helpNoteForm.invalid) {
            return;
        }

         this.SetHelpNote.saveHelpNote(this.helpNoteForm.value).pipe(first()).subscribe(

             //on success
            (dataGet : any) => {
                 this.helpNoteForm.reset();
                 this.submittedForm1 = false;
                 this.alertService.success(dataGet.message, true);

            },

            //on error
            error => {
                 this.alertService.error(error);
            },

          //on complete
           () => {
            this.HideHelpNoteForm();
            this.getAllHelpNotesFunc();
           });


      }

      showSDForm(){
        this.SDFormCol=true;
        this.SDFormButtonLabel='Save New Date';
      }

      hideSDForm(){
        this.SDFormCol=false;
        this.SDFormButtonLabel='Add New Date';
      }

      ShowPayButton(){
        this.SDFormCol=false;
        this.HelpFormButton=false;
        this.PayButton=true;
      }

      HidePayButton(){
        this.SDFormCol=false;
        this.HelpFormButton=true;
        this.PayButton=false;
        console.log(this.PayButton);
      }

        // Executed When Form Is Submitted
    SaveDate() {

        this.submittedForm2 = true;

        // stop here if form is invalid
        if (this.scheduleDateForm.invalid) {
            return;
        }


         let FormValue : any = {  serviceAskForHelpId:this.SD.serviceAskForHelpId.value,
                                  user_id:this.SD.user_id.value,
                                  date_schedule:moment(this.SD.date_schedule.value).format('YYYY-MM-DD'),
                                  time_schedule:moment(this.SD.time_schedule.value).format('hh:mm A') };

         this.SetScheduleDate.saveScheduleDate(FormValue).pipe(first()).subscribe(

             //on success
            (dataGet : any) => {
                 this.helpNoteForm.reset();
                 this.submittedForm2 = false;
                 this.alertService.success(dataGet.message, true);

            },

            //on error
            error => {
                 this.alertService.error(error);
            },

          //on complete
           () => {
            this.hideSDForm();
            this.getAllScheduleDatesFunc();
           });


      }

          // Executed When Form Is Submitted
    ActionScheduleDate(IdVal,reponseVal:string) {

    let valueRep1:number;
    let valueRep2:number;

    if(reponseVal=="approved"){valueRep1=1;valueRep2=0; }

    if(reponseVal=="revoked"){valueRep1=0;valueRep2=1; }


         let FormValue : any = {  approved:valueRep1,revoked:valueRep2};

         this.SetScheduleDate.ApproveOrRevoke(IdVal,FormValue).pipe(first()).subscribe(

             //on success
            (dataGet : any) => {
                 this.alertService.success(dataGet.message, true);

            },

            //on error
            error => {
                 this.alertService.error(error);
            },

          //on complete
           () => {
            this.getAllScheduleDatesFunc();
           });


      }

         // Executed When Form Is Submitted
    onSubmit(AmountValue) {

        this.submitted = true;

        // stop here if form is invalid
        if (this.rechargeWalletForm.invalid) {
            return;
        }

         this.myWalletService.ChargeWallet(this.rechargeWalletForm.value)
         .pipe(first())
         .subscribe(

           //on success
          (dataGet : any) => {
               this.alertService.success(dataGet.message, true);
               let ValueAdded= ( parseInt(this.f.amount.value) + parseInt(AmountValue) );
               this.myWalletService.setBalance(ValueAdded.toString());
               this.submitted = false;
               this.rechargeWalletForm.patchValue({"methodpayment_id": "","amount": "","methodpayment_name": "" });
               $('#ModalDepFunds').modal('hide');

          },

          //on error
          error => {
               this.alertService.error(error);
          },

           //on complete
          () => {
               this.getCurrentWalletCharge();
          });

      }

            // Executed When Form Is Submitted
    onPay(AmountVal) {

        this.submittedPay = true;

        // stop here if form is invalid
        if (this.PayHelperForm.invalid) {
            return;
        }

         let dataTAB : any = {  tips:parseInt(this.pF.tips.value),
                                service_price:parseInt(this.pF.service_price.value),
                                serviceaskforhelp_id:this.pF.serviceaskforhelp_id.value };

         this.serviceList.PayHelper(dataTAB) 
         .pipe(first())
         .subscribe(

           //on success
          (dataGet : any) => {
               this.alertService.success(dataGet.message, true);
               let ValueAdded= ( parseInt(AmountVal) - (parseInt(this.pF.tips.value)) );
               this.myWalletService.setBalance(ValueAdded.toString());
               this.submittedPay = false;
               this.PayButton = false;
               this.PayHelperForm.reset();
               $('#ModalPay').modal('hide');

          },

          //on error
          error => {
               this.alertService.error(error);
          },

           //on complete
          () => {

              this.getAllCateg();
              this.getPaymentType();
              this.getUserStatistics();
              this.getProfileInfoData();
              this.getCurrentWalletCharge();;
              this.getServiceBidInfo();
          });

      }

      onStarClick = ($event: ClickEvent) => {
        this.onClickResult = $event;
        this.RateHelperForm.patchValue({"etoiles_donnees_par_offer_service": this.onClickResult.rating});
      };

      onStarRatingChange = ($event: RatingChangeEvent) => {
        this.onRatingChangeResult = $event;
        this.RateHelperForm.patchValue({"etoiles_donnees_par_offer_service": this.onRatingChangeResult.rating});
      };


               // Executed When Form Is Submitted
    onRate() {

        this.submittedRate = true;

        // stop here if form is invalid
        if (this.RateHelperForm.invalid) {
            return;
        }

         let dataTAB : any = {  etoiles_donnees_par_offer_service:parseInt(this.rF.etoiles_donnees_par_offer_service.value),
                                commentaire_offer_service:this.rF.commentaire_offer_service.value };

         this.serviceList.RateHelper(dataTAB,this.route.snapshot.params.id)
         .pipe(first())
         .subscribe(

           //on success
          (dataGet : any) => {
               this.alertService.success(dataGet.message, true);
               this.submittedRate = false;
               this.RateHelperForm.patchValue({"etoiles_donnees_par_offer_service": "0","commentaire_offer_service":""});
               $('#ModalRate').modal('hide');

          },

          //on error
          error => {
               this.alertService.error(error);
          },

           //on complete
          () => {

              this.getAllCateg();
              this.getPaymentType();
              this.getUserStatistics();
              this.getProfileInfoData();
              this.getCurrentWalletCharge();;
              this.getServiceBidInfo();
          });

      }

      // permet de renseigner un service comme complété

      onTermine() {
        console.log(this.route.snapshot.params.id);
         this.serviceList.compliteSevice(this.route.snapshot.params.id)
         .pipe(first())
         .subscribe(

           //on success
          (dataGet : any) => {
               this.alertService.success(dataGet.message, true);

          },

          //on error
          error => {
               this.alertService.error(error);
          },

           //on complete
          () => {

              // this.getAllCateg();
              // this.getPaymentType();
              // this.getUserStatistics();
              // this.getProfileInfoData();
              // this.getCurrentWalletCharge();;
              this.getServiceBidInfo();
          });

      }

      rate(a: any, valueID:any){
        this.note = a;
        this.commentaire = this.g.commentaire.value;
        console.log("La note est : "+ a+" et le commentaire est : "+this.g.commentaire.value);
        
      }

      rateResult(a: number, b:number){
        this.note = b;
        // console.log("la note est : = "+this.note);
        if(a>this.note){
          return this.couleur1;
        }else{
          return this.couleur2;
        }
      }

      setServiceEtoile(){
        
        let notation : any = {  nbre_etoiles:parseInt(this.note),
          commentaire_offer_service:this.commentaire };
          this.serviceList.setServiceEtoile(this.ServiceBidInfos.data._id,notation)
          .pipe(first())
          .subscribe(
            //on success
            (dataGet : any) => {
                console.log(dataGet.message);
                console.log(dataGet.data);
                this.router.navigate(['/access/completeBid', this.ValUser]);

            },

            //on error
            error => {
              console.log('Une erreur est survenue');
            },

            //on complete
            () => {
              console.log('completed successfully');
            });
          
      }

      rate1(a: any){
        this.note1 = a;
        this.commentaire = this.g1.commentaire.value;
        console.log("La note est : "+ this.note1 +" et le commentaire est : "+this.commentaire);
        
      }

      // permet de modeliser la note donnée par l'offreur
      rateOffer(a: number, b:number){
        this.note1 = b;
        // console.log("la note est : = "+this.note);
        if(a>this.note1){
          return this.couleur1;
        }else{
          return this.couleur2;
        }
      }

      // Permet à l'offreur de noter le déroulemet du service
      setOfferEtoile(){        
        let notation : any = {  nbre_etoiles_celui_choisit:parseInt(this.note1),
          commentaire_celui_choisit:this.commentaire };
          console.log(notation);
          console.log(this.ServiceBidInfos.data._id);
          // return;
          this.serviceList.setOfferEtoile(this.ServiceBidInfos.data._id,notation)
          .pipe(first())
          .subscribe(
            //on success
            (dataGet : any) => {
                console.log(dataGet.message);
                console.log(dataGet.data);
                this.router.navigate(['/access/completeBid', this.ValUser]); 
                // [routerLink]="['/access/completeBid/',valueOH._id]"

            },

            //on error
            error => {
              console.log('Une erreur est survenue');
            },

            //on complete
            () => {
              console.log('completed successfully');
            });
          
      }      

}

