import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CompletebidComponent } from './completebid.component';

const routes: Routes = [{ path: '', component: CompletebidComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompletebidRoutingModule { }
