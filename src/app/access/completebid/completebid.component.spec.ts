import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompletebidComponent } from './completebid.component';

describe('CompletebidComponent', () => {
  let component: CompletebidComponent;
  let fixture: ComponentFixture<CompletebidComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompletebidComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompletebidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
