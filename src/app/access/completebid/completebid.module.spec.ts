import { CompletebidModule } from './completebid.module';

describe('CompletebidModule', () => {
  let completebidModule: CompletebidModule;

  beforeEach(() => {
    completebidModule = new CompletebidModule();
  });

  it('should create an instance', () => {
    expect(completebidModule).toBeTruthy();
  });
});
