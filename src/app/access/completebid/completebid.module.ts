import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CompletebidRoutingModule } from './completebid-routing.module';
import { CompletebidComponent } from './completebid.component';

import { FormsModule,ReactiveFormsModule } from '@angular/forms'; 

import { AlertModule } from './../../_directives/alert.module';

import {DataTableModule} from 'angular-6-datatable';

import {NgxPaginationModule} from 'ngx-pagination'; 

import { StarRatingModule } from 'angular-star-rating';

import { SharedPipesModule } from './../../shared';

import { NgxPayPalModule } from 'ngx-paypal';

import { OwlDateTimeModule, OwlNativeDateTimeModule,OWL_DATE_TIME_FORMATS  } from 'ng-pick-datetime';

import { NgKnifeModule } from 'ng-knife';

    // See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
export const MY_MOMENT_FORMATS = {
    parseInput: 'l LT',
    fullPickerInput: 'L LT',
    datePickerInput: 'L',
    timePickerInput: 'LT',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',


};

@NgModule({
  imports: [
    CommonModule,
    CompletebidRoutingModule,StarRatingModule,
    FormsModule,ReactiveFormsModule,NgKnifeModule,DataTableModule,
    OwlDateTimeModule, OwlNativeDateTimeModule ,
    AlertModule,NgxPaginationModule,SharedPipesModule,NgxPayPalModule
  ],
  declarations: [CompletebidComponent],
  providers: [
        // use french locale
         {provide: OWL_DATE_TIME_FORMATS, useValue: MY_MOMENT_FORMATS},
    ],
})
export class CompletebidModule { }
