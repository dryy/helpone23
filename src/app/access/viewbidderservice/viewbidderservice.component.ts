import { Component, OnInit,Input } from '@angular/core';

// Must import to use Forms functionality  
import { FormBuilder, FormGroup, Validators ,FormsModule,NgForm } from '@angular/forms';
import { first,mergeMap  } from 'rxjs/operators'; 
import { Observable, of, interval } from 'rxjs';
import { Router,ActivatedRoute } from '@angular/router';
import { UserInfo } from '../../_models';
import { ServiceInfoData } from '../../_models/services';

import { WalletService,AlertService,ProfileService,ServiceListingService,ServiceCatService,BidService, HelpnoteService } from '../../_services';

import * as moment from 'moment';

@Component({
  selector: 'app-viewbidderservice',
  templateUrl: './viewbidderservice.component.html',
  styleUrls: ['./viewbidderservice.component.scss']
})
export class ViewbidderserviceComponent implements OnInit {

  currentUser: UserInfo;
  ValUser: string;
  MyFund: any;
  MyInfo: any;
  MyStats: any;
  p: number = 1;
  answerInfos: any;
  minDate: string;

  @Input() serviceData: ServiceInfoData;

  submittedForm1 = false;
  submittedForm2 = false;
  submittedFormBidAccept = false;
  bidCol:boolean=false;
  services_categ: any;
  ServiceBidInfos: any;
  ServiceBidUser: any;
  ServicePicInfos: any;
  ServiceTimeInfos: any;
  ServiceAddressInfos: any;
  BiddersInfo: any;
  dataServPicTab: any;
  dataServTimeTab: any;
  dataServAdressTab: any;
  helpInfos: any;
  ansHelps: any;

  ShowF1EditForm : boolean = false;
  ShowF1Form : boolean = true;
  ShowF2EditForm : boolean = false;
  ShowF2Form : boolean = true;

  Form1: FormGroup;
  Form2: FormGroup;
  FormBidAccept: FormGroup;
  regForm: FormGroup;

  constructor(  private route: ActivatedRoute,
                private router: Router,
                private formLog: FormBuilder,
                private alertService: AlertService,
                private SetServiceCat: ServiceCatService,
                private profileServ: ProfileService,
                private SetBid: BidService,
                private serviceList: ServiceListingService,
                private helpnoteService: HelpnoteService,
                private myWalletService: WalletService) { 

  this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

  this.ValUser=this.currentUser.user._id;

  }

  ngOnInit() {
    this.getDate();

    this.getAllCateg();
    this.getUserStatistics();
    this.getProfileInfoData();
    this.getCurrentWalletCharge();;
    this.getServiceBidInfo();
    this.getServicesBiddersInfo();
    this.getAllNotes();

    this.serviceData = this.serviceList.getServiceData(); 

    // To initialize RechargeForm  
    this.Form1 = this.formLog.group({    
      'description':['', Validators.required]
    }); 

    // To initialize RechargeForm  
    this.Form2 = this.formLog.group({    
      'date0':['', Validators.required],
      'time0':['', Validators.required],
      'date1':['', Validators.required],
      'time1':['', Validators.required],
      'date2':['', Validators.required],
      'time2':['', Validators.required]
    }); 

    // To initialize RechargeForm  
    this.FormBidAccept = this.formLog.group({    
      'user_id_celui_choisit':[''],
      'username_celui_choisit':[''],
      'firstname_celui_choisit':[''],
      'location_name_celui_choisit':[''],
      'number_helps_asked_celui_choisit':[''],
      'number_helps_offer_celui_choisit':[''],
      'nbre_etoiles_celui_choisit':[''],
      'picture_celui_choisit':[''],
      'price_bid_celui_choisit':[''],
      'date_livraison_celui_choisit':[''],
      'time_livraison_celui_choisit':[''],
      'description_proposition_celui_choisit':[''],
      'etoiles_donnees_par_offer_service':[''],
      'commentaire_offer_service':['']
    }); 

    this.regForm = this.formLog.group({ 
      'messService':['', Validators.required]
    });

  }

  // Cette fonction permet de récupérer la date courante afin d'indiquer dans le template le fait que l'utilisateur ne peut pas choisir une date antérieure
  getDate(){
    let date: Date = new Date();
    console.log("Date = " + date);
    console.log(date.getMonth());
    let mois = date.getMonth()+1+'';
    let dat = date.getDate()+'';
    if(mois.length==1){
      mois=0+mois;
    }
    if(dat.length==1){
      dat=0+dat;
    }
    this.minDate = date.getFullYear()+'-'+mois+'-'+dat;
    console.log("La Date = " + this.minDate);
  }

   // convenience getter for easy access to form fields
    get F1() { return this.Form1.controls; }
    get F2() { return this.Form2.controls; }
    get FBA() { return this.FormBidAccept.controls; }
    get F3() { return this.regForm.controls; }

    getCurrentWalletCharge() {
    this.myWalletService.getMyWallet(this.ValUser).pipe(first()).subscribe(funds => { 
                        this.MyFund = funds; 
    });
  }

    getAllCateg() {
       this.SetServiceCat.getAll().pipe(first()).subscribe(serv_cats => { 
            this.services_categ = serv_cats; 
        });
    }

  getUserStatistics() {
    this.profileServ.getUserStats(this.ValUser).pipe(first()).subscribe(UserStatsValues => {          
            this.MyStats = UserStatsValues; 
      });
  } 

  getAllNotes(){
    this.helpnoteService.getAllHelpNotes(this.route.snapshot.params.id).pipe(first()).subscribe(helpInfos => {          
      this.helpInfos = helpInfos; 
      console.log('notes... : '+ this.helpInfos);  
    });
  }

  getAllAnswer(helpnotes_id){
    this.helpnoteService.getAllAnswerHelp(helpnotes_id).pipe(first()).subscribe(ansHelps => {          
      this.ansHelps = ansHelps; 
      console.log('notes... : '+ this.ansHelps);  
    });
  }

  getServiceBidInfo() {
     this.serviceList.getServiceInfos(this.route.snapshot.params.id).pipe(first()).subscribe(ServiceInfo => {          
            this.ServiceBidInfos = ServiceInfo; 
        
      this.profileServ.getProfileInfo(this.ServiceBidInfos.data.user_id).pipe(first()).subscribe(UserSV => {          
            this.ServiceBidUser = UserSV; 
          });

      this.serviceList.setServiceInfoData(this.ServiceBidInfos.data);

      });

   this.serviceList.getServicePictures(this.route.snapshot.params.id).pipe(first()).subscribe(ServiceV => {          
            this.ServicePicInfos = ServiceV; 

            this.dataServPicTab=[];
            for(var i=0; i< this.ServicePicInfos.data.length; i++)  {
              this.dataServPicTab.push({_id:this.ServicePicInfos.data[i]._id,picture:this.ServicePicInfos.data[i].picture});
            }
            this.serviceList.SetServiceInfoPicture(this.dataServPicTab);
      });

    this.serviceList.getServiceTime(this.route.snapshot.params.id).pipe(first()).subscribe(ServiceV2 => {          
            this.ServiceTimeInfos = ServiceV2; 

            this.dataServTimeTab=[];
            for(var i=0; i< this.ServiceTimeInfos.data.length; i++)  {
              this.dataServTimeTab.push({_id:this.ServiceTimeInfos.data[i]._id,date_service:moment(this.ServiceTimeInfos.data[i].date).format('YYYY-MM-DD'),time_service:this.ServiceTimeInfos.data[i].time});
            }
            this.serviceList.SetServiceInfoTime(this.dataServTimeTab);
      });

     this.serviceList.getServiceAddress(this.route.snapshot.params.id).pipe(first()).subscribe(ServiceV3 => {          
            this.ServiceAddressInfos = ServiceV3; 

            this.dataServAdressTab=[];
            for(var i=0; i< this.ServiceAddressInfos.data.length; i++)  {
              this.dataServAdressTab.push({ addressUser_id:this.ServiceAddressInfos.data[i].addressUser_id,
							              address_title:this.ServiceAddressInfos.data[i].address_title,
							              address:this.ServiceAddressInfos.data[i].address,
							              zip_code_postal:this.ServiceAddressInfos.data[i].zip_code_postal,
							              telephone:this.ServiceAddressInfos.data[i].telephone,
							              city:this.ServiceAddressInfos.data[i].city,
							              email:this.ServiceAddressInfos.data[i].email,
							              deliver_help_to_this_address:this.ServiceAddressInfos.data[i].deliver_help_to_this_address,
							              bill_to_this_address:this.ServiceAddressInfos.data[i].bill_to_this_address});
            }
            this.serviceList.SetServiceAddressTime(this.dataServAdressTab);
      });

  }

  getServicesBiddersInfo() {
     
     //this.SetBid.getAllBidders(this.route.snapshot.params.id).pipe(first()).subscribe(ServiceV3 => {          
       //     this.BiddersInfo = ServiceV3; 
      // });

      interval(6000)
      .pipe( mergeMap ( () => this.SetBid.getAllBidders(this.route.snapshot.params.id) ) )
      .subscribe( ServiceV3 => {          
            this.BiddersInfo = ServiceV3; 
      });  

  }

getCategoryName(idCategory:string,TabCat:any): string {
      
      let verified:boolean = false, i=0;
      let VarName="";
      if (idCategory && TabCat) {
         while (!verified && i < TabCat.length ) {

        if ( String(TabCat[i]._id) == idCategory){
            VarName=TabCat[i].categoryName;
            verified=true;
          }

          i++;
        }

      } 

   return VarName;
    }



    getProfileInfoData() {
      this.profileServ.getProfileInfo(this.ValUser).pipe(first()).subscribe(UserInfoValues => {          
            this.MyInfo = UserInfoValues; 
      });
    }

  // when user clicks the 'edit' link
 editF1(value:string){
    this.ShowF1EditForm = true;
    this.ShowF1Form = false;
    this.Form1.patchValue({"description": value });    
    
}

    // when user clicks the 'cancel' link
cancelF1(){
  this.ShowF1EditForm = false;
  this.ShowF1Form = true;
}

// when user clicks the 'submit' link
SubmitUpdateF1(IDvalue) {
    this.submittedForm1 = true;

    // stop here if form is invalid
    if (this.Form1.invalid) {
        return;
    } 

    this.serviceList.setF1Value(this.F1.description.value);

    this.serviceList.updateServiceInfo(this.serviceData,IDvalue).pipe(first()).subscribe(
       //on success 
       (data : any) => { 
            this.alertService.success(data.message, true);
            this.submittedForm1 = false;                          
       },
       //on error
       error => {
            this.alertService.error(error);
       },
        //on complete
       () => {                
          this.cancelF1();
          this.getAllCateg();
          this.getServiceBidInfo();  
       }); 
  }

    // when user clicks the 'edit' link
 editF2(value:string){
    this.ShowF2EditForm = true;
    this.ShowF2Form = false;

    this.serviceList.getServiceTime(this.route.snapshot.params.id).pipe(first()).subscribe(ServiceV2 => {          
            this.ServiceTimeInfos = ServiceV2; 
            this.Form2.patchValue({ 
            date0:moment(this.ServiceTimeInfos.data[0].date).format('YYYY-MM-DD'),time0:moment(this.ServiceTimeInfos.data[0].time,"hh:mm A"),
            date1:moment(this.ServiceTimeInfos.data[1].date).format('YYYY-MM-DD'),time1:moment(this.ServiceTimeInfos.data[1].time,"hh:mm A"),
            date2:moment(this.ServiceTimeInfos.data[2].date).format('YYYY-MM-DD'),time2:moment(this.ServiceTimeInfos.data[2].time,"hh:mm A")});
            
      });
    
}

    // when user clicks the 'cancel' link
cancelF2(){
  this.ShowF2EditForm = false;
  this.ShowF2Form = true;
}

// when user clicks the 'submit' link
SubmitUpdateF2(IDvalue) {
    this.submittedForm2 = true;

    // stop here if form is invalid
    if (this.Form2.invalid) {
        return;
    }

     this.serviceList.getServiceTime(this.route.snapshot.params.id).pipe(first()).subscribe(ServiceV2 => {          
            this.ServiceTimeInfos = ServiceV2;

     let dateVal = [moment(this.F2.date0.value).format('YYYY-MM-DD'),moment(this.F2.date1.value).format('YYYY-MM-DD'),moment(this.F2.date2.value).format('YYYY-MM-DD')];

     let timeVal = [moment(this.F2.time0.value).format('hh:mm A'),moment(this.F2.time1.value).format('hh:mm A'),moment(this.F2.time2.value).format('hh:mm A')];

      let dataArrayTime=[];

      for(var i=0; i< this.ServiceTimeInfos.data.length; i++)  {
        dataArrayTime.push({_id:this.ServiceTimeInfos.data[i]._id,time_service:timeVal[i],date_service:dateVal[i]});
      }
            this.serviceList.setF2Value(dataArrayTime);

             this.serviceList.updateServiceInfo(this.serviceData,IDvalue).pipe(first()).subscribe(
       //on success 
       (data : any) => { 
            this.alertService.success(data.message, true);
            this.submittedForm1 = false;                          
       },
       //on error
       error => {
            this.alertService.error(error);
       },
        //on complete
       () => {                
          this.cancelF2();
          this.getAllCateg();
          this.getServiceBidInfo();  
       }); 
       
      }); 

 }

 // when user clicks the 'Accept' button

   AcceptBid(IDvalue,val1,val2,val3,val4,val5,val6,val7,val8,val9,val10,val11,val12,val13) {
    
    this.submittedFormBidAccept = true;

    // stop here if form is invalid
    if (this.FormBidAccept.invalid) {
        return;
    }

     let user_id_celui_choisit =val1;
     let username_celui_choisit = val2;
     let firstname_celui_choisit = val3;
     let location_name_celui_choisit = val4;
     let number_helps_asked_celui_choisit = val5;
     let number_helps_offer_celui_choisit = val6;
     let nbre_etoiles_celui_choisit = val7;
     let picture_celui_choisit = val8;
     let price_bid_celui_choisit = val9;
     let date_livraison_celui_choisit = moment(val10).format('YYYY-MM-DD');
     let time_livraison_celui_choisit = val11;
     let description_proposition_celui_choisit = val12;
     let etoiles_donnees_par_offer_service = val13;
     let commentaire_offer_service = '';

     let dataTAB : any = { user_id_celui_choisit:user_id_celui_choisit,
                        username_celui_choisit:username_celui_choisit,
                        firstname_celui_choisit:firstname_celui_choisit,
                        location_name_celui_choisit:location_name_celui_choisit,
                        number_helps_asked_celui_choisit:number_helps_asked_celui_choisit,
                        number_helps_offer_celui_choisit:number_helps_offer_celui_choisit,
                        nbre_etoiles_celui_choisit:nbre_etoiles_celui_choisit,
                        picture_celui_choisit:picture_celui_choisit,
                        price_bid_celui_choisit:price_bid_celui_choisit,
                        date_livraison_celui_choisit:date_livraison_celui_choisit,
                        time_livraison_celui_choisit:time_livraison_celui_choisit,
                        description_proposition_celui_choisit:description_proposition_celui_choisit,
                        etoiles_donnees_par_offer_service:etoiles_donnees_par_offer_service,
                        commentaire_offer_service:commentaire_offer_service };


      this.serviceList.acceptServiceBid(dataTAB,IDvalue).pipe(first()).subscribe( 
       //on success 
       (data : any) => { 
            this.alertService.success(data.message, true);
            this.submittedFormBidAccept = false;                          
       },
       //on error
       error => {
            this.alertService.error(error);
       },
        //on complete
       () => {                
          this.router.navigate(['/access/completeBid/',IDvalue]); 
       }); 


  
      }
//Permet à l'offreur de repondre à un commentaire sur un service qu'il offre
  onSubmitAnswer(helpInfosId: string, client: string){ 
    if (this.regForm.invalid) {
      return;
    }

    let chatValues : any = {
      note_help_id:helpInfosId,
      user_id_celui_qui_recoit:client,
      ans_help:this.F3.messService.value }; 

      this.helpnoteService.saveAnswer(chatValues).pipe(first()).subscribe(answerInfos => {          
        this.answerInfos = answerInfos; 
        console.log('chat : '+ this.answerInfos);
        this.regForm.reset();
        this.getAllNotes();
      });
  }

}
