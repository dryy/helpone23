import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViewbidderserviceRoutingModule } from './viewbidderservice-routing.module';
import { ViewbidderserviceComponent } from './viewbidderservice.component';
import { FormsModule,ReactiveFormsModule } from '@angular/forms'; 
import { AlertModule } from './../../_directives/alert.module';
import {NgxPaginationModule} from 'ngx-pagination'; 

import { SharedPipesModule } from './../../shared';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { NgKnifeModule } from 'ng-knife';

    // See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
// export const MY_MOMENT_FORMATS = {
//     parseInput: 'l LT',
//     fullPickerInput: 'L LT',
//     datePickerInput: 'L',
//     timePickerInput: 'LT',
//     monthYearLabel: 'MMM YYYY',
//     dateA11yLabel: 'LL',
//     monthYearA11yLabel: 'MMMM YYYY',


// };

@NgModule({
  imports: [
    CommonModule,
    ViewbidderserviceRoutingModule,
    FormsModule,ReactiveFormsModule,NgKnifeModule,
    OwlDateTimeModule, OwlNativeDateTimeModule ,
    AlertModule,NgxPaginationModule,SharedPipesModule
  ],
  declarations: [ViewbidderserviceComponent],
  providers: [
        // use french locale. This provider is causing trouble when displaying date, so that is why I comment it
        //  {provide: OWL_DATE_TIME_FORMATS, useValue: MY_MOMENT_FORMATS},
    ],
})
export class ViewbidderserviceModule { }
