import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewbidderserviceComponent } from './viewbidderservice.component';

describe('ViewbidderserviceComponent', () => {
  let component: ViewbidderserviceComponent;
  let fixture: ComponentFixture<ViewbidderserviceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewbidderserviceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewbidderserviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
