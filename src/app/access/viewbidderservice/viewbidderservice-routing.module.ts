import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewbidderserviceComponent } from './viewbidderservice.component';

const routes: Routes = [{ path: '', component: ViewbidderserviceComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewbidderserviceRoutingModule { }
