import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { SharedPipesModule } from './../../shared';

import { DashRoutingModule } from './dash-routing.module';
import { DashComponent } from './dash.component';
import { GridModule } from 'src/app/components/grid/grid.module';


@NgModule({
  imports: [
    CommonModule,
    SharedPipesModule,
    DashRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    GridModule
  ],
  declarations: [DashComponent]
})
export class DashModule { }
