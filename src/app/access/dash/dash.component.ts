import { Component, OnInit, Input } from '@angular/core';
import { first } from 'rxjs/operators'; 
import { Router,ActivatedRoute } from '@angular/router';
import { UserInfo, ProfileData } from '../../_models';

import { WalletService,ProfileService,ServiceListingService } from '../../_services';
import { VariablesGlobales } from 'src/app/_models/variablesGlobales';
import { AccessComponent } from '../access.component';
import { FormGroup, FormBuilder } from '@angular/forms';
import { HelpClassComponent } from '../classes/help-class.component';

@Component({
  selector: 'app-dash',
  templateUrl: './dash.component.html',
  styleUrls: ['./dash.component.scss']
})
export class DashComponent extends HelpClassComponent implements OnInit {
	
	currentUser: UserInfo;
  ValUser: string;
  MyFund: any;
  MyInfo: any;
  MyStats: any;
  OpenHelpsReq: any;
  BidHelpDataTab: any;
  BidHelpInfoDataTab: any;
  activate: any;
  regForm: FormGroup;
  bntStyle: string;
  @Input() profileData: ProfileData;

  chosenOption : any ;
  ChosenOptNum: number;
  HideElement:boolean= false;
  state1:boolean= true;
  state2:boolean= true;
  dataLangTab = [];
  dataSkillsTab = [];
  columnDefs1 = [
    {headerName: 'ID', field: '_id' },
    {headerName: 'DES', field: 'description' },
    {headerName: 'DEV NAME', field: 'device_name'},
    {headerName: 'DATE', field: 'create_date' },
    {headerName: 'MAX PRICE', field: 'max_price'},
    {headerName: 'MIN PRICE', field: 'min_price'}
  ];

  constructor(  private route: ActivatedRoute,
                private router: Router,
                private profileServ: ProfileService,
                private serviceList: ServiceListingService,
                private myWalletService: WalletService,
                public param: VariablesGlobales,
                private formLog: FormBuilder,
                public acc: AccessComponent) { 
  super(serviceList);
  this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

  this.ValUser=this.currentUser.user._id;
    // if(this.param.parametre=='Ask for Helps'){
    //   this.router.navigate(['/access/askHelp-step1']); 
    // }
    this.acc.menuManager = 'Dashboard'; 
    if(this.param.parametre == ''){
      this.param.parametre = this.currentUser.user.fonctionName;
    }
    this.route.queryParams
      .subscribe(params => {
          if(params.activate !== undefined && params.activate === 'true') {
              this.activate = 'Congratulations '+this.currentUser.user.username+' Your account has been successfully activated. Complete your profile';
          }
      });
      console.log(this.activate);
  } 

  ngOnInit() {

    this.profileData = this.profileServ.getUserProfileData();
    
    this. bntStyle = 'log-bg-white';
    this.getUserStatistics();
    this.getProfileInfoData();
    this.getCurrentWalletCharge();
    this.getRecentHelpsRequestsListing(this.columnDefs1, this.ValUser, "serviceaskforhelp/recent_helps_requests");
    this.getOpenHelpsListing();
    this.getMyBiddingHelps();
    this.getProfileInfoFunc();
    this.regForm = this.formLog.group({    
    });

  }

  getProfileInfoFunc(){
    this.profileServ.getProfileInfo(this.ValUser).pipe(first()).subscribe(UserInfoValues => {

          this.MyInfo = UserInfoValues;
          this.dataLangTab = [];
          this.dataSkillsTab = [];

          for(var i=0; i< this.MyInfo.data[1].length; i++)  {
              this.dataSkillsTab.push({sousCategorie_id:this.MyInfo.data[1][i].souscategory_id,sousCategoryName:this.MyInfo.data[1][i].souscategoryName});
          }

          for(var i=0; i< this.MyInfo.data[2].length; i++)  {
              this.dataLangTab.push({langue:this.MyInfo.data[2][i].langue});
          }

          this.profileServ.setUserProfileData(this.MyInfo.data[0],this.dataSkillsTab,this.dataLangTab);
    });
  }

  logTextFunction1(value: string): void {
    this.chosenOption = `You Choose '${value}'\n`;
    this.ChosenOptNum = 1;
    this.HideElement = true; this.state2=true; this.state1 = !this.state1; 
  }

  logTextFunction2(value: string): void {
    this.chosenOption = `You Choose '${value}'\n`;
    this.ChosenOptNum = 2;
    this.HideElement = true; this.state1=true; this.state2 = !this.state2; 
  }

  get f1() { return this.regForm.controls; }

  onNext(value:number) {  
     localStorage.removeItem('optChooseText');  
     localStorage.setItem('optChooseText','Offer Helps');
     console.log(localStorage.getItem('optChooseText'));
     this.router.navigate(['/access/switchAccount']);
    
 }

 onSave(value: number){
  localStorage.removeItem('optChooseText');  
  this.profileServ.SetFonctionName('Ask for Helps');
  this.profileServ.updateProfileInfo(this.profileData,this.ValUser).pipe(first()).subscribe(
    //on success
    (data : any) => {
         console.log(data.message); 
         console.log(data.data[0]); 
         this.currentUser.user = data.data[0];
         localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
         this.router.navigate(['/access']);
    },
    //on error
    error => {
         console.log(error);
    },
     //on complete
    () => {
      this.router.navigate(['/access']);
    });
 }

  typeCompte(){
    if(this.param.parametre == ''){
      this.param.parametre = this.currentUser.user.fonctionName;
    }
  }

    getCurrentWalletCharge() {
    this.myWalletService.getMyWallet(this.ValUser).pipe(first()).subscribe(funds => { 
                        this.MyFund = funds; 
    });
  }

  getUserStatistics() {
    this.profileServ.getUserStats(this.ValUser).pipe(first()).subscribe(UserStatsValues => {          
            this.MyStats = UserStatsValues; 
      });
  } 

  getOpenHelpsListing() {
    this.serviceList.getMyOpenHelps(this.ValUser).pipe(first()).subscribe(OpenHelpsReqData => {          
            this.OpenHelpsReq = OpenHelpsReqData; 
      });
  }

  getMyBiddingHelps() {
    this.serviceList.getMyBiddingHelps(this.ValUser).pipe(first()).subscribe(BidHelpData => {          
            this.BidHelpDataTab = BidHelpData; 
      });
  }



    getProfileInfoData(){
      this.profileServ.getProfileInfo(this.ValUser).pipe(first()).subscribe(UserInfoValues => {          
            this.MyInfo = UserInfoValues; 
      });
  }

  viewBidder(value:string){
      this.router.navigate(['/access/viewBidder/',value]);
  }

}
