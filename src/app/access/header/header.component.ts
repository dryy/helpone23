import { Component, OnInit,Input } from '@angular/core';
import { first } from 'rxjs/operators'; 
import { Router,ActivatedRoute } from '@angular/router';
import {MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { UserInfo } from '../../_models';
import { AuthenticationService,WalletService } from '../../_services';
import { FileNameDialogComponent } from '../file-name-dialog/file-name-dialog.component';
import { VariablesGlobales } from 'src/app/_models/variablesGlobales';
import { AccessComponent } from '../access.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'] 
})
export class HeaderComponent implements OnInit {

  currentUser: UserInfo;
  ValUser: string;
  MyFund: any;
  account_type: any;

  @Input() CurrentBalance;
  fileNameDialogRef: MatDialogRef<FileNameDialogComponent>;

  constructor ( private route: ActivatedRoute,
                private router: Router,
                private authenticationService: AuthenticationService,
                private myWalletService: WalletService,
                private matDialog: MatDialog,
                private dialog: MatDialog,
                public param: VariablesGlobales,
                public acc: AccessComponent) {

    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

    this.ValUser=this.currentUser.user._id;
    this.account_type = this.currentUser.user.fonctionName;

   }



  ngOnInit() {
      
        this.myWalletService.getMyWallet(this.ValUser).pipe(first()).subscribe(funds => { 
                this.MyFund = funds; 
                this.myWalletService.setBalance(this.MyFund.data[0].amount);
         });

        this.CurrentBalance = this.myWalletService.getCurrentBalData();
      //   this.param.parametre = this.currentUser.user.fonctionName;
   }

  logoutApp(): void {
        console.log("Logout"); 
        this.authenticationService.logout();
  }

  GotoAskUrl(): void {
        this.router.navigate(['/access/askHelp-step1']);
  }

  GotoHome(): void{
        this.router.navigate(['/home']);
  }
  GotoEx(){
        if(this.account_type=='Ask for Helps'){
            this.router.navigate(['/access/askHelp-step1']);
        }
        else if(this.account_type=='Offer Helps'){
            this.router.navigate(['/access/helpBanks']);
        }
  }
  
  GotoSwitch(): void {
        console.log('rrrrrrrrrrrrr'+this.currentUser.user.experience);
      if(this.currentUser.user.experience=='Beginner' || this.currentUser.user.experience=='Intermediate' || this.currentUser.user.experience=='Advanced'){
            if(this.param.parametre=='Ask for Helps'){
                  this.param.parametre='Offer Helps'
                  console.log("yyyyyyyeeeeeeeessssss : "+this.param.parametre);
            }
            else if(this.param.parametre=='Offer Helps'){
                  this.param.parametre='Ask for Helps'
                  console.log("nnnnnnnnnnnooooooooo : "+this.param.parametre);
            }
            this.router.navigate(['/access']);
      }   
      else{            
            // this.param.parametre='Offer Helps'; 
            this.router.navigate(['/access/switchAccount']);
      }   
      
  }

  GotoRecentsHelps(): void {
        this.router.navigate(['/access/helpBanks']);
  }
  openAddFileDialog() {
      this.fileNameDialogRef = this.dialog.open(FileNameDialogComponent, {
            hasBackdrop: false
      });
  }

}
