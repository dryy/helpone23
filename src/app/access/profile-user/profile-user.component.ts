import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProfileService, LanguageService, ServiceCatService } from 'src/app/_services';
import { UserInfo, ProfileData } from 'src/app/_models';
import { first } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

declare var jQuery:any;
declare var $ :any;

@Component({
selector: 'app-profile-user',
templateUrl: './profile-user.component.html',
styleUrls: ['./profile-user.component.scss']
})
export class ProfileUserComponent implements OnInit {

    saveInfo: FormGroup;
    formEditable = false;
    submittedFormLang = false;
    MyInfo: any;
    dataLangTab = [];
    dataSkillsTab = [];
    ValUser: string;
    currentUser: UserInfo;
    LanguageData: any[] = [];
    @Input() profileData: ProfileData;
    ShowLangEditForm : boolean = false;
    ShowLangForm : boolean = true;
    saveUserLangForm: FormGroup;
    dropdownList = [];
    selectedItems = [];
    dropdownSettings = {};
    TableData: any[] = [];
    chosenOption:any;
    third_col:boolean= false;
    categs: any;
    sec_col:boolean= false;
    subcats_data: any;
    chosenOptionTitle:any;
    constructor(private formLog: FormBuilder,
                private route: ActivatedRoute,
                private SetLangRole: LanguageService,
                private SetServiceCat: ServiceCatService,
                private profileServ: ProfileService,) {
                    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
                    this.ValUser=this.currentUser.user._id;
                }

    ngOnInit() {
        this.profileData = this.profileServ.getUserProfileData();
        this.getProfileInfoFunc();
        // To initialise Language Input Select
        this.dropdownSettings = {
        singleSelection: false,
        idField: 'nom_langue',
        textField: 'nom_langue',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        maxHeight:400,
        itemsShowLimit: 4,
        allowSearchFilter: true
        };
        // To initialize RechargeForm
        this.saveInfo = this.formLog.group({
            'username':['', Validators.required],
            'firstname':['', Validators.required],
            'surname':['', Validators.required],
            'description':['', Validators.required],
            'phone':['', Validators.required],
            'email':['', Validators.compose([Validators.required,Validators.email])]
        });
        // To Initialise User Lang Form
        this.saveUserLangForm = this.formLog.group({
            'langues':['', Validators.required]
        });
        this.saveInfo.disable();
        this.getLanguages();
        this.getCategories();
    }

    ListSelectedItems(event,list:number) {
        if (event.target.checked) {
            if (this.TableData.indexOf((event.target.name)) < 0 && this.TableData.length < list ) {
                    this.TableData.push(({'souscategory_id':event.target.value,'souscategoryName':event.target.name}));
            }

        } else {
            for(let i = 0; i < this.TableData.length; i++){
                if(this.TableData[i].souscategory_id == event.target.value) {
                    this.TableData.splice(i, 1);
                    i--;
                }
            }
        }
        this.third_col = true;

        let SelectedItems = this.TableData.length;
        this.chosenOption = SelectedItems+' / '+list+' selected';

    }


    get f() { return this.saveInfo.controls; }
    get flang() { return this.saveUserLangForm.controls; }

    onSubmit(){
        let profileUser = {
            "username": this.f.username.value,
            "description": this.f.description.value,
            "firstname": this.f.firstname.value,
            "surname": this.f.surname.value,
            "phone": this.f.phone.value,
            "email": this.f.username.value
        }
        this.profileServ.setValue(profileUser);
        console.log(this.profileData);
        this.profileServ.updateProfileInfo(this.profileData, this.ValUser).pipe(first()).subscribe(
            //on success
            (data : any) => {
                console.log(data);
                alert(data.message);
            },
            //on error
            error => {
                    alert(error);
                    this.saveInfo.disable();
            },
                //on complete
            () => {
                this.getProfileInfoFunc();
                
            });
    }

    
    // when user clicks the 'submit' link
    SubmitUpdateLangForm() {
        this.submittedFormLang = true;
        // stop here if form is invalid
        if (this.saveUserLangForm.invalid) {
            return;
        }
        let stringToSplit = this.flang.langues.value.toString().split(",");
        var dataLangTabArray = [];
        for(var i=0; i< this.flang.langues.value.length; i++)  {
            dataLangTabArray.push(this.flang.langues.value[i].nom_langue);
            console.log("languesss    ", dataLangTabArray);        
        }
        this.profileServ.SetLang(dataLangTabArray);
        this.profileServ.updateProfileInfo(this.profileData,this.ValUser).pipe(first()).subscribe(
        //on success
        (data : any) => {
            console.log(data.message);
            this.submittedFormLang = false;
        },
        //on error
        error => {
            console.log("error");
        },
            //on complete
        () => {
            this.cancelLangForm();
            this.getProfileInfoFunc();
        });
    }

    // when user clicks the 'cancel' link
    cancelLangForm(){
        this.ShowLangEditForm = false;
        this.ShowLangForm = true;
    }

    getProfileInfoFunc(){
        this.profileServ.getProfileInfo(this.route.snapshot.params.id).pipe(first()).subscribe(UserInfoValues => {
            this.MyInfo = UserInfoValues;
            this.MyInfo = this.MyInfo;
            console.log("langue;;;;;;;; ", this.MyInfo.data);
            this.editF(this.MyInfo.data[0])
            for(var i=0; i< this.MyInfo.data[2].length; i++)  {
                this.dataLangTab.push(this.MyInfo.data[2][i].langue);
            }
            this.profileServ.setUserProfileData(this.MyInfo.data[0],this.MyInfo.data[1],this.dataLangTab);
        });
    }

    editF(data: any){
        this.saveInfo.patchValue({"username": data.username });
        this.saveInfo.patchValue({"firstname": data.firstname });
        this.saveInfo.patchValue({"surname": data.surname });
        this.saveInfo.patchValue({"description": data.personal_description });
        this.saveInfo.patchValue({"phone": data.phone });
        this.saveInfo.patchValue({"email": data.email });
    }

    editForm(){
        this.saveInfo.enable();
        this.formEditable = !this.formEditable;
    }
    cancelEditForm(){
        this.saveInfo.disable();
        this.formEditable = !this.formEditable;
    }

    getLanguages(){
        this.SetLangRole.getAll().pipe(first()).subscribe((lang: any)=> {
                this.LanguageData = lang.data;
                console.log("laaaaaaaaagues : ", this.LanguageData);
            });
    }

    editLangForm(email:string){
        this.ShowLangEditForm = true;
        this.ShowLangForm = false;
        //this.saveUserEmailForm.patchValue({"email": email });
    }

    getCategories(){
        this.SetServiceCat.getAll().pipe(first()).subscribe(catsData => {
            this.categs = catsData;
        });
    }

    loadSubCat(value:string,name:string) {
        this.SetServiceCat.getSubCatbyCatID(value).pipe(first()).subscribe(subcats => {
            this.subcats_data = subcats;
        });
        this.chosenOptionTitle = name;
        this.sec_col = true;
    }

    onSave() {
        this.profileServ.SetSousCateg(this.TableData);
          console.log(this.TableData);
        this.profileServ.updateProfileInfo(this.profileData,this.ValUser).pipe(first()).subscribe(
            //on success
            (data : any) => {
                    alert(data.message);
                    $('.modal').modal('hide');
                    this.sec_col = false;
                    this.third_col= false;
            },
            //on error
            error => {
                    alert(error);
            },
                //on complete
            () => {
    
                this.getProfileInfoFunc();
                this.getCategories();
            });
    }

}
