import { Component, OnInit } from '@angular/core';
import { ServiceListingService } from 'src/app/_services';
import { first } from 'rxjs/operators';

export class HelpClassComponent {
    columnDefs : any;
    RecentsHelpReq: any;
    private serviceList1: ServiceListingService;
  
    constructor(serviceList: ServiceListingService) {
        this.serviceList1 = serviceList;
    }


    getRecentHelpsRequestsListing(columnDefs: any, ValUser: any, url:any) {
        this.serviceList1.getRecentHelpsRequest(ValUser, url).pipe(first()).subscribe(RecentsHelpReqData => {          
            this.RecentsHelpReq = RecentsHelpReqData;
            this.RecentsHelpReq = this.RecentsHelpReq.data;
            this.columnDefs = columnDefs;            
            console.log(this.RecentsHelpReq);
        });
    }

}
