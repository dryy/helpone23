import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SwitchAccountComponent } from './switch-account.component';

const routes: Routes = [

	{
        path: '', component: SwitchAccountComponent
    }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SwitchAccountRoutingModule { }
