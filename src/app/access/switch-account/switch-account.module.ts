import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms'; 

import { SwitchAccountRoutingModule } from './switch-account-routing.module';
import { SwitchAccountComponent } from './switch-account.component';

@NgModule({
  imports: [
    CommonModule,
    SwitchAccountRoutingModule,
    FormsModule,ReactiveFormsModule
  ],
  declarations: [SwitchAccountComponent]
})
export class SwitchAccountModule { }
