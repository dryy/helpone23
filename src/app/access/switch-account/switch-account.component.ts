import { Component, OnInit, Input } from '@angular/core';

// Must import to use Forms functionality  
import { FormBuilder, FormGroup, Validators ,FormsModule,NgForm,FormControl,FormArray } from '@angular/forms'; 
import { first } from 'rxjs/operators'; 
import { Router,ActivatedRoute } from '@angular/router';
import { AlertService, RegisterDataService,ServiceCatService, UserService, AuthenticationService, ProfileService} from '../../_services';
import { UserInfo } from 'src/app/_models/user';
import { ProfileData } from 'src/app/_models';
import { VariablesGlobales } from 'src/app/_models/variablesGlobales';


@Component({
  selector: 'app-register-step4',
  templateUrl: './switch-account.component.html',
  styleUrls: ['./switch-account.component.scss']
})
export class SwitchAccountComponent implements OnInit {
  
  cats:any;
  subcats_data:any;
  chosenOption:any;
  chosenOptionTitle:any;
  listChosen:any;

  bntText: string ='Skip';

  sec_col:boolean= false;
  third_col:boolean= false;
  
  stateChecked:string='';
  
  TableData: any[] = [];
  @Input() profileData: ProfileData;

  valueOptChose:string;
  currentUser: UserInfo;
  ValUser: string;
  HideFinishElement:boolean= false;
    state1:boolean= true;
    state2:boolean= true;
    state3:boolean= true;
    experience: string;
  conectionError: string;
  loading: boolean;

  MyInfo: any;
  dataLangTab = [];
  dataSkillsTab = [];

constructor(  private formLog: FormBuilder,private route: ActivatedRoute,
          private router: Router,private alertService: AlertService,
          private SetServiceCat: ServiceCatService,
          private userService: UserService,
          public param: VariablesGlobales,
          private regDataService: RegisterDataService,
          private profileServ: ProfileService,
          private authenticationService: AuthenticationService) { 
          
              // override the route reuse strategy
            this.router.routeReuseStrategy.shouldReuseRoute = function() {
              return false;
            };
            this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
            this.ValUser=this.currentUser.user._id;
            
          }

ngOnInit() {

  this.profileData = this.profileServ.getUserProfileData();
  this.valueOptChose = localStorage.getItem("optChooseText");
  this.getProfileInfoFunc();

  this.SetServiceCat.getAll().pipe(first()).subscribe(catsData => { 
    this.cats = catsData; 
  });


}


loadSubCat(value:string,name:string){
  
  this.SetServiceCat.getSubCatbyCatID(value).pipe(first()).subscribe(subcats => { 
    this.subcats_data = subcats;
   }); 
   this.chosenOptionTitle = name;
   this.sec_col = true;
}

logTextFunction1(value: string): void {
  this.experience = value;
  this.HideFinishElement = true; 
  this.state2=true; 
  this.state3=true;
  this.state1 = !this.state1; 
}

logTextFunction2(value: string): void {
  this.experience = value;
  this.HideFinishElement = true; 
  this.state1=true; 
  this.state3=true;
  this.state2 = !this.state2; 
}

logTextFunction3(value: string): void {
  this.experience = value;
  this.HideFinishElement = true; 
  this.state1=true; 
  this.state2=true;
  this.state3 = !this.state3; 
}


 ListSelectedItems(event,list:number) {
    if (event.target.checked) {
        if (this.TableData.indexOf((event.target.name)) < 0 && this.TableData.length < list ) { 
                this.TableData.push(({'sousCategorie_id':event.target.value,'sousCategoryName':event.target.name}));

        }

    } else {
        
       //let index = this.TableData.findIndex(d => d.sousCategorie_id === event.target.value); //find index in your array
       //this.TableData.splice(index, 1);//remove element from array

	        for(let i = 0; i < this.TableData.length; i++){
		        if(this.TableData[i].sousCategorie_id == event.target.value) {
		            this.TableData.splice(i, 1);
		            i--;
		        }
	       }
    }
    this.third_col = true;

    let SelectedItems = this.TableData.length;
    this.chosenOption = SelectedItems+' / '+list+' selected';

	  if (SelectedItems > 0 ){
	  	this.bntText='GO';
	  }
	  else {
	  	this.bntText='Skip';
	  }

}


  onNext() {
    //  this.regDataService.SetSousCateg(this.TableData); 
    console.log(this.experience);
    this.loading = true;
        this.userService.switchForm(this.ValUser, this.experience, this.TableData) 
            .pipe(first())
            .subscribe(
                (data : any) => { 
                   this.alertService.success(data.message, true);
                       console.log(data);
                       console.log(data.user);
                       this.currentUser.user = data.user;
                        localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
                        this.param.parametre='Offer Helps'; 
                        this.router.navigate(['/access']);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                }); 
  }

  //Permet de get les infos d'un user
  getProfileInfoFunc(){
    this.profileServ.getProfileInfo(this.ValUser).pipe(first()).subscribe(UserInfoValues => {

          this.MyInfo = UserInfoValues;
          this.dataLangTab = [];
          this.dataSkillsTab = [];

          for(var i=0; i< this.MyInfo.data[1].length; i++)  {
              this.dataSkillsTab.push({sousCategorie_id:this.MyInfo.data[1][i].souscategory_id,sousCategoryName:this.MyInfo.data[1][i].souscategoryName});
          }

          for(var i=0; i< this.MyInfo.data[2].length; i++)  {
              this.dataLangTab.push({langue:this.MyInfo.data[2][i].langue});
          }

          this.profileServ.setUserProfileData(this.MyInfo.data[0],this.dataSkillsTab,this.dataLangTab);
    });
  } 

  //permet de définir un compter comme offer helps
  // onSave(){
  //   localStorage.removeItem('optChooseText');   
  //   this.profileServ.SetFonctionName('Offer Helps');
  //   this.profileServ.updateProfileInfo(this.profileData,this.ValUser).pipe(first()).subscribe(
  //     //on success 
  //     (data : any) => {
  //          console.log(data); 
          
  //          this.onNext();
           
  //     },
  //     //on error
  //     error => {
  //          console.log(error);
  //     },
  //      //on complete
  //     () => {
  //       // this.router.navigate(['/access']);
  //     });
  //  }


  // méthode pour mettre à jour les information concernant le domaine de compétence de l'utilisateur et en lui donnant pour la première fois une fonctionName qui est "offer helps"
   onSave(value: number){
    localStorage.removeItem('optChooseText');
    this.profileServ.SetFonctionName('Offer Helps');
    this.profileServ.SetExperience(this.experience);
    this.profileServ.SetSousCategories(this.TableData);
    console.log("récapitulationnnnnnnnnnnnn   :  "+ this.profileServ);
    this.profileServ.updateProfileInfo(this.profileData,this.ValUser).pipe(first()).subscribe(
      //on success
      (data : any) => {
           console.log(data.message); 
           console.log(data.data[0]); 
           this.currentUser.user = data.data[0];
           localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
           this.param.parametre='Offer Helps';
           this.router.navigate(['/access']);
      },
      //on error
      error => {
           console.log(error);
      },
       //on complete
      () => {
        this.router.navigate(['/access']);
      });
   }

  //  permettre à un utilisateur ask for help de devenir offer help en mettant à jour ses compétences
   becomeOffer(value: number){
    localStorage.removeItem('optChooseText');
    this.profileServ.SetExperience(this.experience);
    this.profileServ.SetSousCategories(this.TableData);
    console.log("récapitulationnnnnnnnnnnnn   :  "+ this.profileServ);
    this.profileServ.updateProfileInfo(this.profileData,this.ValUser).pipe(first()).subscribe(
      //on success
      (data : any) => {
           console.log(data.message); 
           console.log(data.data[0]); 
           this.currentUser.user = data.data[0];
           localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
           this.param.parametre='Offer Helps';
           this.router.navigate(['/access']);
      },
      //on error
      error => {
           console.log(error);
      },
       //on complete
      () => {
        this.router.navigate(['/access']);
      });
   }

  onPrevious() {
     localStorage.removeItem('optChooseText');
     this.regDataService.ResetSousCateg();  
     this.router.navigate(['access/dashboard']);
  }


}    