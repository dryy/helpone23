import { ProfileUserComponent } from './profile-user/profile-user.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccessComponent } from './access.component';
import { WorkflowGuard }        from '../_services/workflow-guard.service';

const routes: Routes = [

   {   

        path: '',
        component: AccessComponent,
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
            { path: 'dashboard', loadChildren: './dash/dash.module#DashModule' },
            { path: 'inbox', loadChildren: './inbox/inbox.module#InboxModule' },
            { path: 'notifications', loadChildren: './notif/notif.module#NotifModule' },
            { path: 'profile', loadChildren: './profile/profile.module#ProfileModule' },
            { path: 'profile-user', component: ProfileUserComponent },
            { path: 'myOpenHelps', loadChildren: './myopenhelps/myopenhelps.module#MyopenhelpsModule' },
            { path: 'myHelps', loadChildren: './myhelprequests/myhelprequests.module#MyhelprequestsModule' },
            { path: 'helpBanks', loadChildren: './helpbanks/helpbanks.module#HelpbanksModule' },
            { path: 'completedHelps', loadChildren: './helpcompleted/helpcompleted.module#HelpcompletedModule' },
            { path: 'placeBid/:id', loadChildren: './placebidservice/placebidservice.module#PlacebidserviceModule' },
            { path: 'viewBidder/:id', loadChildren: './viewbidderservice/viewbidderservice.module#ViewbidderserviceModule' },
            { path: 'completeBid/:id', loadChildren: './completebid/completebid.module#CompletebidModule' },

            { path: 'tab', loadChildren: './table/table.module#TableModule' },
            { path: 'switchAccount', loadChildren: './switch-account/switch-account.module#SwitchAccountModule' },
            { path: 'contact', loadChildren: './contact/contact.module#ContactModule' },

            { path: 'askHelp-step1',loadChildren: './ask-help-step1/ask-help-step1.module#AskHelpStep1Module' },
            { path: 'askHelp-step2',canActivate: [WorkflowGuard], loadChildren: './ask-help-step2/ask-help-step2.module#AskHelpStep2Module' }
            
        ]
    }





];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [WorkflowGuard] 
})
export class AccessRoutingModule { }
