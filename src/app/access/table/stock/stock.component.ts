import { Component, Input, EventEmitter, Output, OnChanges } from '@angular/core';
@Component({

    selector: 'app-stock',
    templateUrl: './stock.component.html',
    styleUrls: ['./stock.component.scss']
})

export class StockComponent implements OnChanges {

    @Input() stock: number;

    @Input() productId: number;

    @Output() stockValueChange = new EventEmitter();

    color = '';

    updatedstockvalue: number;

    stockValueChanged() {

        this.stockValueChange.emit({ id: this.productId, updatdstockvalue: this.updatedstockvalue });

        this.updatedstockvalue = null;

    }

    ngOnChanges() {

        if (this.stock > 10) {

            this.color = 'green';

        } else {

            this.color = 'red';

        }

    }

}