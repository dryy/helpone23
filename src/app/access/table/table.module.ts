import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { TableRoutingModule } from './table-routing.module';
import { TableComponent } from './table.component';
import { StockComponent } from './stock/stock.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TableRoutingModule
  ],
  declarations: [TableComponent, StockComponent]
})
export class TableModule { }
