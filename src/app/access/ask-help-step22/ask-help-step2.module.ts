import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms'; 

import { AskHelpStep2RoutingModule } from './ask-help-step2-routing.module';
import { AskHelpStep2Component } from './ask-help-step2.component';
import { NgKnifeModule } from 'ng-knife';

import { AlertModule } from './../../_directives/alert.module';
import { DeleteAdressComponent } from './delete-adress/delete-adress.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgKnifeModule,
    AlertModule,
    AskHelpStep2RoutingModule
  ],
  declarations: [AskHelpStep2Component, DeleteAdressComponent]
})
export class AskHelpStep2Module { }
