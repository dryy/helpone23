import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AskHelpStep2Component } from './ask-help-step2.component';

describe('AskHelpStep2Component', () => {
  let component: AskHelpStep2Component;
  let fixture: ComponentFixture<AskHelpStep2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AskHelpStep2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AskHelpStep2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
