import { Component, Input, Output, EventEmitter } from '@angular/core';

// Must import to use Forms functionality  
import { first } from 'rxjs/operators'; 

import { AddressService,AlertService} from './../../../_services';

@Component({
  selector: 'app-delete-adress',
  templateUrl: './delete-adress.component.html',
  styleUrls: ['./delete-adress.component.scss']
})
export class DeleteAdressComponent  {
/*
        @Output will tell the parent component (AskHelpStep2Component)
        that an event has happened (via .emit(), see readAddressContent() method below)
    */
    @Output() show_address_list_event = new EventEmitter();
 
    // @Input enable getting the product_id from parent component (AskHelpStep2Component)
    @Input() adress_id : string;
    @Input() address : string;
     @Input() addressTitle : string;
    @Input() ZipCode : string;
 
    // initialize product service
    constructor(private adressService: AddressService,private alertService: AlertService){}
 
    // user clicks 'yes' button
    delete(){
 
        // delete data from database

         this.adressService.delete(this.adress_id).pipe(first()).subscribe(

           //on success 
           (data : any) => { 
                this.alertService.success(data.message, true);
           },

           //on error
           error => {
                this.alertService.error(error);
           },

            //on complete
           () => { 
                this.readAddressContent();
           });
       }
 
    // user clicks the 'read products' button
    readAddressContent(){
        this.show_address_list_event.emit({ title: "Read Addresses" });
    }
}
