import { Component, OnInit,Input,Output,EventEmitter,ViewChild } from '@angular/core';

// Must import to use Forms functionality  
import { FormBuilder, FormGroup, Validators ,FormsModule,NgForm } from '@angular/forms'; 
import { first } from 'rxjs/operators'; 
import { Router,ActivatedRoute } from '@angular/router';

import { AskhelpService,AlertService,AddressService,ServicetransactionService,WalletService,ProfileService } from '../../_services';

import { ASKStep2,AskData }   from '../../_models/askHelp';

import { Address,UserInfo } from './../../_models';

@Component({
  selector: 'app-ask-help-step2',
  templateUrl: './ask-help-step2.component.html',
  styleUrls: ['./ask-help-step2.component.scss']
})
export class AskHelpStep2Component implements OnInit {

    currentUser: UserInfo;
    ValUser: string;
    MyFund: any;
    MyInfo: any;
    MyStats: any;
	
	  askForm2: FormGroup;
    submitted = false;
    fstep: ASKStep2;
    priceBorder :boolean = false;
    checkField :boolean = false;

    AdressData: any;
    ShowUpdateForm = [];

    checkIfDeliversAreSelected = [];
    checkIfBillsAreSelected = [];

    ShowContentAddress : boolean[]=[];
    show_delete_content=false;
    show_address_list=true;

    adress_title_value:string;
    adress_value:string;
    zip_code_value:string;
    IdAddress:string;
    amountChecked:number=0;

    saveAdressFirstForm: FormGroup;
    saveAdressSecForm: FormGroup;
    submittedFirstForm = false;
    submittedSecForm = false;

    TableData: any[] = [];
    TableDataModified: any[] = [];

     @ViewChild('myForm', {static: false}) formValues; // Added this


    @Input() askData: AskData;
    isFormValid: boolean = false;

  constructor(  private formLog: FormBuilder,private route: ActivatedRoute,
		        private router: Router, private askDataService: AskhelpService,
            private addressService: AddressService,
            private ServiceTransService: ServicetransactionService,
            private profileServ: ProfileService,
            private alertService: AlertService,private myWalletService: WalletService) { 

             // override the route reuse strategy
            this.router.routeReuseStrategy.shouldReuseRoute = function() {
                return false;
            };

            this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

            this.ValUser=this.currentUser.user._id;

            this.myWalletService.getMyWallet(this.ValUser).pipe(first()).subscribe(funds => { 
                        this.MyFund = funds; 
                    });
            
            }


   ngOnInit() {

  		// To initialize FormGroup  
    this.askForm2 = this.formLog.group({    
      'IdUser':[this.ValUser, Validators.required],
      'min_price':['', Validators.required],
      'max_price':['', Validators.required],
    }); 

      // To initialize FormGroup  
    this.saveAdressFirstForm = this.formLog.group({    
      'user_id':[this.ValUser, Validators.required],
      'address_title':['', Validators.required],
      'address':['', Validators.required],
      'zip_code_postal':['', Validators.required],
      'telephone':['', Validators.required],
      'city':['', Validators.required],
      'email':['', Validators.compose([Validators.required,Validators.email])]
    }); 

    this.saveAdressSecForm = this.formLog.group({    
      'user_id':[this.ValUser, Validators.required],
      '_id':['', Validators.required],
      'address_title':['', Validators.required],
      'address':['', Validators.required],
      'zip_code_postal':['', Validators.required],
      'telephone':['', Validators.required],
      'city':['', Validators.required],
      'email':['', Validators.compose([Validators.required,Validators.email])]
    }); 

     this.loadAddresses();
     this.getProfileInfoData();
     this.getUserStatistics();


     this.askData = this.askDataService.getAskData();
     this.isFormValid = this.askDataService.isFormAskValid();

  }

    private getProfileInfoData() {
      this.profileServ.getProfileInfo(this.ValUser).pipe(first()).subscribe(UserInfoValues => {          
            this.MyInfo = UserInfoValues; 
      });
  }

   private getUserStatistics() {
    this.profileServ.getUserStats(this.ValUser).pipe(first()).subscribe(UserStatsValues => {          
            this.MyStats = UserStatsValues; 
      });
  }

    private loadAddresses() {
        this.addressService.getByUserID(this.ValUser).pipe(first()).subscribe(dataAdress => { 
            this.AdressData = dataAdress; 
             
             for(let t = 0; t < this.AdressData.data.length; t++) {
               this.ShowContentAddress[t] = true;
             } 
        });

    }


          checkSelected(e, id,ArrayLength,address,telephone,address_title,zip_code_postal,city,email) {

            this.checkIfDeliversAreSelected[id] = false;
            //this.checkIfBillsAreSelected[id] = true;
            
            if(e.target.checked) {

            if (this.TableData.indexOf((e.target.name)) < 0 ) { 
                this.TableData.push(({ 'name':e.target.name,
                                       'addressUser_id':e.target.value,
                                       'address_title':address_title,
                                       'address':address,
                                       'zip_code_postal':zip_code_postal,
                                       'telephone':telephone,
                                       'city':city,
                                       'email':email,
                                       'deliver_help_to_this_address':1,
                                       'bill_to_this_address':0}));
            }

              for(let i = 0; i < ArrayLength; i++) {
                if (i !== id) {
                  this.checkIfDeliversAreSelected[i] = true;
                }
                else {
                  this.checkIfDeliversAreSelected[i] = false;
                }
                
              }  
            }
            else{              
                 
                 for(let i = 0; i < ArrayLength; i++) {
                    this.checkIfDeliversAreSelected[i] = false;
                } 

                for(let j = 0; j < this.TableData.length; j++){
                  if(this.TableData[j].address_title == e.target.name) {
                      this.TableData.splice(j, 1);
                      j--;
                  }
               }
            }

          //  console.log('TABCHOSEN :'+JSON.stringify(this.TableData));
            
         }

         checkBillSelected(e, id,ArrayLength,address,telephone,address_title,zip_code_postal,city,email) {

            this.checkIfBillsAreSelected[id] = false;

            if(e.target.checked) {

            if (this.TableData.indexOf((e.target.name)) < 0 ) { 
                this.TableData.push(({ 'name':e.target.name,
                                       'addressUser_id':e.target.value,
                                       'address_title':address_title,
                                       'address':address,
                                       'zip_code_postal':zip_code_postal,
                                       'telephone':telephone,
                                       'city':city,
                                       'email':email,
                                       'deliver_help_to_this_address':0,
                                       'bill_to_this_address':1}));
            }

              for(let i = 0; i < ArrayLength; i++) {
                if (i !== id) {
                  this.checkIfBillsAreSelected[i] = true;
                }
                else {
                  this.checkIfBillsAreSelected[i] = false;
                }
                
              }  
            }
            else{              
                 
                 for(let i = 0; i < ArrayLength; i++) {
                    this.checkIfBillsAreSelected[i] = false;
                } 

                for(let j = 0; j < this.TableData.length; j++){
                  if(this.TableData[j].address_title == e.target.name) {
                      this.TableData.splice(j, 1);
                      j--;
                  }
               }
            }

           // console.log('TABCHOSEN :'+JSON.stringify(this.TableData));
            
         }


  	onPrevious() {

       this.router.navigate(['/access/askHelp-step1']);
    }

      // show addresses list
    showListAddress($event){
        this.loadAddresses();
        this.show_delete_content=false;
        this.show_address_list=true;
    }

    // convenience getter for easy access to form fields
      get f() { return this.askForm2.controls; }
      get g() { return this.saveAdressFirstForm.controls; }
      get h() { return this.saveAdressSecForm.controls; }

      // Executed When Form Is Submitted  
    SubmitForm() {
       
        this.submittedFirstForm = true;

        // stop here if form is invalid
        if (this.saveAdressFirstForm.invalid) {
            return;
        }

       this.addressService.save(this.saveAdressFirstForm.value)  
      .subscribe(
       //on success 
       (data : any) => { 
            this.alertService.success(data.message, true);
            this.submittedFirstForm = false;
            this.saveAdressFirstForm.patchValue({
            "address_title": '',"city": '',"telephone": '',
            "email": '',"address": '',"zip_code_postal": '' 
             });
            
            
       },
       //on error
       error => {
           this.alertService.error(error);
       },
        //on complete
       () => { 
            this.loadAddresses(); 
       });


      }

       // when user clicks the 'delete' button
deleteAddress(id:string,address:string,address_title:string,zip_code_postal:string) {

    this.adress_title_value=address_title;
    this.adress_value=address;
    this.zip_code_value=zip_code_postal;
    this.IdAddress=id;

    this.show_delete_content=true;
    this.show_address_list=false;
}

        // when user clicks the 'edit' link
      editAddress(id:string,address:string,telephone:string,address_title:string,zip_code_postal:string,city:string,email:string,index: number){
 
    this.ShowUpdateForm[index] = true;
    for(let i = 0; i < this.ShowUpdateForm.length; i++) {
      if (i !== index) {
        this.ShowUpdateForm[i] = false;
      }
    }  

    this.ShowContentAddress[index] = false;
    for(let i = 0; i < this.ShowContentAddress.length; i++) {
      if (i !== index) {
        this.ShowContentAddress[i] = true;
      }
    } 
    
    this.saveAdressSecForm.patchValue({
    "_id": id,"address_title": address_title,"city": city,"telephone": telephone,
    "email": email,"address": address,"zip_code_postal": zip_code_postal 
     });    
    
}

  // when user clicks the 'cancel' link
cancelUpdate(index: number){
  this.ShowUpdateForm[index] = false;
  this.ShowContentAddress[index] = true;
}

// when user clicks the 'update' link
SubmitUpdate(index: number) {
       
        this.submittedSecForm = true;

        // stop here if form is invalid
        if (this.saveAdressSecForm.invalid) {
            return;
        }

       this.addressService.update(this.saveAdressSecForm.value).pipe(first()).subscribe(

           //on success 
           (data : any) => { 
                this.alertService.success(data.message, true);
                this.submittedSecForm = false;
                this.cancelUpdate(index);
                
           },

           //on error
           error => {
                this.alertService.error(error);
           },

            //on complete
           () => { 
                
                this.loadAddresses(); 
           }); 


      }
	  
	  // Executed When Form Is Submitted  
	  onSubmit() {
       
       	this.submitted = true;

        // stop here if form is invalid
        if (this.askForm2.invalid) {
            return;
        }

        // stop if price mix is greather than price max
        if( (+this.f.min_price.value) >= (+this.f.max_price.value ) ) {
          this.priceBorder = true;
         return;
        }

        else {
          this.priceBorder = false;
        }

        // Control if Deliver and Bill Adresses were checked
        if( this.TableData.length !== 2 ) {
          this.checkField = true;
         return;
        }

        else {
          this.checkField = false;
        }

        this.TableDataModified=this.TableData;

        //remove property name on this array
        for (var i = 0; i < this.TableDataModified.length; i++) {
            delete this.TableDataModified[i].name;
        }

        this.askDataService.setSecStep(this.askForm2.value); 

        this.askDataService.SetAdressesInfo(this.TableDataModified); 

        this.askDataService.SetUserID(this.f.IdUser.value); 
       

         this.ServiceTransService.save(this.askData)
         .pipe(first())
         .subscribe(

           //on success 
          (dataGet : any) => { 
               this.askData = this.askDataService.resetAskFormData();
               this.isFormValid = false;
              this.alertService.success(dataGet.message, true);
               
          },

          //on error
          error => {
               this.alertService.error(error);
          },

           //on complete
          () => { 
            this.router.navigate(['/access/askHelp-step1'], { queryParams: { state: 'true' } });
          });


      }

}
