import { Component, OnInit,Input } from '@angular/core';

// Must import to use Forms functionality  
import { FormBuilder, FormGroup, Validators ,FormsModule,NgForm } from '@angular/forms'; 
import { first,mergeMap  } from 'rxjs/operators'; 
import { Observable, of, interval } from 'rxjs';
import { Router,ActivatedRoute } from '@angular/router';
import { UserInfo } from '../../_models';

import { WalletService,AlertService,ProfileService,ServiceListingService,ServiceCatService,BidService } from '../../_services';

import * as moment from 'moment';
import { NotificationsService } from 'src/app/_services/notifications.service';
import { Notifications } from 'src/app/_models/notifications';
import { VariablesGlobales } from 'src/app/_models/variablesGlobales';

function myFunction() {
  alert("Hello! bid completed");
}

@Component({
  selector: 'app-placebidservice',
  templateUrl: './placebidservice.component.html',
  styleUrls: ['./placebidservice.component.scss']
})
export class PlacebidserviceComponent implements OnInit {

  currentUser: UserInfo;
  notif: Notifications;
  ValUser: string;
  MyInfo: any;
  mySkills: any;
  MyStats: any;
  p: number = 1;
  textDescription: any;

  submittedForm1 = false;
  bidCol:boolean=false;
  textPriceBid:boolean=false;

  services_categ: any;
  ServiceBidInfos: any;
  ServiceBidUser: any;
  ServicePicInfos: any;
  ServiceTimeInfos: any;
  BiddersInfo: any;
  regForm: FormGroup;
  bidOnIt: boolean = true;
  verifie: boolean = true;

  saveBidForm: FormGroup;

  constructor(  private route: ActivatedRoute,
                private router: Router,
                public param: VariablesGlobales,
                private formLog: FormBuilder,
                private alertService: AlertService,
                private SetServiceCat: ServiceCatService,
                private profileServ: ProfileService,
                private SetBid: BidService,
                private serviceList: ServiceListingService,
                private notificationsService: NotificationsService,
                private myWalletService: WalletService) { 

  this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

  this.ValUser=this.currentUser.user._id;

  }

  ngOnInit() {

    this.getAllCateg();
    this.getProfileInfoData();
    this.getServiceBidInfo();
    this.getAllBiddersInfo();

    // To initialize RechargeForm  
    this.saveBidForm = this.formLog.group({    
      'serviceAskForHelp_id':[this.route.snapshot.params.id, Validators.required],
      'user_id_celui_qui_offre_service':['', Validators.required],
      'price_bid':['', Validators.required],
      'date_livraison':['', Validators.required],
      'time_livraison':['', Validators.required],
      'user_id_celui_qui_bid':[this.ValUser, Validators.required],
      'description':['', Validators.required]
    }); 
    this.regForm = this.formLog.group({ 
      'messService':['', Validators.required]
    });

  }

   // convenience getter for easy access to form fields
    get F1() { return this.saveBidForm.controls; }
    get F2() { return this.regForm.controls; }

    getAllCateg() {
       this.SetServiceCat.getAll().pipe(first()).subscribe(serv_cats => { 
            this.services_categ = serv_cats; 
        });
    }

  getServiceBidInfo() {
     this.serviceList.getServiceInfos(this.route.snapshot.params.id).pipe(first()).subscribe(ServiceInfo => {          
            this.ServiceBidInfos = ServiceInfo; 
        
      this.profileServ.getProfileInfo(this.ServiceBidInfos.data.user_id).pipe(first()).subscribe(UserSV => {          
            this.ServiceBidUser = UserSV; 
          });

      });

   this.serviceList.getServicePictures(this.route.snapshot.params.id).pipe(first()).subscribe(ServiceV => {          
            this.ServicePicInfos = ServiceV; 
    });

   this.serviceList.getServiceTime(this.route.snapshot.params.id).pipe(first()).subscribe(ServiceV2 => {          
            this.ServiceTimeInfos = ServiceV2; 
      });

  }

  getAllBiddersInfo() {
      interval(60000)
      .pipe( mergeMap ( () => this.SetBid.getAllBidders(this.route.snapshot.params.id) ) )
      .subscribe( ServiceV3 => {          
            this.BiddersInfo = ServiceV3; 
      });
  }

  getCategoryName(idCategory:string,TabCat:any): string {
        
    let verified:boolean = false, i=0;
    let VarName="";
    if (idCategory && TabCat) {
      while (!verified && i < TabCat.length ) {

      if ( String(TabCat[i]._id) == idCategory){
          VarName=TabCat[i].categoryName;
          verified=true;
        }

        i++;
      }
    }
    return VarName;
  }

  getProfileInfoData() {
    this.profileServ.getProfileInfo(this.ValUser).pipe(first()).subscribe(UserInfoValues => {          
          this.MyInfo = UserInfoValues;
          console.log(this.MyInfo);
          this.mySkills = this.MyInfo.data[1]
          console.log(this.mySkills);
                      
    });
  }

  placeBid (value) {
    this.bidCol=true;
    this.saveBidForm.patchValue({"user_id_celui_qui_offre_service": value });
  }

  cancelBid () {
    this.bidCol=false;
  }

     // Executed When Form Is Submitted  
	  SaveBid(minVal,MaxVal) {       
       	this.submittedForm1 = true;
        // stop if price bid is not between price min and price max of service
        if( (this.F1.price_bid.value < minVal) || (this.F1.price_bid.value > MaxVal) ) {
           this.textPriceBid = true;
           return;
        }
        else{
          this.textPriceBid = false;
        }
        // stop here if form is invalid
        if (this.saveBidForm.invalid) {
            return;
        }
        let dateVal1 = moment(this.F1.date_livraison.value).format('YYYY-MM-DD');
        let timeVal1 = moment(this.F1.time_livraison.value).format('hh:mm A');
        let serviceAskForHelp_id = this.F1.serviceAskForHelp_id.value;
        let user_id_celui_qui_bid = this.F1.user_id_celui_qui_bid.value;
        let user_id_celui_qui_offre_service = this.F1.user_id_celui_qui_offre_service.value;
        let price_bid = this.F1.price_bid.value;
        let description = this.F1.description.value;
        let dataDT : any = { date_livraison:dateVal1,
                    time_livraison:timeVal1,
                    serviceAskForHelp_id:serviceAskForHelp_id,
                    user_id_celui_qui_bid:user_id_celui_qui_bid,
                    user_id_celui_qui_offre_service:user_id_celui_qui_offre_service,
                    price_bid:price_bid,
                    description:description };
        this.saveNotif(serviceAskForHelp_id);

        
         this.SetBid.saveBid(dataDT).pipe(first()).subscribe(

	           //on success 
	          (dataGet : any) => { 
	               this.saveBidForm.reset();
	               this.bidCol = false;
	               this.submittedForm1 = false;
	               this.alertService.success(dataGet.message, true);
	                
	          },

	          //on error
	          error => {
	               this.alertService.error(error);
	          },	      
	        //on complete
	         () => {
            this.getAllBiddersInfo();
	          this.getProfileInfoData();
	          this.getServiceBidInfo();
	         });
      }

  // Envoyer la notification
  saveNotif(serviceAskForHelp_id: any){
    let dataNotif : any = { serviceAskForHelp_id:serviceAskForHelp_id,
      content: this.currentUser.user.username +' has bidded on your asked help. click to check' };
      console.log(dataNotif)
      myFunction();
      this.notificationsService.save(dataNotif).pipe(first()).subscribe( 
        //on success 
        (notif : any) => {
          this.notif = notif.data; 
          console.log(this.notif); 
          console.log(notif.message);
            this.alertService.success(notif.message, true);              
        },
        //on error
        error => {
            this.alertService.error(error);
        },
    
      //on complete
      () => {
      });
  }
}
