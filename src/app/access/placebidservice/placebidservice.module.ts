import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OwlDateTimeModule, OwlNativeDateTimeModule,OWL_DATE_TIME_FORMATS  } from 'ng-pick-datetime';
import { NgKnifeModule } from 'ng-knife';
import { PlacebidserviceRoutingModule } from './placebidservice-routing.module';
import { PlacebidserviceComponent } from './placebidservice.component';
import { FormsModule,ReactiveFormsModule } from '@angular/forms'; 

import { AlertModule } from './../../_directives/alert.module';

import {NgxPaginationModule} from 'ngx-pagination'; 

import { SharedPipesModule } from './../../shared';

    // See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
export const MY_MOMENT_FORMATS = {
    parseInput: 'l LT',
    fullPickerInput: 'L LT',
    datePickerInput: 'L',
    timePickerInput: 'LT',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',


};

@NgModule({
  imports: [
    CommonModule,SharedPipesModule,AlertModule,NgKnifeModule,
    FormsModule,ReactiveFormsModule,NgxPaginationModule,
    OwlDateTimeModule, OwlNativeDateTimeModule,
    PlacebidserviceRoutingModule
  ],
  declarations: [PlacebidserviceComponent],
  providers: [
        // use french locale. This provider is causing trouble when displaying date, so that is why I comment it
        //  {provide: OWL_DATE_TIME_FORMATS, useValue: MY_MOMENT_FORMATS},
    ],
})
export class PlacebidserviceModule { }
