import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlacebidserviceComponent } from './placebidservice.component';

describe('PlacebidserviceComponent', () => {
  let component: PlacebidserviceComponent;
  let fixture: ComponentFixture<PlacebidserviceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlacebidserviceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlacebidserviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
