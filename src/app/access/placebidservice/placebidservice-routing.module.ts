import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PlacebidserviceComponent } from './placebidservice.component';

const routes: Routes = [{ path: '', component: PlacebidserviceComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlacebidserviceRoutingModule { }
