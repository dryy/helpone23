import { SelectModule } from './../../components/select/select.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms'; 

import { OwlDateTimeModule, OwlNativeDateTimeModule,OWL_DATE_TIME_FORMATS  } from 'ng-pick-datetime';

import { AskHelpStep1RoutingModule } from './ask-help-step1-routing.module';
import { AskHelpStep1Component } from './ask-help-step1.component';

import { AlertModule } from './../../_directives/alert.module';


    // See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
export const MY_MOMENT_FORMATS = {
    parseInput: 'l LT',
    fullPickerInput: 'L LT',
    datePickerInput: 'L',
    timePickerInput: 'LT',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',


};

@NgModule({
  imports: [
    CommonModule,
    AskHelpStep1RoutingModule,
    FormsModule,AlertModule,
    OwlDateTimeModule, OwlNativeDateTimeModule ,
    ReactiveFormsModule,
    SelectModule
  ],
  declarations: [AskHelpStep1Component],
  providers: [
        // use french locale
        //  {provide: OWL_DATE_TIME_FORMATS, useValue: MY_MOMENT_FORMATS},
    ],
})
export class AskHelpStep1Module { }
