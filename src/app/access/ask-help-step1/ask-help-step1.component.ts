import { Component, OnInit } from '@angular/core';
import { DatePipe, formatDate } from '@angular/common';
// Must import to use Forms functionality  
import { FormBuilder, FormGroup, Validators ,FormsModule,NgForm } from '@angular/forms'; 
import { first } from 'rxjs/operators'; 
import { Router,ActivatedRoute } from '@angular/router';

import { ServiceCatService,AskhelpService,WalletService,ProfileService } from '../../_services';

import { UserInfo } from '../../_models';

import { ASKStep1 }   from '../../_models/askHelp';

import * as moment from 'moment';

@Component({
  selector: 'app-ask-help-step1',
  templateUrl: './ask-help-step1.component.html',
  styleUrls: ['./ask-help-step1.component.scss']
})
export class AskHelpStep1Component implements OnInit {

    currentUser: UserInfo;
    ValUser: string;
    MyFund: any;
    MyInfo: any;
    MyStats: any;

    askForm: FormGroup;
    submitted = false;
    textUplaod :boolean = false;
    textNoUplaod :boolean = false;
    returnUrl: string;
    categs: any;
    categories: any;
    subcats_data: any;
    urls = new Array<string>();
    fstep: ASKStep1;
    imageSrc: any;
    DataImg =[];
    dateBorder :boolean = false;

    infoMessage:string='';
    minDate: string;
    couleur1: string='#aaa';
    couleur2: string='orange';
    note: number;
    // myDate = formatDate(new Date(), 'yyyy/MM/dd', 'en');
    myDate = new Date();
    yourCategory_id: string;
    yourSubCategory_id: string;
    yourCategory: string;
    yourSubCategory: string;
    yourArea: boolean=false;

    constructor(  private formLog: FormBuilder,private route: ActivatedRoute,
		        private router: Router, private askDataService: AskhelpService, private SetServiceCat: ServiceCatService,private myWalletService: WalletService,private profileServ: ProfileService) {

              this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

              this.ValUser=this.currentUser.user._id;             
        
             }

  ngOnInit() {
    this.getDate();

    this.route.queryParams
      .subscribe(params => {
          if(params.state !== undefined && params.state === 'true') {
              this.infoMessage = 'New Service Created';
          }
      });

  		// To initialize FormGroup  
    this.askForm = this.formLog.group({    
      'categoryService_id':['', Validators.required],
      'sousCategory_id':['', Validators.required],
      'description':['', Validators.required],
      'date1':['', Validators.required],
      'time1':['', Validators.required],
      'date2':['', Validators.required],
      'time2':['', Validators.required],
      'date3':['', Validators.required],
      'time3':['', Validators.required]
    });
    if(localStorage.getItem('idlaCategorie') && localStorage.getItem('idlaSousCategorie')){
      this.f.categoryService_id.setValue(localStorage.getItem('idlaCategorie'));
      this.f.sousCategory_id.setValue(localStorage.getItem('idlaSousCategorie'));
      this.yourCategory_id = localStorage.getItem('idlaCategorie');
      this.yourSubCategory_id = localStorage.getItem('idlaSousCategorie');
      this.yourCategory = localStorage.getItem('categorie');
      this.yourSubCategory = localStorage.getItem('laSousCategorie');
      this.yourArea = true;
    }

    
    this.getCategories();
    this.getCurrentWalletCharge();
    this.getProfileInfoData();
    this.getUserStatistics();

    console.log('enter OnInit');
    
    this.fstep = this.askDataService.getFirstStep(); 
  }

  // Cette fonction permet de récupérer la date courante afin d'indiquer dans le template le fait que l'utilisateur ne peut pas choisir une date antérieure
  getDate(){
    let date: Date = new Date();
    console.log("Date = " + date);
    console.log(date.getMonth());
    let mois = date.getMonth()+1+'';
    let dat = date.getDate()+'';
    if(mois.length==1){
      mois=0+mois;
    }
    if(dat.length==1){
      dat=0+dat;
    }
    this.minDate = date.getFullYear()+'-'+mois+'-'+dat;
    console.log("La Date = " + this.minDate);
  }

  getProfileInfoData() {
      this.profileServ.getProfileInfo(this.ValUser).pipe(first()).subscribe(UserInfoValues => {          
            this.MyInfo = UserInfoValues; 
      });
  }

  getCurrentWalletCharge() {
    this.myWalletService.getMyWallet(this.ValUser).pipe(first()).subscribe(funds => { 
                        this.MyFund = funds; 
    });
  }

    getUserStatistics() {
    this.profileServ.getUserStats(this.ValUser).pipe(first()).subscribe(UserStatsValues => {          
            this.MyStats = UserStatsValues; 
      });
  } 

  getCategories() {
     this.SetServiceCat.getAll().pipe(first()).subscribe(catsData => { 
         this.categs = catsData;
         this.categories = catsData;
         this.categories = this.categories.data;
         console.log(this.categories)
         console.log(this.categs) ;
      });
  }

  loadSubCat(value:string){
    this.f.categoryService_id.setValue(value);
    console.log(this.f.categoryService_id.value);
	  this.SetServiceCat.getSubCatbyCatID(value).pipe(first()).subscribe(subcats => { 
      this.subcats_data = subcats;
      console.log(this.subcats_data);
	   }); 
  }
  
  saveSubCat(value: string){
    this.f.sousCategory_id.setValue(value);
    console.log(value);
  }

	detectFiles(event) {
    this.urls = [];
    this.DataImg = [];
    let files = event.target.files;
    if (files) {
      for (let file of files) {
        let reader = new FileReader();
        reader.onload = (e: any) => {
          this.urls.push(e.target.result);
          this.DataImg.push({picture:e.target.result});
        }
        
        reader.readAsDataURL(file);
      }   

      
    }
  }

   		  // convenience getter for easy access to form fields
      get f() { return this.askForm.controls; }
	  
	  // Executed When Form Is Submitted  
	  onSubmit() {
       
         this.submitted = true;
         console.log(this.askForm.value);

      // stop if upload more than 6 images
        if(this.DataImg.length > 6 ) {
         this.textUplaod = true;
         return;
       }

       // stop if upload no images
        if(this.DataImg.length < 1 ) {
         this.textNoUplaod = true;
         return;
       }

        // stop here if form is invalid
        if (this.askForm.invalid) {
            return;
        }

       this.askDataService.setFirstStep(this.askForm.value); 
       
       this.askDataService.SetImageService(this.DataImg);

	       let dateVal1 = moment(this.f.date1.value).format('YYYY-MM-DD');
	       let timeVal1 = moment(this.f.time1.value).format('hh:mm A');
	       let dateVal2 = moment(this.f.date2.value).format('YYYY-MM-DD');
	       let timeVal2 = moment(this.f.time2.value).format('hh:mm A');
	       let dateVal3 = moment(this.f.date3.value).format('YYYY-MM-DD');
         let timeVal3 = moment(this.f.time3.value).format('hh:mm A');
         let myDates = moment(this.myDate).format('YYYY-MM-DD');
         console.log(myDates);
         if( (dateVal1 < myDates) || (dateVal2 < myDates) || (dateVal3 < myDates) ) {
          console.log("daaaaate 1 : "+ dateVal1+timeVal1);
          console.log("daaaaate 2 : "+ dateVal2+timeVal2);
          console.log("daaaaate 3 : "+ dateVal3+timeVal3);
          this.dateBorder = true;
        }
        else{
            var dataDT = [];
            dataDT.push({time_service:timeVal1,date_service:dateVal1});
            dataDT.push({time_service:timeVal2,date_service:dateVal2});
            dataDT.push({time_service:timeVal3,date_service:dateVal3});
        
        this.askDataService.SetTimeDate(dataDT);

        this.router.navigate(['/access/askHelp-step2']);

        }
      }

      rate(a: any){
        console.log("La note est : "+ a);
      }

      rateResult(a: number){
        this.note = 4;
        if(a>=this.note){
          return this.couleur1;
        }else{
          return this.couleur2;
        }
      }
  sousCat(value:any){

    // this.SetServiceCat.getSubCatbyCatID(value).pipe(first()).subscribe(subcats => { 
    //   this.subcats_data = subcats;
    //   }); 
    console.log(value);
  }

}
