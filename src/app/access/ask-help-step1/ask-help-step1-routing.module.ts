import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AskHelpStep1Component } from './ask-help-step1.component';

const routes: Routes = [{ path: '', component: AskHelpStep1Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AskHelpStep1RoutingModule { }
