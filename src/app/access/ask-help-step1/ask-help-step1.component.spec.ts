import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AskHelpStep1Component } from './ask-help-step1.component';

describe('AskHelpStep1Component', () => {
  let component: AskHelpStep1Component;
  let fixture: ComponentFixture<AskHelpStep1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AskHelpStep1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AskHelpStep1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
