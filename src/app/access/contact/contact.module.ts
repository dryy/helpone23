import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms'; 
import { AlertModule } from './../../_directives/alert.module';

import { ContactRoutingModule } from './contact-routing.module';
import { ContactComponent } from './contact.component';
import { DeletecontactComponent } from './deletecontact/deletecontact.component';

@NgModule({
  imports: [
    CommonModule,
    ContactRoutingModule,AlertModule,
    FormsModule,ReactiveFormsModule
  ],
  declarations: [ContactComponent, DeletecontactComponent]
})
export class ContactModule { }
