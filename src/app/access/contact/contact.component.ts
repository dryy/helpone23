import { Component, OnInit, Output,EventEmitter,ViewChild } from '@angular/core';

// Must import to use Forms functionality  
import { FormBuilder, FormGroup, Validators ,FormsModule,NgForm } from '@angular/forms'; 
import { first } from 'rxjs/operators'; 
import { Router,ActivatedRoute } from '@angular/router';

import { AlertService,AddressService} from './../../_services';
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  AdressData: any;
  ValUser:string='5bd2053c8bc6830ef6ca9bbe';

  title="Read Products";
  adress_id_value:any;

  show_delete_product_html=false;
  show_read_products_html=true;
  show_edit_form_html=false;

  itemsVisibility = [];
  itemsTRVisibility : boolean[]=[];

  saveAdressForm: FormGroup;
  saveAdressForm2: FormGroup;
  submitted = false;submittedSecForm = false;

  @ViewChild('myForm', {static: false}) formValues; // Added this
	
  constructor(  private formLog: FormBuilder,private route: ActivatedRoute,
		        private router: Router,private addressService: AddressService,
		        private alertService: AlertService) { }

  ngOnInit() {

   

  	// To initialize FormGroup  
    this.saveAdressForm = this.formLog.group({    
      'user_id':[this.ValUser, Validators.required],
      'address_title':['', Validators.required],
      'address':['', Validators.required],
      'zip_code_postal':['', Validators.required],
      'telephone':['', Validators.required],
      'city':['', Validators.required],
      'email':['', Validators.required]
    }); 

    this.saveAdressForm2 = this.formLog.group({    
      'user_id':[this.ValUser, Validators.required],
      '_id':['', Validators.required],
      'address_title':['', Validators.required],
      'address':['', Validators.required],
      'zip_code_postal':['', Validators.required],
      'telephone':['', Validators.required],
      'city':['', Validators.required],
      'email':['', Validators.required]
    }); 

     this.loadAddresses();
  }

   private loadAddresses() {
        this.addressService.getByUserID(this.ValUser).pipe(first()).subscribe(dataAdress => { 
            this.AdressData = dataAdress; 
             
             for(let t = 0; t < this.AdressData.data.length; t++) {
               this.itemsTRVisibility[t] = true;
             } 
            //console.log('Array :'+);
        });

    }

      get f() { return this.saveAdressForm.controls; }

      get g() { return this.saveAdressForm2.controls; }
	  
	  // Executed When Form Is Submitted  
	  onSubmit() {
       
       	this.submitted = true;

        // stop here if form is invalid
        if (this.saveAdressForm.invalid) {
            return;
        }

       this.addressService.save(this.saveAdressForm.value)  
      .subscribe(
       //on success 
       (data : any) => { 
            this.alertService.success(data.message, true);
            this.submitted = false;
            //this.saveAdressForm.reset();
            //this.formValues.resetForm();
            this.saveAdressForm.patchValue({
		        "address_title": '',"city": '',"telephone": '',
		        "email": '',"address": '',"zip_code_postal": '' 
		         });
            
            
       },
       //on error
       error => {
           this.alertService.error(error);
       },
        //on complete
       () => { 
            this.loadAddresses(); 
       });


      }

  SubmitUpdate(index: number) {
       
        this.submittedSecForm = true;

        // stop here if form is invalid
        if (this.saveAdressForm2.invalid) {
            return;
        }

       this.addressService.update(this.saveAdressForm2.value).pipe(first()).subscribe(

           //on success 
           (data : any) => { 
                this.alertService.success(data.message, true);
                this.submittedSecForm = false;
                this.cancelUpdate(index);
                
           },

           //on error
           error => {
                this.alertService.error(error);
           },

            //on complete
           () => { 
                
                this.loadAddresses(); 
           }); 


      }
  // show products list
showReadProducts($event){
    // set title
    this.title=$event.title;

    this.loadAddresses();
 
    // hide all html then show only one html
    this.hideAll_Html();
    this.show_read_products_html=true;
}
 
// hide all html views
hideAll_Html(){
    this.show_read_products_html=false;
    this.show_delete_product_html=false;
}

  // when user clicks the 'delete' button
deleteProduct(id){
    // tell the parent component (AppComponent)
    //this.show_delete_product_event.emit({
       // adress_id: id,
      //  title: "Delete Product"
   // });

    this.adress_id_value=id;
    this.title="Delete Product";

    // hide all html then show only one html
    this.hideAll_Html();
    this.show_delete_product_html=true;
}

  // when user clicks the 'delete' button
editProduct(id:string,address:string,telephone:string,address_title:string,zip_code_postal:string,city:string,email:string,index: number){
 
    this.itemsVisibility[index] = true;
    for(let i = 0; i < this.itemsVisibility.length; i++) {
      if (i !== index) {
        this.itemsVisibility[i] = false;
      }
    }  

    this.itemsTRVisibility[index] = false;
    for(let i = 0; i < this.itemsTRVisibility.length; i++) {
      if (i !== index) {
        this.itemsTRVisibility[i] = true;
      }
    } 
    
    this.saveAdressForm2.patchValue({
    "_id": id,"address_title": address_title,"city": city,"telephone": telephone,
    "email": email,"address": address,"zip_code_postal": zip_code_postal 
     });    
    
}

  // when user clicks the 'delete' button
cancelUpdate(index: number){
  this.itemsVisibility[index] = false;
  this.itemsTRVisibility[index] = true;
}




}
