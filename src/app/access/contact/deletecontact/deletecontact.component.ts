import { Component, Input, Output, EventEmitter } from '@angular/core';

// Must import to use Forms functionality  
import { FormBuilder, FormGroup, Validators ,FormsModule,NgForm } from '@angular/forms'; 
import { first } from 'rxjs/operators'; 
import { AddressService,AlertService} from './../../../_services';



@Component({
  selector: 'app-deletecontact',
  templateUrl: './deletecontact.component.html',
  styleUrls: ['./deletecontact.component.scss']
})
export class DeletecontactComponent {

  /*
        @Output will tell the parent component (AppComponent)
        that an event has happened (via .emit(), see readProducts() method below)
    */
    @Output() show_adress_event = new EventEmitter();
 
    // @Input enable getting the product_id from parent component (AppComponent)
    @Input() adress_id : string;

    @Input() titlePage : string;
 
    // initialize product service
    constructor(private adressService: AddressService,private alertService: AlertService){}
 
    // user clicks 'yes' button
    deleteProduct(){
 
        // delete data from database

         this.adressService.delete(this.adress_id).pipe(first()).subscribe(

           //on success 
           (data : any) => { 
                this.alertService.success(data.message, true);
           },

           //on error
           error => {
                this.alertService.error(error);
           },

            //on complete
           () => { 
                this.readProducts();
           });
       }
 
    // user clicks the 'read products' button
    readProducts(){
        this.show_adress_event.emit({ title: "Read Addresses" });
    }
}

