import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteAdressComponent } from './delete-adress.component';

describe('DeleteAdressComponent', () => {
  let component: DeleteAdressComponent;
  let fixture: ComponentFixture<DeleteAdressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteAdressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteAdressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
