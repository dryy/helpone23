import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AskHelpStep2Component } from './ask-help-step2.component';

const routes: Routes = [{ path: '', component: AskHelpStep2Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AskHelpStep2RoutingModule { }
