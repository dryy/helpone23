import { AskHelpStep2Module } from './ask-help-step2.module';

describe('AskHelpStep2Module', () => {
  let askHelpStep2Module: AskHelpStep2Module;

  beforeEach(() => {
    askHelpStep2Module = new AskHelpStep2Module();
  });

  it('should create an instance', () => {
    expect(askHelpStep2Module).toBeTruthy();
  });
});
