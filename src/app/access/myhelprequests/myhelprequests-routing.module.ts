import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MyhelprequestsComponent } from './myhelprequests.component';

const routes: Routes = [{ path: '', component: MyhelprequestsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyhelprequestsRoutingModule { }
