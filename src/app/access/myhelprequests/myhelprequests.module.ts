import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MyhelprequestsRoutingModule } from './myhelprequests-routing.module';
import { MyhelprequestsComponent } from './myhelprequests.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { GridModule } from 'src/app/components/grid/grid.module';

import { SharedPipesModule } from './../../shared';

@NgModule({
  imports: [
    CommonModule,
    SharedPipesModule,
    FormsModule,NgxPaginationModule,
    MyhelprequestsRoutingModule,
    GridModule
  ],
  declarations: [MyhelprequestsComponent]
})
export class MyhelprequestsModule { }
