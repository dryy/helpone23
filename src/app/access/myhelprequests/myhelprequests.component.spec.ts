import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MyhelprequestsComponent } from './myhelprequests.component';

describe('MyhelprequestsComponent', () => {
  let component: MyhelprequestsComponent;
  let fixture: ComponentFixture<MyhelprequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyhelprequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyhelprequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
