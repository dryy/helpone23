import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators'; 
import { Router,ActivatedRoute } from '@angular/router';

import { UserInfo } from '../../_models';

import { WalletService,ProfileService,ServiceListingService,ServiceCatService,PagerService } from '../../_services';
import { HelpClassComponent } from '../classes/help-class.component';

@Component({
    selector: 'app-myhelprequests',
    templateUrl: './myhelprequests.component.html',
    styleUrls: ['./myhelprequests.component.scss']
})
export class MyhelprequestsComponent extends HelpClassComponent implements OnInit {

    currentUser: UserInfo;
    ValUser: string;
    MyFund: any;
    MyInfo: any;
    MyStats: any;
    services_categ: any;
    OpenHelpsReq: any;
    queryString:any;
    p: number = 1;
    columnDefs1 = [
        {headerName: 'ID', field: '_id' },
        {headerName: 'DES', field: 'description' },
        {headerName: 'DEV NAME', field: 'device_name', width: 70},
        {headerName: 'DATE', field: 'create_date' },
        {headerName: 'STATUS', field: 'status' },
        {headerName: 'UPDATED DATE', field: 'update_date' },
        {headerName: 'PRICE', field: 'max_price', width: 50}
    ];

    constructor(  private route: ActivatedRoute,
                    private router: Router,
                    private profileServ: ProfileService,
                    private SetServiceCat: ServiceCatService,
                    private pagerService: PagerService,
                    private serviceList: ServiceListingService,
                    private myWalletService: WalletService) { 
        super(serviceList);
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.ValUser=this.currentUser.user._id;
    }

    ngOnInit() {
        this.getProfileInfoData();
        this.getRecentHelpsRequestsListing(this.columnDefs1, this.ValUser, "serviceaskforhelp/other_recent_helps_requests");
        this.getMyHelpsListing();

    }

    getMyHelpsListing() {
        this.serviceList.getMyAllHelps(this.ValUser).pipe(first()).subscribe(OpenHelpsReqData => {          
                this.OpenHelpsReq = OpenHelpsReqData; 
        });
    }

    getProfileInfoData(){
        this.profileServ.getProfileInfo(this.ValUser).pipe(first()).subscribe(UserInfoValues => {          
                this.MyInfo = UserInfoValues; 
        });
    }

    navigateTo(event: any) {
        console.log("e eeeeeeeee e : ", event.data._id);
        this.router.navigate(['/access/viewBidder/',event.data._id]);
    }

}
