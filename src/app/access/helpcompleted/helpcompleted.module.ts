import { GridModule } from './../../components/grid/grid.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { HelpcompletedRoutingModule } from './helpcompleted-routing.module';
import { HelpcompletedComponent } from './helpcompleted.component';

import {NgxPaginationModule} from 'ngx-pagination'; 

import { SharedPipesModule } from './../../shared';

@NgModule({
  imports: [
    CommonModule,NgxPaginationModule,
    FormsModule,SharedPipesModule,
    HelpcompletedRoutingModule,
    GridModule
  ],
  declarations: [HelpcompletedComponent]
})
export class HelpcompletedModule { }
