import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators'; 
import { Router,ActivatedRoute } from '@angular/router';
import { UserInfo } from '../../_models';
import { WalletService,ProfileService,ServiceListingService,ServiceCatService,PagerService } from '../../_services';
import { AccessComponent } from '../access.component';
import { HelpClassComponent } from '../classes/help-class.component';

@Component({
  selector: 'app-helpcompleted',
  templateUrl: './helpcompleted.component.html',
  styleUrls: ['./helpcompleted.component.scss']
})
export class HelpcompletedComponent extends HelpClassComponent implements OnInit {

  currentUser: UserInfo;
  ValUser: string;
  MyFund: any;
  MyInfo: any;
  MyStats: any;
  queryString:any;
  services_categ: any;
  RecentsHelpReq: any;
  p: number = 1;

  columnDefs1 = [
    {headerName: 'ID', field: '_id' },
    {headerName: 'DES', field: 'description' },
    {headerName: 'DEV NAME', field: 'device_name'},
    {headerName: 'DATE', field: 'create_date' },
    {headerName: 'STATUS', field: 'status' },
    {headerName: 'UPDATED DATE', field: 'update_date' },
    {headerName: 'PRICE', field: 'max_price'}
  ];

  constructor(  private route: ActivatedRoute,
                private router: Router,
                private profileServ: ProfileService,
                private SetServiceCat: ServiceCatService,
                private pagerService: PagerService,
                private serviceList: ServiceListingService,
                private myWalletService: WalletService,
                public acc: AccessComponent) { 
    super(serviceList);
  this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

  this.ValUser=this.currentUser.user._id;
  this.acc.menuManager = 'Completed Helps';

  }

  ngOnInit() {

  	this.getAllCateg();
    this.getUserStatistics();
    this.getProfileInfoData();
    this.getCurrentWalletCharge();
    this.getRecentHelpsRequestsListing(this.columnDefs1, null, "serviceaskforhelp/completed_helps_requests");

  }

    getCurrentWalletCharge() {
    this.myWalletService.getMyWallet(this.ValUser).pipe(first()).subscribe(funds => { 
                        this.MyFund = funds; 
    });
  }

  getUserStatistics() {
    this.profileServ.getUserStats(this.ValUser).pipe(first()).subscribe(UserStatsValues => {          
            this.MyStats = UserStatsValues; 
      });
  } 

  getAllCateg() {
       this.SetServiceCat.getAll().pipe(first()).subscribe(serv_cats => { 
            this.services_categ = serv_cats; 
        });
  }

  
    getCategoryLogo(idCategory:string,TabCat:any): string {

              let verified:boolean = false, i=0;
              let LogoName="";
              if (idCategory && TabCat) {
                 while (!verified && i < TabCat.length ) {

                if ( String(TabCat[i]._id) == idCategory){
                    LogoName=TabCat[i].logo;
                    verified=true;
                  }

                  i++;
                }

              } 

           return LogoName;
    }    

  getCategoryName(idCategory:string,TabCat:any): string {

              let verified:boolean = false, i=0;
              let VarName="";
              if (idCategory && TabCat) {
                 while (!verified && i < TabCat.length ) {

                if ( String(TabCat[i]._id) == idCategory){
                    VarName=TabCat[i].categoryName;
                    verified=true;
                  }

                  i++;
                }

              } 

           return VarName;
    }

    getProfileInfoData(){
      this.profileServ.getProfileInfo(this.ValUser).pipe(first()).subscribe(UserInfoValues => {          
            this.MyInfo = UserInfoValues; 
      });
  }

}
