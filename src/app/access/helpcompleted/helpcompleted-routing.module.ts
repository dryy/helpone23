import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HelpcompletedComponent } from './helpcompleted.component';

const routes: Routes = [{ path: '', component: HelpcompletedComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HelpcompletedRoutingModule { }
