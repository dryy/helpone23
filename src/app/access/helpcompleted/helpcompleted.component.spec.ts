import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HelpcompletedComponent } from './helpcompleted.component';

describe('HelpbanksComponent', () => {
  let component: HelpcompletedComponent;
  let fixture: ComponentFixture<HelpcompletedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HelpcompletedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpcompletedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
