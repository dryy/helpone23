import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ReactiveFormsModule,FormsModule} from '@angular/forms';

import { SharedPipesModule } from './../../shared';

import { NotifRoutingModule } from './notif-routing.module';
import { NotifComponent } from './notif.component';


@NgModule({
  imports: [
    CommonModule,
    SharedPipesModule,
    NotifRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [NotifComponent]
})
export class NotifModule { }
