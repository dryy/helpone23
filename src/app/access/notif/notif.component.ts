import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';

import { first } from 'rxjs/operators'; 

import { VariablesGlobales } from 'src/app/_models/variablesGlobales';
import { NotificationsService } from 'src/app/_services/notifications.service';
import { Notifications } from 'src/app/_models/notifications';

import { AccessComponent } from '../access.component';
import { UserInfo, RoleInfo } from '../../_models';
import { ProfileService, WalletService, UserService, RoleService } from '../../_services';

@Component({
  selector: 'app-notif',
  templateUrl: './notif.component.html',
  styleUrls: ['./notif.component.scss']
})
export class NotifComponent implements OnInit {

  persons: UserInfo[] = [];
  notif: Notifications[] = [];

  role_data: RoleInfo[] = [];

  messages: any;
  // showHide: boolean[] = [];
  notifColor : string = 'rgb(187, 247, 175)';

  currentUser: UserInfo;
  MessageSent: string;
  submitted = false;
	
  ValUser: string;
  MyFund: any;
  MyInfo: any;
  MyStats: any;
  RecentsHelpReq: any;
  OpenHelpsReq: any;
  BidHelpDataTab: any;
  BidHelpInfoDataTab: any;

  constructor(  private profileServ: ProfileService,
                public param: VariablesGlobales,
                private router: Router,
                private SetServiceRole: RoleService,
                private userService: UserService,
                public route: ActivatedRoute,
                private myWalletService: WalletService,
                private notificationsService: NotificationsService,
                public acc: AccessComponent) { 

    this.router.routeReuseStrategy.shouldReuseRoute = function() {
        return false;
    };    
    this.acc.menuManager = 'Notif';
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.ValUser=this.currentUser.user._id;    
    // this.getAll();
  }

  ngOnInit() {    
    this.getAll();
    this.loadAllUsers();
    this.getUserStatistics();
    this.getProfileInfoData();
    this.getCurrentWalletCharge();
  }

  private loadAllUsers() {
    this.userService.getAll().pipe(first()).subscribe((persons: any) => { 
        this.persons = persons.data; 
        console.log(persons.data);
    });

    this.SetServiceRole.getAll().pipe(first()).subscribe((roles: any) => { 
        this.role_data = roles.data; 
    });
  }

  // permet de récupérer toutes les notifications
  getAll(){
    this.notificationsService.getAll(this.currentUser.user._id).pipe(first()).subscribe((notif: any) => {
        this.notif = notif.data; 
        console.log(this.notif); 
        console.log(notif.message); 
    });          
    
  }

  // accordions(i: number){
  //     if(this.showHide[i]==false){
  //         this.showHide[i] = true;
  //     }else{
  //         this.showHide[i] = false;
  //     }
  // }


  getCurrentWalletCharge() {
    this.myWalletService.getMyWallet(this.ValUser).pipe(first()).subscribe(funds => { 
                        this.MyFund = funds; 
    });
  }
  
  getUserStatistics() {
    this.profileServ.getUserStats(this.ValUser).pipe(first()).subscribe(UserStatsValues => {          
            this.MyStats = UserStatsValues; 
      });
  } 

     getProfileInfoData(){
      this.profileServ.getProfileInfo(this.ValUser).pipe(first()).subscribe(UserInfoValues => {          
            this.MyInfo = UserInfoValues; 
      });
  }

  // delete a notification
  onDelete(notifId: string){
    this.notificationsService.deleteNotif(notifId).pipe(first()).subscribe((notif: any) => { 
        console.log(notif.message); 
        console.log(notif.status); 
        this.getAll();
    });
  }

  onRead(notif_id: string){
    this.notificationsService.readNotif(notif_id).pipe(first()).subscribe((notif: any) => { 
      console.log(notif.message); 
      console.log(notif_id);
      console.log(notif.data); 
      this.getAll();
    });
  }

  couleur(lue: boolean){
    if(lue==false){
      this.notifColor = 'rgb(187, 247, 175)';
    }
    else{
      this.notifColor = 'rgb(255, 255, 255)';
    }
  }
  

}
