import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotifComponent } from './notif.component';

const routes: Routes = [{ path: '', component: NotifComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotifRoutingModule { }
