import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { AuthenticationService } from '../../_services';

@Component({
  selector: 'app-file-name-dialog',
  templateUrl: './file-name-dialog.component.html',
  styleUrls: ['./file-name-dialog.component.scss'],
  
})

export class FileNameDialogComponent implements OnInit {

  constructor(private dialogRef: MatDialogRef<FileNameDialogComponent>,
     private authenticationService: AuthenticationService,) { }

  ngOnInit() {
  }

  clo() {
    this.dialogRef.close();
  }

  logoutApp(): void {
    console.log("Logout");
    this.authenticationService.logout();
    this.clo();
}

}
