import { Component, OnInit, Input } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { AskhelpService,ProfileService }  from '../_services';
import { VariablesGlobales } from '../_models/variablesGlobales';
import { UserInfo } from '../_models';

declare var jQuery:any;
declare var $ :any;

@Component({
  selector: 'app-access',
  templateUrl: './access.component.html', 
  styleUrls: ['./access.component.scss']
})
export class AccessComponent implements OnInit {
	
	@Input() formData;
	@Input() UserProfileData;
	menuManager: string;
	currentUser: UserInfo;

  constructor( public route: ActivatedRoute,
               private router: Router,
			   private AskHelpService: AskhelpService,
			   public param: VariablesGlobales,
               private userProfileService: ProfileService) {
    
      // override the route reuse strategy
        this.router.routeReuseStrategy.shouldReuseRoute = function() {
            return false;
		};
		this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
		if(this.param.parametre == ''){
			this.param.parametre = this.currentUser.user.fonctionName;
		}

   }
 
  ngOnInit() {

  this.formData = this.AskHelpService.getAskData();

  this.UserProfileData = this.userProfileService.getUserProfileData();

  //________SCROLL TOP BUTTON________//	

	jQuery("button.scroltop").on('click', function() {
		jQuery("html, body").animate({
			scrollTop: 0
		}, 1000);
		return false;
	});

		//________BOOSTRAP 4 TABS ACTIVE________//

	$(".nav-tabs li a").click(function(){
		      $(".nav-tabs li").removeClass("active");
		      $(this).parent().addClass("active");
        });


	jQuery(window).on("scroll", function() {
		var scroll = jQuery(window).scrollTop();
		if (scroll > 900) {
			jQuery("button.scroltop").fadeIn(1000);
		} else {
			jQuery("button.scroltop").fadeOut(1000);
		}
	});


//________FOOTER FIXED ON BOTTOM PART________//	

	jQuery('.site-footer').css('display', 'block');
	jQuery('.site-footer').css('height', 'auto');
	var footerHeight = jQuery('.site-footer').outerHeight();
	jQuery('.footer-fixed > .page-wraper').css('padding-bottom', footerHeight);
	jQuery('.site-footer').css('height', footerHeight);

  }

}
