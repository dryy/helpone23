import { Component, OnInit, Input } from '@angular/core';
import { IPayPalConfig,ICreateOrderRequest } from 'ngx-paypal';
// Must import to use Forms functionality
import { FormBuilder, FormGroup, Validators ,FormsModule,NgForm } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router,ActivatedRoute } from '@angular/router';
import { UserInfo } from '../../_models';
import { ProfileData }  from '../../_models/profile';
import { WalletService,MethodPaymentService,AlertService,ProfileService,LanguageService,ServiceCatService } from '../../_services';
import { MatDialog } from '@angular/material/dialog';

declare var jQuery:any;
declare var $ :any;

@Component({ 
selector: 'app-profile',
templateUrl: './profile.component.html',
styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

    @Input() profileData: ProfileData;

    public payPalConfig ? : IPayPalConfig;

    currentUser: UserInfo;
    ValUser: string;
    MyFund: any;
    MyInfo: any;
    MyStats: any;
    MyAddress: any;
    dataSkillsTab = [];
    dataLangTab = [];
    LanguageData: any[] = [];
    pourcentage = '%';
    niveau: number = 0;

    categs: any;
    subcats_data: any;
    chosenOption:any;
    chosenOptionTitle:any;
    listChosen:any;

    sec_col:boolean= false;
    third_col:boolean= false;

    buttonDeposit:boolean= true;
    buttonPaypal:boolean= false;

    TableData: any[] = [];

    urls: any;
    DataImg: any;
    DataCniImg: any;
    DataCniWithImg: any;
    showPicturePreview: boolean=false;
    showCniPicturePreview: boolean=false;
    showImgWithCniPicturePreview: boolean=false;

    dropdownList = [];
    selectedItems = [];
    dropdownSettings = {};

    activeTab:string;;
    paymentVal: any;
    historyVal: any;
    paymentNameValue: string='';

    submitted = false;
    submittedAddressForm = false;
    submittedForm1 = false;
    submittedForm2 = false;
    submittedForm3 = false;
    submittedForm4 = false;
    submittedFormLang = false;


    ShowUpdateForm = [];
    ShowContentAddress : boolean[]=[];

    ShowF1EditForm : boolean = false;
    ShowF1Form : boolean = true;

    ShowF2EditForm : boolean = false;
    ShowF2Form : boolean = true;

    ShowF3EditForm : boolean = false;
    ShowF3Form : boolean = true;

    ShowF4EditForm : boolean = false;
    ShowF4Form : boolean = true;

    ShowLangEditForm : boolean = false;
    ShowLangForm : boolean = true;
    email: string;

    rechargeWalletForm: FormGroup;
    saveAdressForm: FormGroup;
    saveUserNameForm: FormGroup;
    saveUserDescForm: FormGroup;
    saveUserTelForm: FormGroup;
    saveUserEmailForm: FormGroup;
    saveUserLangForm: FormGroup;

    constructor(  private route: ActivatedRoute,
                    private router: Router,
                    private formLog: FormBuilder,
                    private myWalletService: WalletService,
                    private alertService: AlertService,
                    private SetLangRole: LanguageService,
                    private SetServiceCat: ServiceCatService,
                    private PaymentType: MethodPaymentService,
                    private profileServ: ProfileService,
                    public dialog: MatDialog) {

        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.ValUser=this.currentUser.user._id;
    }

    ngOnInit() {
    // Get thr URL route params to get a particular Tab Active and Open
    this.route.fragment.subscribe(
        (fragment) => {
            this.activeTab=fragment;
        });
        this.initPaypalConfig();
        // To initialise Language Input Select
        this.dropdownSettings = {
        singleSelection: false,
        idField: 'nom_langue',
        textField: 'nom_langue',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        maxHeight:400,
        itemsShowLimit: 4,
        allowSearchFilter: true
        };
                // To initialize RechargeForm
        this.rechargeWalletForm = this.formLog.group({
        'methodpayment_id':['', Validators.required],
        'methodpayment_name':[this.paymentNameValue, Validators.required],
        'user_id':[this.ValUser, Validators.required],
        'user_balance':['', Validators.required],
        'amount':['', Validators.required]
        });


        // To Initialise Address Update Form
        this.saveAdressForm = this.formLog.group({
        'user_id':[this.ValUser, Validators.required],
        '_id':['', Validators.required],
        'address_title':['', Validators.required],
        'address':['', Validators.required],
        'zip_code_postal':['', Validators.required],
        'telephone':['', Validators.required],
        'city':['', Validators.required],
        'email':['', Validators.compose([Validators.required,Validators.email])]
        });

        // To Initialise Address Update Form
        this.saveUserNameForm = this.formLog.group({
        'username':['', Validators.required]
        });

        // To Initialise User Description Form
        this.saveUserDescForm = this.formLog.group({
        'personal_description':['', Validators.required]
        });


        // To Initialise User Telephone Form
        this.saveUserTelForm = this.formLog.group({
        'phone':['', Validators.required]
        });


        // To Initialise User Email Form
        this.saveUserEmailForm = this.formLog.group({
        'email':['', Validators.compose([Validators.required,Validators.email])]
        });

        // To Initialise User Lang Form
        this.saveUserLangForm = this.formLog.group({
        'langues':['', Validators.required]
        });



        this.profileData = this.profileServ.getUserProfileData();

        this.getUserStatistics();
        this.getLanguages();
        this.getCategories();
        this.getProfileInfoFunc();
        this.getProfileAddressFunc();
        this.getHistWallet();
        this.getCurrentWalletCharge();
        this.getPaymentType();
    }

    private initPaypalConfig(): void {
        this.payPalConfig =  {
            currency: 'USD',
            clientId: 'Adihiwo7dpJ3GzDqFoP6BK-KsrfXsWLIRsDl51vQ-etwrUGCaO7ZHUNkb_S_iMQFQqbzEX5boNFNudXX',
            createOrderOnClient: (data) => < ICreateOrderRequest > {
                intent: 'CAPTURE',
                purchase_units: [{
                    amount: {
                        currency_code: 'USD',
                        value: this.f.amount.value,
                        breakdown: {
                            item_total: {
                                currency_code: 'USD',
                                value: this.f.amount.value
                            }
                        }
                    },
                    items: [{
                        name: 'HELP123 FUNDS DEPOSIT',
                        quantity: '1',
                        category: 'DIGITAL_GOODS',
                        unit_amount: {
                            currency_code: 'USD',
                            value: this.f.amount.value,
                        },
                    }]
                }]
            },

            style: {
                label: 'paypal',
                layout: 'horizontal'
            },
            onApprove: (data, actions) => {
                console.log('onApprove - transaction was approved, but not authorized', data, actions);
                actions.order.get().then(details => {
                    console.log('onApprove - you can get full order details inside onApprove: ', details);
                });

            },
            onClientAuthorization: (data) => {
                console.log('onClientAuthorization - you should probably inform your server about completed transaction at this point', data);

            this.myWalletService.ChargeWallet(this.rechargeWalletForm.value)
            .pipe(first())
            .subscribe(

                //on success
                (dataGet : any) => {
                    this.alertService.success(dataGet.message, true);
                    let ValueAdded= ( parseInt(this.f.amount.value) + parseInt(this.f.user_balance.value) );
                    this.myWalletService.setBalance(ValueAdded.toString());
                    this.submitted = false;
                    this.rechargeWalletForm.patchValue({"methodpayment_id": "","amount": "","methodpayment_name": "" });
                    this.buttonDeposit= true;
                    this.buttonPaypal= false;

                },

                //on error
                error => {
                    this.alertService.error(error);
                },

                //on complete
                () => {
                    this.initPaypalConfig();
                    this.getCurrentWalletCharge();
                    this.getHistWallet();
                });

            },
            onCancel: (data, actions) => {
                console.log('OnCancel', data, actions);

            },
            onError: err => {
                console.log('OnError', err);
            },
            onClick: () => {
                console.log('onClick');
            },

        };
    }


    // convenience getter for easy access to form fields
    get h() { return this.saveAdressForm.controls; }
    get f1() { return this.saveUserNameForm.controls; }
    get f2() { return this.saveUserDescForm.controls; }
    get f3() { return this.saveUserTelForm.controls; }
    get f4() { return this.saveUserEmailForm.controls; }
    get flang() { return this.saveUserLangForm.controls; }


    getCurrentWalletCharge(){
        this.myWalletService.getMyWallet(this.ValUser).pipe(first()).subscribe(funds => {
                            this.MyFund = funds;
            this.rechargeWalletForm.patchValue({"user_balance": this.MyFund.data[0].amount });
        });
    }


    getPaymentType(){
        this.PaymentType.getAll().pipe(first()).subscribe(paymentData => {
        this.paymentVal = paymentData;
        });

    }

    getLanguages(){
        this.SetLangRole.getAll().pipe(first()).subscribe((lang: any)=> {
                this.LanguageData = lang.data;
                console.log("laaaaaaaaagues : ", this.LanguageData);
                // let arr = [];  
                // Object.keys(lang.data).map(function(key){  
                //     arr.push({[key]:lang.data[key]})  
                //     return arr;  
                // }); 
                // console.log('Object=',this.LanguageData)  
                // console.log('Array=',arr[0][0].nom_langue) 
            });

    }

    getCategories(){
            this.SetServiceCat.getAll().pipe(first()).subscribe(catsData => {
                this.categs = catsData;
            });
    }

    private getUserStatistics() {
        this.profileServ.getUserStats(this.ValUser).pipe(first()).subscribe(UserStatsValues => {
                this.MyStats = UserStatsValues;
        });
    }

    getProfileInfoFunc(){
        this.pourcentage="%";
        this.profileServ.getProfileInfo(this.ValUser).pipe(first()).subscribe(UserInfoValues => {

            // console.log("langue;;;;;;;; "+ UserInfoValues.data[]);
            this.MyInfo = UserInfoValues;
            this.dataLangTab = [];
            this.dataSkillsTab = [];
            console.log("langue;;;;;;;; ", this.MyInfo.data);

            for(var i=0; i< this.MyInfo.data[1].length; i++)  {
                this.dataSkillsTab.push({souscategory_id:this.MyInfo.data[1][i].souscategory_id,souscategoryName:this.MyInfo.data[1][i].souscategoryName});
            }

            for(var i=0; i< this.MyInfo.data[2].length; i++)  {
                this.dataLangTab.push(this.MyInfo.data[2][i].langue);
            }

            this.profileServ.setUserProfileData(this.MyInfo.data[0],this.dataSkillsTab,this.dataLangTab);
            this.niveau = this.profileServ.getNiveau();
            console.log('Le niveau est      :    '+this.niveau);
            this.pourcentage = ((this.niveau*100)/28|0) + this.pourcentage;
        });
    }

    getProfileAddressFunc(){
        this.profileServ.getProfileAddress(this.ValUser).pipe(first()).subscribe(UserAddressValues => {
                    this.MyAddress = UserAddressValues;

                    for(let t = 0; t < this.MyAddress.data.length; t++) {
                        this.ShowContentAddress[t] = true;
                    }
        });
    }

    getHistWallet(){
        this.myWalletService.HistoryTransact(this.ValUser).pipe(first()).subscribe(DataHistTransac => {
            this.historyVal = DataHistTransac;
        });
    }

        uploadProfilePic(event) {
        this.showPicturePreview=true;
        this.urls = [];
        this.DataImg = '' ;
        let files = event.target.files;
        if (files) {
        for (let file of files) {
            let reader = new FileReader();
            reader.onload = (e: any) => {
            this.urls.push(e.target.result);
            this.DataImg = e.target.result;
            }
            reader.readAsDataURL(file);
        }
        }
    }

    uploadCniPic(event){
        this.showCniPicturePreview=true;
        this.urls = [];
        this.DataCniImg = '' ;
        let files = event.target.files;
        if (files) {
        for (let file of files) {
            let reader = new FileReader();
            reader.onload = (e: any) => {
            this.urls.push(e.target.result);
            this.DataCniImg = e.target.result;
            }
            reader.readAsDataURL(file);
        }
        }
    }

    uploadCniWithPic(event){
        this.showImgWithCniPicturePreview=true;
        this.urls = [];
        this.DataCniWithImg = '' ;
        let files = event.target.files;
        if (files) {
        for (let file of files) {
            let reader = new FileReader();
            reader.onload = (e: any) => {
            this.urls.push(e.target.result);
            this.DataCniWithImg = e.target.result;
            }
            reader.readAsDataURL(file);
        }
        }
    }

    cancelUpdateProfile(){
    this.showPicturePreview=false;
    this.urls = [];
    this.DataImg = '' ;
    }

    cancelUpdateCni(){
        this.showCniPicturePreview=false;
        this.urls = [];
        this.DataCniImg = '' ;
    }

    cancelUpdateImgWithCni(){ 
        this.showImgWithCniPicturePreview=false;
        this.urls = [];
        this.DataCniWithImg = '' ;
    }

    UpdateProfile() {

    this.profileServ.setImgValue(this.DataImg);

    this.profileServ.updateProfileInfo(this.profileData,this.ValUser).pipe(first()).subscribe(
        //on success
        (data : any) => {
                this.alertService.success(data.message, true);
                this.showPicturePreview=false;
                this.urls = [];
                this.DataImg = '' ;
        },
        //on error
        error => {
                this.alertService.error(error);
        },
            //on complete
        () => {
            this.getProfileInfoFunc();
        });
    }

    UpdateCni() {

        this.profileServ.setCniImgValue(this.DataCniImg); 

        this.profileServ.updateProfileInfo(this.profileData,this.ValUser).pipe(first()).subscribe(
            //on success
            (data : any) => {
                this.alertService.success(data.message, true);
                this.showCniPicturePreview=false;
                this.urls = [];
                this.DataCniImg = '' ;
            },
            //on error
            error => {
                this.alertService.error(error);
            },
            //on complete
            () => {
            this.getProfileInfoFunc();
            });
    }

    UpdateImgWithCni() {

        this.profileServ.setImgWithCniValue(this.DataCniWithImg); 

        this.profileServ.updateProfileInfo(this.profileData,this.ValUser).pipe(first()).subscribe(
            //on success
            (data : any) => {
                this.alertService.success(data.message, true);
                this.showImgWithCniPicturePreview=false;
                this.urls = [];
                this.DataCniWithImg = '' ;
            },
            //on error
            error => {
                this.alertService.error(error);
            },
            //on complete
            () => {
            this.getProfileInfoFunc();
            });
    }

    loadSubCat(value:string,name:string) {

        this.SetServiceCat.getSubCatbyCatID(value).pipe(first()).subscribe(subcats => {
        this.subcats_data = subcats;
        });
        this.chosenOptionTitle = name;
        this.sec_col = true;
    }

    ListSelectedItems(event,list:number) {
        if (event.target.checked) {
            if (this.TableData.indexOf((event.target.name)) < 0 && this.TableData.length < list ) {
                    this.TableData.push(({'souscategory_id':event.target.value,'souscategoryName':event.target.name}));
            }

        } else {

        //let index = this.TableData.findIndex(d => d.sousCategorie_id === event.target.value);
        //find index in your array
        //this.TableData.splice(index, 1);//remove element from array

            for(let i = 0; i < this.TableData.length; i++){
                if(this.TableData[i].souscategory_id == event.target.value) {
                    this.TableData.splice(i, 1);
                    i--;
                }
            }
        }
        this.third_col = true;

        let SelectedItems = this.TableData.length;
        this.chosenOption = SelectedItems+' / '+list+' selected';

    // console.log('data Cat Service :'+JSON.stringify(this.TableData));
    }

    onSave() {

    this.profileServ.SetSousCateg(this.TableData);

    this.profileServ.updateProfileInfo(this.profileData,this.ValUser).pipe(first()).subscribe(
        //on success
        (data : any) => {
                this.alertService.success(data.message, true);
                $('.modal').modal('hide');
                this.sec_col = false;
                this.third_col= false;
        },
        //on error
        error => {
                this.alertService.error(error);
        },
            //on complete
        () => {

            this.getProfileInfoFunc();
            this.getCategories();
        });
    }

            // when user clicks the 'edit' link
    editF1(username:string){
        this.ShowF1EditForm = true;
        this.ShowF1Form = false;
        this.saveUserNameForm.patchValue({"username": username });

    }

        // when user clicks the 'cancel' link
    cancelF1(){
    this.ShowF1EditForm = false;
    this.ShowF1Form = true;
    }

    // when user clicks the 'submit' link
    SubmitUpdateF1() {
        this.submittedForm1 = true;

        // stop here if form is invalid
        if (this.saveUserNameForm.invalid) {
            return;
        }

        this.profileServ.setF1Value(this.f1.username.value);

        this.profileServ.updateProfileInfo(this.profileData,this.ValUser).pipe(first()).subscribe(
        //on success
        (data : any) => {
                this.alertService.success(data.message, true); 
                this.submittedForm1 = false;
        },
        //on error
        error => {
                this.alertService.error(error);
        },
            //on complete
        () => {
            this.cancelF1();
            this.getProfileInfoFunc();
        });
    }

                // when user clicks the 'edit' link
    editF2(description:string){
        this.ShowF2EditForm = true;
        this.ShowF2Form = false;
        this.saveUserDescForm.patchValue({"personal_description": description });

    }

    addF2(){
        this.ShowF2EditForm = true;
        this.ShowF2Form = false;

    }

        // when user clicks the 'cancel' link
    cancelF2(){
    this.ShowF2EditForm = false;
    this.ShowF2Form = true;
    }

    // when user clicks the 'submit' link
    SubmitUpdateF2() {
        this.submittedForm2 = true;

        // stop here if form is invalid
        if (this.saveUserDescForm.invalid) {
            return;
        }

        this.profileServ.setF2Value(this.f2.personal_description.value);

        this.profileServ.updateProfileInfo(this.profileData,this.ValUser).pipe(first()).subscribe(
        //on success
        (data : any) => {
                this.alertService.success(data.message, true);
                this.submittedForm2 = false;
        },
        //on error
        error => {
                this.alertService.error(error);
        },
            //on complete
        () => {
            this.cancelF2();
            this.getProfileInfoFunc();
        });
    }

    editF3(phone:string){
        this.ShowF3EditForm = true;
        this.ShowF3Form = false;
        this.saveUserTelForm.patchValue({"phone": phone });

    }

    addF3(){
        this.ShowF3EditForm = true;
        this.ShowF3Form = false;

    }

        // when user clicks the 'cancel' link
    cancelF3(){
    this.ShowF3EditForm = false;
    this.ShowF3Form = true;
    }

    // when user clicks the 'submit' link
    SubmitUpdateF3() {
        this.submittedForm3 = true;

        // stop here if form is invalid
        if (this.saveUserTelForm.invalid) {
            return;
        }

        this.profileServ.setF3Value(this.f3.phone.value);

        this.profileServ.updateProfileInfo(this.profileData,this.ValUser).pipe(first()).subscribe(
        //on success
        (data : any) => {
                this.alertService.success(data.message, true);
                this.submittedForm3 = false;
        },
        //on error
        error => {
                this.alertService.error(error);
        },
            //on complete
        () => {
            this.cancelF3();
            this.getProfileInfoFunc();
        });
    }

    editF4(email:string){
        this.ShowF4EditForm = true;
        this.ShowF4Form = false;
        this.saveUserEmailForm.patchValue({"email": email });

    }

    // when user clicks the 'cancel' link
    cancelF4(){
        this.ShowF4EditForm = false;
        this.ShowF4Form = true;
    }
    // when user clicks the 'submit' link
    SubmitUpdateF4() {
        this.submittedForm4 = true;
        // stop here if form is invalid
        if (this.saveUserEmailForm.invalid) {
            return;
        }
        this.profileServ.setF4Value(this.f4.email.value);
        this.profileServ.updateProfileInfo(this.profileData,this.ValUser).pipe(first()).subscribe(
        //on success
        (data : any) => {
                this.alertService.success(data.message, true);
                this.submittedForm4 = false;
        },
        //on error
        error => {
                this.alertService.error(error);
        },
            //on complete
        () => {
            this.cancelF4();
            this.getProfileInfoFunc();
        });
    }

    editLangForm(email:string){
        this.ShowLangEditForm = true;
        this.ShowLangForm = false;
        //this.saveUserEmailForm.patchValue({"email": email });
    }

    // when user clicks the 'cancel' link
    cancelLangForm(){
        this.ShowLangEditForm = false;
        this.ShowLangForm = true;
    }

    // when user clicks the 'submit' link
    SubmitUpdateLangForm() {
        this.submittedFormLang = true;
        // stop here if form is invalid
        if (this.saveUserLangForm.invalid) {
            return;
        }
        let stringToSplit = this.flang.langues.value.toString().split(",");
        var dataLangTabArray = [];
        for(var i=0; i< this.flang.langues.value.length; i++)  {
            dataLangTabArray.push(this.flang.langues.value[i].nom_langue);
            console.log("languesss    ", dataLangTabArray);        
        }
        this.profileServ.SetLang(dataLangTabArray);
        this.profileServ.updateProfileInfo(this.profileData,this.ValUser).pipe(first()).subscribe(
        //on success
        (data : any) => {
            this.alertService.success(data.message, true);
            this.submittedFormLang = false;
        },
        //on error
        error => {
            this.alertService.error(error);
        },
            //on complete
        () => {
            this.cancelLangForm();
            this.getProfileInfoFunc();
        });
    }

    // when user clicks the 'cancel' link
    cancelUpdateAddress(index: number){
        this.ShowUpdateForm[index] = false;
        this.ShowContentAddress[index] = true;
    }


        // when user clicks the 'edit' link
    editAddress(id:string,address:string,telephone:string,address_title:string,zip_code_postal:string,city:string,email:string,index: number){

        this.ShowUpdateForm[index] = true;
        for(let i = 0; i < this.ShowUpdateForm.length; i++) {
            if (i !== index) {
                this.ShowUpdateForm[i] = false;
            }
        }

        this.ShowContentAddress[index] = false;
        for(let i = 0; i < this.ShowContentAddress.length; i++) {
        if (i !== index) {
            this.ShowContentAddress[i] = true;
        }
        }

        this.saveAdressForm.patchValue({
        "_id": id,"address_title": address_title,"city": city,"telephone": telephone,
        "email": email,"address": address,"zip_code_postal": zip_code_postal
        });

    }

    // when user clicks the 'submit' link
    SubmitUpdateAddress(index: number) {
        this.submittedAddressForm = true;
        // stop here if form is invalid
        if (this.saveAdressForm.invalid) {
            return;
        }

        this.profileServ.updateProfileAddress(this.saveAdressForm.value).pipe(first()).subscribe(
            //on success
            (data : any) => {
                    this.alertService.success(data.message, true);
                    this.submittedAddressForm = false;
            },
            //on error
            error => {
                    this.alertService.error(error);
            },
                //on complete
            () => {
                this.cancelUpdateAddress(index);
                this.getProfileAddressFunc();
            }
        );
    }

    showPaymentName(value:string) {
        //this.paymentNameValue = value;
        this.rechargeWalletForm.patchValue({
            "methodpayment_name": value
        });
        if(value=="PayPal") {
            this.buttonDeposit= false;
            this.buttonPaypal= false;
        }
        else{
            this.buttonDeposit= true;
            this.buttonPaypal= false;
        }
    }

    inputValidator(event: any) {
        if ( (event.target.value.length > 0) && (this.f.methodpayment_name.value=="PayPal") ) {
        this.buttonDeposit= false;
        this.buttonPaypal= true;
        }
        else {
        this.buttonDeposit= true;
        this.buttonPaypal= false;
        }
    }

    // convenience getter for easy access to form fields
    get f() { return this.rechargeWalletForm.controls; }

    // Executed When Form Is Submitted
    onSubmit(AmountValue) {
        this.submitted = true;
        // stop here if form is invalid
        if (this.rechargeWalletForm.invalid) {
            return;
        }
        this.myWalletService.ChargeWallet(this.rechargeWalletForm.value)
        .pipe(first())
        .subscribe(
        //on success
        (dataGet : any) => {
            this.alertService.success(dataGet.message, true);
            let ValueAdded= ( parseInt(this.f.amount.value) + parseInt(AmountValue) );
            this.myWalletService.setBalance(ValueAdded.toString());
            this.submitted = false;
            this.rechargeWalletForm.patchValue({"methodpayment_id": "","amount": "","methodpayment_name": "" });
        },
        //on error
        error => {
            this.alertService.error(error);
        },
        //on complete
        () => {
            this.getCurrentWalletCharge();
            this.getHistWallet();
        });
    }
    
}
