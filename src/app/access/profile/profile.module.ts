import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule,ReactiveFormsModule } from '@angular/forms'; 
import { AlertModule } from './../../_directives/alert.module';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { CustomMaterialModule} from "./../../material.module";

import { SharedPipesModule } from './../../shared';

import {DataTableModule} from 'angular-6-datatable';

import { NgxPayPalModule } from 'ngx-paypal';

import { NgKnifeModule } from 'ng-knife';

import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileComponent } from './profile.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,SharedPipesModule,NgxPayPalModule,
    ReactiveFormsModule,CustomMaterialModule,NgKnifeModule,
    AlertModule,DataTableModule,NgMultiSelectDropDownModule,
    ProfileRoutingModule
  ],
  declarations: [ProfileComponent]
})
export class ProfileModule { }
