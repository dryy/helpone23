import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { HelpbanksRoutingModule } from './helpbanks-routing.module';
import { HelpbanksComponent } from './helpbanks.component';

import {NgxPaginationModule} from 'ngx-pagination'; 

import { SharedPipesModule } from './../../shared';
import { GridModule } from 'src/app/components/grid/grid.module';

@NgModule({
  imports: [
    CommonModule,NgxPaginationModule,
    FormsModule,SharedPipesModule,
    HelpbanksRoutingModule,
    GridModule
  ],
  declarations: [HelpbanksComponent]
})
export class HelpbanksModule { }
