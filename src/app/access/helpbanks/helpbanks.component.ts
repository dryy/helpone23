import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { first } from 'rxjs/operators'; 
import { Router } from '@angular/router';
import { UserInfo } from '../../_models';
import { ProfileService,ServiceListingService } from '../../_services';
import { AccessComponent } from '../access.component';
import { HelpClassComponent } from '../classes/help-class.component';
import { PopupComponent } from 'src/app/components/popup/popup.component';
// import { AboutComponent } from 'src/app/components/about/about.component';

@Component({
    selector: 'app-helpbanks',
    templateUrl: './helpbanks.component.html',
    styleUrls: ['./helpbanks.component.scss']
})
export class HelpbanksComponent extends HelpClassComponent implements OnInit {

        // @ViewChild('content', {static: true}) content: AboutComponent;
        currentUser: UserInfo;
        ValUser: string;
        MyInfo: any;
        columnDefs1 = [
            {headerName: 'ID', field: '_id' },
            {headerName: 'DES', field: 'description' },
            {headerName: 'DEV NAME', field: 'device_name', width: 70},
            {headerName: 'DATE', field: 'create_date' },
            {headerName: 'STATUS', field: 'status' },
            {headerName: 'UPDATED DATE', field: 'update_date' },
            {headerName: 'PRICE', field: 'max_price', width: 50}
        ];
        ServicePicInfos: any;

        constructor(private router: Router,
                    private profileServ: ProfileService,
                    private serviceList: ServiceListingService,
                    public dialog: MatDialog,
                    public acc: AccessComponent) { 
            super(serviceList);
            this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
            this.ValUser=this.currentUser.user._id;
            this.acc.menuManager = 'Help Banks';
        }

        ngOnInit() {
            this.getProfileInfoData();
            this.getRecentHelpsRequestsListing(this.columnDefs1, this.ValUser, "serviceaskforhelp/recent_helps_requests");
        }

        navigateTo(event: any) {
            console.log("e eeeeeeeee e : ", event);
            this.serviceList.getServicePictures(event.data._id).pipe(first()).subscribe(ServiceV => {          
                this.ServicePicInfos = ServiceV;
                this.ServicePicInfos = this.ServicePicInfos.data;
            });
            this.dialog.open(PopupComponent, {
                data: {
                service: event.data,
                picture: this.ServicePicInfos,
                }
            });
        }

        getProfileInfoData(){
        this.profileServ.getProfileInfo(this.ValUser).pipe(first()).subscribe(UserInfoValues => {          
                this.MyInfo = UserInfoValues; 
                console.log("Data  : ",this.MyInfo)
        });
    }

}
