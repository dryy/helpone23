import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HelpbanksComponent } from './helpbanks.component';

describe('HelpbanksComponent', () => {
  let component: HelpbanksComponent;
  let fixture: ComponentFixture<HelpbanksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HelpbanksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpbanksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
