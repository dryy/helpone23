import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HelpbanksComponent } from './helpbanks.component';

const routes: Routes = [{ path: '', component: HelpbanksComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HelpbanksRoutingModule { }
